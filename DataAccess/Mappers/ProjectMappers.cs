﻿using Common.Infos;
using Common.Models;
using DataAccess.Mappers.Utils;

namespace DataAccess.Mappers;

public static class ProjectMappers
{
    public static ProjectGroupInfo Map(this ProjectGroup entity)
    {
        var info = new ProjectGroupInfo();
        entity.MapParentReferenceEntity<ProjectGroup, Project, ProjectGroupInfo>(info);
        return info;
    }

    public static ProjectInfo Map(this Project entity)
    {
        var info = new ProjectInfo();
        entity.MapChildReferenceEntity<Project, ProjectGroup, ProjectInfo>(info);
        return info;
    }
}