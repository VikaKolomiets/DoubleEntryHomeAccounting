﻿using Common.Infos;
using Common.Models;
using DataAccess.Mappers.Utils;

namespace DataAccess.Mappers;

public static class CorrespondentMappers
{
    public static CorrespondentGroupInfo Map(this CorrespondentGroup entity)
    {
        var info = new CorrespondentGroupInfo();
        entity.MapParentReferenceEntity<CorrespondentGroup, Correspondent, CorrespondentGroupInfo>(info);
        return info;
    }

    public static CorrespondentInfo Map(this Correspondent entity)
    {
        var info = new CorrespondentInfo();
        entity.MapChildReferenceEntity<Correspondent, CorrespondentGroup, CorrespondentInfo>(info);
        return info;
    }
}