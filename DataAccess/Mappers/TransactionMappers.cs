﻿using Common.Infos;
using Common.Models;
using DataAccess.Mappers.Utils;

namespace DataAccess.Mappers;

public static class TransactionMappers
{
    public static TransactionInfo Map(this Transaction entity)
    {
        var info = new TransactionInfo();
        entity.MapId(info);

        info.DateTime = entity.DateTime;
        info.State = entity.State;
        info.Comment = entity.Comment;

        info.EntriesIds.AddRange(entity.Entries.Select(e => e.Id));

        return info;
    }

    public static TransactionEntryInfo Map(this TransactionEntry entity)
    {
        var info = new TransactionEntryInfo();
        entity.MapId(info);

        info.TransactionId = entity.Transaction.Id;
        info.AccountId = entity.Account.Id;
        info.Amount = entity.Amount;
        info.Rate = entity.Rate;

        return info;
    }
}