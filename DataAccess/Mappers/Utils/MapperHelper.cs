﻿using Common.Infos.Interfaces;
using Common.Models.Interfaces;

namespace DataAccess.Mappers.Utils;

public static class MapperHelper
{
    internal static void MapId<T, TInfo>(this T entity, TInfo info)
        where T : IEntity
        where TInfo : IInfo
    {
        info.Id = entity.Id;
    }

    internal static void MapName<T, TInfo>(this T entity, TInfo info)
        where T : INamedEntity
        where TInfo : INamedInfo
    {
        info.Name = entity.Name;
        info.Description = entity.Description;
    }

    internal static void MapOrder<T, TInfo>(this T entity, TInfo info)
        where T : IOrderedEntity
        where TInfo : IOrderedInfo
    {
        info.Order = entity.Order;
    }

    internal static void MapFavorite<T, TInfo>(this T entity, TInfo info)
        where T : IFavoriteEntity
        where TInfo : IFavoriteInfo
    {
        info.IsFavorite = entity.IsFavorite;
    }

    internal static void MapChild<T, TParent, TInfo>(this T entity, TInfo info)
        where T : IChildEntity<TParent>
        where TParent : IEntity
        where TInfo : IChildInfo
    {
        info.ParentId = entity.Parent.Id;
    }

    internal static void MapParent<T, TChild, TInfo>(this T entity, TInfo info)
        where T : IParentEntity<TChild>
        where TChild : IEntity
        where TInfo : IParentInfo
    {
        info.ChildrenIds.AddRange(entity.Children.Select(e => e.Id));
    }

    internal static void MapReferenceEntity<T, TInfo>(this T entity, TInfo info)
        where T : IEntity, INamedEntity, IOrderedEntity, IFavoriteEntity
        where TInfo : IInfo, INamedInfo, IOrderedInfo, IFavoriteInfo
    {
        entity.MapId(info);
        entity.MapName(info);
        entity.MapOrder(info);
        entity.MapFavorite(info);
    }

    internal static void MapChildReferenceEntity<T, TParent, TInfo>(this T entity, TInfo info)
        where T : IEntity, INamedEntity, IOrderedEntity, IFavoriteEntity, IChildEntity<TParent>
        where TParent : IEntity
        where TInfo : IInfo, INamedInfo, IOrderedInfo, IFavoriteInfo, IChildInfo
    {
        entity.MapReferenceEntity(info);
        entity.MapChild<T, TParent, TInfo>(info);
    }

    internal static void MapParentReferenceEntity<T, TChild, TInfo>(this T entity, TInfo info)
        where T : IEntity, INamedEntity, IOrderedEntity, IFavoriteEntity, IParentEntity<TChild>
        where TChild : IEntity
        where TInfo : IInfo, INamedInfo, IOrderedInfo, IFavoriteInfo, IParentInfo
    {
        entity.MapReferenceEntity(info);
        entity.MapParent<T, TChild, TInfo>(info);
    }
}