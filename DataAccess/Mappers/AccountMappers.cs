﻿using Common.Infos;
using Common.Models;
using DataAccess.Mappers.Utils;

namespace DataAccess.Mappers;

public static class AccountMappers
{
    public static AccountGroupInfo Map(this AccountGroup entity)
    {
        var info = new AccountGroupInfo();
        entity.MapParentReferenceEntity<AccountGroup, AccountSubGroup, AccountGroupInfo>(info);
        return info;
    }

    public static AccountSubInfo Map(this AccountSubGroup entity)
    {
        var info = new AccountSubInfo();
        entity.MapChildReferenceEntity<AccountSubGroup, AccountGroup, AccountSubInfo>(info);
        entity.MapParent<AccountSubGroup, Account, AccountSubInfo>(info);
        return info;
    }

    public static AccountInfo Map(this Account entity)
    {
        var info = new AccountInfo();
        entity.MapChildReferenceEntity<Account, AccountSubGroup, AccountInfo>(info);

        info.CategoryId = entity.Category?.Id;

        info.CorrespondentId = entity.Correspondent?.Id;

        info.ProjectId = entity.Project?.Id;

        info.CurrencyId = entity.Currency.Id;

        return info;
    }
}