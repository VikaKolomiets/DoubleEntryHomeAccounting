﻿using Common.Infos;
using Common.Models;
using DataAccess.Mappers.Utils;

namespace DataAccess.Mappers;

public static class TemplateMappers
{
    public static TemplateGroupInfo Map(this TemplateGroup entity)
    {
        var info = new TemplateGroupInfo();
        entity.MapParentReferenceEntity<TemplateGroup, Template, TemplateGroupInfo>(info);
        return info;
    }

    public static TemplateInfo Map(this Template entity)
    {
        var info = new TemplateInfo();
        entity.MapChildReferenceEntity<Template, TemplateGroup, TemplateInfo>(info);

        info.EntriesIds.AddRange(entity.Entries.Select(e => e.Id));

        return info;
    }

    public static TemplateEntryInfo Map(this TemplateEntry entity)
    {
        var info = new TemplateEntryInfo();
        entity.MapId(info);

        info.TemplateId = entity.Template.Id;
        info.AccountId = entity.Account.Id;
        info.Amount = entity.Amount;

        return info;
    }
}