﻿using Common.Infos;
using Common.Models;
using DataAccess.Mappers.Utils;

namespace DataAccess.Mappers;

public static class CategoryMappers
{
    public static CategoryGroupInfo Map(this CategoryGroup entity)
    {
        var info = new CategoryGroupInfo();
        entity.MapParentReferenceEntity<CategoryGroup, Category, CategoryGroupInfo>(info);
        return info;
    }

    public static CategoryInfo Map(this Category entity)
    {
        var info = new CategoryInfo();
        entity.MapChildReferenceEntity<Category, CategoryGroup, CategoryInfo>(info);
        return info;
    }
}