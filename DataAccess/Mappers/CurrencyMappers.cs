﻿using Common.Infos;
using Common.Models;
using DataAccess.Mappers.Utils;

namespace DataAccess.Mappers;

public static class CurrencyMappers
{
    public static CurrencyInfo Map(this Currency entity)
    {
        var info = new CurrencyInfo();
        entity.MapId(info);
        entity.MapOrder(info);
        entity.MapFavorite(info);

        info.IsoCode = entity.IsoCode;
        info.Symbol = entity.Symbol;
        info.Name = entity.Name;

        info.RatesIds.AddRange(entity.Rates.Select(e => e.Id));

        return info;
    }

    public static CurrencyRateInfo Map(this CurrencyRate entity)
    {
        var info = new CurrencyRateInfo();
        entity.MapId(info);

        info.CurrencyId = entity.Currency.Id;
        info.Date = entity.Date;
        info.Rate = entity.Rate;
        info.Comment = entity.Comment;

        return info;
    }
}