﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.ProjectTests;

[TestFixture]
public class DeleteProjectTest
{
    [Test]
    public void DeleteProjectPositiveTest()
    {
        var isDeleted = false;

        var parent = new ProjectGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var deletedEntity = new Project
            { Id = Guid.NewGuid(), Name = "Name", Description = "", IsFavorite = true, Order = 2, Parent = parent };
        parent.Children.Add(deletedEntity);
        parent.Children.Add(new Project
        {
            Id = Guid.NewGuid(), Name = "Kate", Description = "Vacation", IsFavorite = false, Order = 3, Parent = parent
        });
        parent.Children.Add(new Project
        {
            Id = Guid.NewGuid(), Name = "Mom", Description = "Health", IsFavorite = true, Order = 1, Parent = parent
        });

        var mockEntityDataAccess = new Mock<IProjectDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => deletedEntity);
        mockEntityDataAccess.Setup(eda => eda.LoadParent(It.IsAny<Project>()));
        mockEntityDataAccess.Setup(eda => eda.Delete(It.IsAny<Project>())).Callback(() => isDeleted = true);

        mockEntityDataAccess.Setup(eda => eda.UpdateList(It.IsAny<List<Project>>()))
            .Callback(() => parent.Children.Remove(deletedEntity));

        var mockParentEntityDataAccess = new Mock<IProjectGroupDataAccess>();
        mockParentEntityDataAccess.Setup(pda => pda.LoadChildren(It.IsAny<ProjectGroup>()));

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetAccountsByProject(It.IsAny<Project>()))
            .Returns(() => new List<Account>
            {
                new() { Id = Guid.NewGuid(), Name = "NO" },
                new() { Id = Guid.NewGuid(), Name = "Yes" },
                new() { Id = Guid.NewGuid(), Name = "For" }
            });

        mockAccountDataAccess.Setup(ada => ada.Update(It.IsAny<Account>()));

        var service = new ProjectService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object, mockAccountDataAccess.Object);
        service.Delete(deletedEntity.Id);

        Assert.IsTrue(isDeleted);
        Assert.AreEqual(2, parent.Children.Count());
        Assert.AreEqual(2, parent.Children[0].Order);
    }

    [Test]
    public void DeleteProjectCheckAndGetEntityByIdTest()
    {
        var mockEntityDataAccess = new Mock<IProjectDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new ProjectService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(), CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Delete(Guid.NewGuid()));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private IProjectGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockParentEntityDataAccess = new Mock<IProjectGroupDataAccess>();
        return mockParentEntityDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}