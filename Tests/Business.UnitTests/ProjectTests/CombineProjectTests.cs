﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.ProjectTests;

[TestFixture]
public class CombineProjectTests
{
    [Test]
    public void CombineProjectPositiveTest()
    {
        var secondaryParent = new ProjectGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var primaryEntity = new Project
        {
            Id = Guid.NewGuid(), Name = "Fiona", Description = "", IsFavorite = false, Order = 1,
            Parent = secondaryParent
        };
        var secondaryEntity = new Project
        {
            Id = Guid.NewGuid(), Name = "Name", Description = "", IsFavorite = true, Order = 2, Parent = secondaryParent
        };
        secondaryParent.Children.Add(secondaryEntity);
        secondaryParent.Children.Add(new Project
        {
            Id = Guid.NewGuid(), Name = "Kate", Description = "Vacation", IsFavorite = false, Order = 3,
            Parent = secondaryParent
        });
        secondaryParent.Children.Add(new Project
        {
            Id = Guid.NewGuid(), Name = "Mom", Description = "Health", IsFavorite = true, Order = 1,
            Parent = secondaryParent
        });
        var accounts = new List<Account>
        {
            new() { Id = Guid.NewGuid(), Name = "What", Project = secondaryEntity },
            new() { Id = Guid.NewGuid(), Name = "Why", Project = secondaryEntity },
            new() { Id = Guid.NewGuid(), Name = "When", Project = secondaryEntity }
        };

        var mockEntityDataAccess = new Mock<IProjectDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>()))
            .Returns<Guid>(id => id == primaryEntity.Id ? primaryEntity : secondaryEntity);
        mockEntityDataAccess.Setup(eda => eda.LoadParent(It.IsAny<Project>()));
        mockEntityDataAccess.Setup(eda => eda.Delete(It.IsAny<Project>()));
        mockEntityDataAccess.Setup(eda => eda.UpdateList(It.IsAny<List<Project>>()))
            .Callback(() => secondaryParent.Children.Remove(secondaryEntity));

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetAccountsByProject(It.IsAny<Project>())).Returns(accounts);
        mockAccountDataAccess.Setup(ada => ada.Update(It.IsAny<Account>()))
            .Callback(() => accounts.ForEach(a => a.Project = primaryEntity));

        var mockParentEntityDataAccess = new Mock<IProjectGroupDataAccess>();
        mockParentEntityDataAccess.Setup(peda => peda.LoadChildren(It.IsAny<ProjectGroup>()));

        var service = new ProjectService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object, mockAccountDataAccess.Object);
        service.CombineTwoEntities(primaryEntity.Id, secondaryEntity.Id);

        Assert.AreEqual(2, secondaryParent.Children.Count);
        Assert.AreEqual(primaryEntity.Id, accounts[0].Project.Id);
        Assert.AreEqual(primaryEntity.Name, accounts[1].Project.Name);
    }

    [Test]
    public void CombineProjectCheckAndGetEntityByIdExceptionTest()
    {
        var mockEntityDataAccess = new Mock<IProjectDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new ProjectService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(), CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.CombineTwoEntities(Guid.NewGuid(), Guid.NewGuid()));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private IProjectGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockParentEntityDataAccess = new Mock<IProjectGroupDataAccess>();
        return mockParentEntityDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}