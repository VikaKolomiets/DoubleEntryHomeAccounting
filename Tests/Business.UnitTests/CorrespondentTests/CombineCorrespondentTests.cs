﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.CorrespondentTests;

[TestFixture]
public class CombineCorrespondentTests
{
    [Test]
    public void CombineCorrespondentPositiveTest()
    {
        var secondaryParent = new CorrespondentGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var primaryEntity = new Correspondent
        {
            Id = Guid.NewGuid(), Name = "Fiona", Description = "", IsFavorite = false, Order = 1,
            Parent = secondaryParent
        };
        var secondaryEntity = new Correspondent
        {
            Id = Guid.NewGuid(), Name = "Name", Description = "", IsFavorite = true, Order = 2, Parent = secondaryParent
        };
        secondaryParent.Children.Add(secondaryEntity);
        secondaryParent.Children.Add(new Correspondent
        {
            Id = Guid.NewGuid(), Name = "Kate", Description = "Vacation", IsFavorite = false, Order = 3,
            Parent = secondaryParent
        });
        secondaryParent.Children.Add(new Correspondent
        {
            Id = Guid.NewGuid(), Name = "Mom", Description = "Health", IsFavorite = true, Order = 1,
            Parent = secondaryParent
        });
        var accounts = new List<Account>
        {
            new() { Id = Guid.NewGuid(), Name = "What", Correspondent = secondaryEntity },
            new() { Id = Guid.NewGuid(), Name = "Why", Correspondent = secondaryEntity },
            new() { Id = Guid.NewGuid(), Name = "When", Correspondent = secondaryEntity }
        };

        var mockEntityDataAccess = new Mock<ICorrespondentDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>()))
            .Returns<Guid>(id => id == primaryEntity.Id ? primaryEntity : secondaryEntity);
        mockEntityDataAccess.Setup(eda => eda.LoadParent(It.IsAny<Correspondent>()));
        mockEntityDataAccess.Setup(eda => eda.Delete(It.IsAny<Correspondent>()));
        mockEntityDataAccess.Setup(eda => eda.UpdateList(It.IsAny<List<Correspondent>>()))
            .Callback(() => secondaryParent.Children.Remove(secondaryEntity));

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetAccountsByCorrespondent(It.IsAny<Correspondent>())).Returns(accounts);
        mockAccountDataAccess.Setup(ada => ada.Update(It.IsAny<Account>()))
            .Callback(() => accounts.ForEach(a => a.Correspondent = primaryEntity));

        var mockParentEntityDataAccess = new Mock<ICorrespondentGroupDataAccess>();
        mockParentEntityDataAccess.Setup(peda => peda.LoadChildren(It.IsAny<CorrespondentGroup>()));

        var service = new CorrespondentService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object, mockAccountDataAccess.Object);
        service.CombineTwoEntities(primaryEntity.Id, secondaryEntity.Id);

        Assert.AreEqual(2, secondaryParent.Children.Count);
        Assert.AreEqual(primaryEntity.Id, accounts[0].Correspondent.Id);
        Assert.AreEqual(primaryEntity.Name, accounts[1].Correspondent.Name);
    }

    [Test]
    public void CombineCorrespondentCheckAndGetEntityByIdExceptionTest()
    {
        var mockEntityDataAccess = new Mock<ICorrespondentDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new CorrespondentService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(), CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.CombineTwoEntities(Guid.NewGuid(), Guid.NewGuid()));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private ICorrespondentGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockParentEntityDataAccess = new Mock<ICorrespondentGroupDataAccess>();
        return mockParentEntityDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}