﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.CategoryGroupTests;

[TestFixture]
public class DeleteCategoryGroupTests
{
    [Test]
    public void DeleteCategoryGroupPositiveTest()
    {
        var isDeleted = false;
        var deletedEntity = new CategoryGroup
        {
            Id = Guid.NewGuid(),
            Name = "Second",
            Description = "Test",
            IsFavorite = true
        };
        var reoderedGroups = new List<CategoryGroup>
        {
            new() { Id = Guid.NewGuid(), Name = "First", Order = 2 },
            new() { Id = Guid.NewGuid(), Name = "Third", Order = 3 }
        };

        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => deletedEntity);
        mockEntityDataAccess.Setup(eda => eda.Delete(It.IsAny<CategoryGroup>())).Callback(() => isDeleted = true);
        mockEntityDataAccess.Setup(eda => eda.GetList()).Returns(reoderedGroups);
        mockEntityDataAccess.Setup(eda => eda.UpdateList(It.IsAny<List<CategoryGroup>>()));

        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());
        service.Delete(deletedEntity.Id);

        Assert.AreEqual(1, reoderedGroups[0].Order);
        Assert.AreEqual(2, reoderedGroups[1].Order);
        Assert.IsTrue(isDeleted);
    }

    [Test]
    public void DeleteCategoryGroupCheckAndGetEntityByIdExceptionTest()
    {
        var deletedEntity = new CategoryGroup();

        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Delete(deletedEntity.Id));
    }

    [Test]
    public void DeleteCategoryGroupСheckExistedChildrenInTheGroupExceptionTest()
    {
        var deletedEntity = new CategoryGroup();
        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => deletedEntity);
        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccessForException());

        Assert.ThrowsAsync<ArgumentException>(async () => await service.Delete(deletedEntity.Id));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private ICategoryDataAccess CreateMockChildEntityDataAccessForException()
    {
        var mockChildEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockChildEntityDataAccess.Setup(cda => cda.GetCount(It.IsAny<Guid>())).Returns(() => 5);
        return mockChildEntityDataAccess.Object;
    }

    private ICategoryDataAccess CreateMockChildEntityDataAccess()
    {
        var mockChildEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockChildEntityDataAccess.Setup(cda => cda.GetCount(It.IsAny<Guid>())).Returns(() => 0);
        return mockChildEntityDataAccess.Object;
    }
}