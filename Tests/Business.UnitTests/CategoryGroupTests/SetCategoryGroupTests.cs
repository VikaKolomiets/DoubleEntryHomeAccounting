﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.CategoryGroupTests;

[TestFixture]
public class SetCategoryGroupTests
{
    [Test]
    public void SetFavoriteStatusCategoryGroupPositiveTest()
    {
        var entity = new CategoryGroup
        {
            Id = Guid.NewGuid(),
            Name = "Second",
            Description = "Test",
            IsFavorite = true
        };
        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockEntityDataAccess.Setup(eda => eda.Update(It.IsAny<CategoryGroup>()));
        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());

        service.SetFavoriteStatus(entity.Id, false);
        Assert.AreEqual(false, entity.IsFavorite);
    }

    [Test]
    public void SetFavoriteStatusCategoryGroupExceptionTest()
    {
        var entity = new CategoryGroup
        {
            Id = Guid.NewGuid(),
            Name = "Second",
            Description = "Test",
            IsFavorite = true
        };
        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetFavoriteStatus(entity.Id, false));
    }

    [TestCase(3)]
    [TestCase(1)]
    public void SetOrderCategoryGroupPositiveTest(int order)
    {
        var entity = new CategoryGroup
        {
            Id = Guid.NewGuid(),
            Name = "Second",
            Description = "Test",
            Order = 2
        };
        var reoderedGroups = new List<CategoryGroup>
        {
            new() { Id = Guid.NewGuid(), Name = "First", Order = 1 },
            new() { Id = Guid.NewGuid(), Name = "Third", Order = 3 }
        };
        reoderedGroups.Add(entity);

        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockEntityDataAccess.Setup(eda => eda.GetList()).Returns(() => reoderedGroups);
        mockEntityDataAccess.Setup(eda => eda.UpdateList(It.IsAny<List<CategoryGroup>>()));
        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());

        service.SetOrder(entity.Id, order);
        Assert.AreEqual(order, entity.Order);
    }

    [Test]
    public void SetOrderCategoryGroupExceptionTest()
    {
        var entity = new CategoryGroup
        {
            Id = Guid.NewGuid(),
            Name = "Second",
            Description = "Test",
            Order = 1
        };
        var reoderedGroups = new List<CategoryGroup>
        {
            new() { Id = Guid.NewGuid(), Name = "First", Order = 2 },
            new() { Id = Guid.NewGuid(), Name = "Third", Order = 3 }
        };
        reoderedGroups.Add(entity);
        var order = 3;
        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOrder(entity.Id, order));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private ICategoryDataAccess CreateMockChildEntityDataAccess()
    {
        var mockChildEntityDataAccess = new Mock<ICategoryDataAccess>();
        return mockChildEntityDataAccess.Object;
    }
}