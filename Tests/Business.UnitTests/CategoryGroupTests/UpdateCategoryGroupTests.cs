﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.CategoryGroupTests;

[TestFixture]
public class UpdateCategoryGroupTests
{
    [TestCase("Name", "Description", true, "Mom", "All", false)]
    [TestCase("Dad", "For all family", false, "Child", "school", true)]
    public void UpdateCategoryGroupPositiveTest(string newName, string newDescription, bool newIsFavorite
        , string originalName, string originalDescription, bool originalIsFavorite)
    {
        var originalEntity = new CategoryGroup
        {
            Name = originalName,
            Description = originalDescription,
            IsFavorite = originalIsFavorite
        };
        var newEntity = new CategoryGroup
        {
            Id = originalEntity.Id,
            Name = newName,
            Description = newDescription,
            IsFavorite = newIsFavorite
        };

        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.GetByName(It.IsAny<string>())).Returns(() => new List<CategoryGroup>());
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => originalEntity);
        mockEntityDataAccess.Setup(eda => eda.Update(It.IsAny<CategoryGroup>()));

        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());

        service.Update(newEntity);

        Assert.AreEqual(newEntity.Name, originalEntity.Name);
        Assert.AreEqual(newEntity.Description, originalEntity.Description);
        Assert.AreEqual(newEntity.IsFavorite, originalEntity.IsFavorite);
    }

    [Test]
    public void ExceptionUpdateCategoryGroupCheckEntityNullTest()
    {
        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());

        CategoryGroup entity = null;
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void ExceptionUpdateCategoryGroupCheckEntityNameForNullTest()
    {
        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());

        var entity = new CategoryGroup { Name = null };
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void ExceptionUpdateCategoryGroupCheckEntityWithSameNameTest()
    {
        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();

        mockEntityDataAccess.Setup(eda => eda.GetByName(It.IsAny<string>()))
            .Returns(() => new List<CategoryGroup> { new() });
        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());

        var entity = new CategoryGroup { Name = "Dasha", Description = "Super", IsFavorite = true };
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void ExceptionUpdateCategoryGroupCheckGetEntityByIdTest()
    {
        var mockEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.GetByName(It.IsAny<string>())).Returns(() => new List<CategoryGroup>());
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new CategoryGroupService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockChildEntityDataAccess());

        var entity = new CategoryGroup
            { Id = Guid.NewGuid(), Name = "Dasha", Description = "Super", IsFavorite = true };
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private ICategoryDataAccess CreateMockChildEntityDataAccess()
    {
        var mockChildEntityDataAccess = new Mock<ICategoryDataAccess>();
        return mockChildEntityDataAccess.Object;
    }
}