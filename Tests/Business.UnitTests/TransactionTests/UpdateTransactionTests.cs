﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Common.Models.Enums;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.TransactionTests;

[TestFixture]
public class UpdateTransactionTests
{
    [Test]
    public void UpdateTransactionPositiveTest()
    {
        var updatedEntity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2023, 01, 19), Comment = "", State = TransactionState.NoValid
        };
        var entity = new Transaction
        {
            Id = updatedEntity.Id, DateTime = new DateTime(2023, 04, 19), Comment = "Comment",
            State = TransactionState.Planned
        };
        var entityCurrency = new Currency { IsoCode = "CurrencyIsoCode" };
        var rateCurrency = new CurrencyRate { Rate = 1, Currency = entityCurrency };
        entityCurrency.Rates.Add(rateCurrency);

        var accounts = new List<Account>();
        var first = new Account { Id = Guid.NewGuid(), Name = "What", Currency = entityCurrency };
        var third = new Account { Id = Guid.NewGuid(), Name = "How", Currency = entityCurrency };
        accounts.Add(first);
        accounts.Add(third);

        var entryFirst = new TransactionEntry { Transaction = entity, Account = first, Amount = 2500, Rate = 1 };
        var entryThird = new TransactionEntry { Transaction = entity, Account = third, Amount = -2800, Rate = 1 };
        var entrySecond = new TransactionEntry { Transaction = entity, Account = first, Amount = 300, Rate = 1 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMainCurrencyIsoCode()).Returns(() => "CurrencyIsoCode");

        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockTransactionDataAccess.Setup(tda => tda.Update(It.IsAny<Transaction>()))
            .Callback(() => updatedEntity = entity);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns<Guid>(g => g == first.Id ? first : third);
        mockAccountDataAccess.Setup(ada => ada.LoadCurrency(It.IsAny<Account>()))
            .Callback<Account>(a => a.Currency.IsoCode = "CurrencyIsoCode");

        var service = new TransactionService(mockTransactionDataAccess.Object,
            mockAccountDataAccess.Object,
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        service.Update(entity);

        Assert.AreEqual(entity.State, updatedEntity.State);
        Assert.AreEqual(entity.Entries.Count, updatedEntity.Entries.Count);
        Assert.AreEqual(entity.DateTime, updatedEntity.DateTime);
        Assert.AreEqual(entrySecond.Account, updatedEntity.Entries.FirstOrDefault(e => e.Amount == 300m).Account);
    }

    [Test]
    public void UpdateTransactionCheckInputForNullExceptionTest()
    {
        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            CreateSystemConfigDataAccess(),
            CreateGlobalDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(null));
    }

    [TestCase(2019)]
    [TestCase(2000)]
    public void UpdateTransactionCheckInputDataMinRangeExceptionTest(int year)
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(year, 04, 19), Comment = "Comment",
            State = TransactionState.Planned
        };
        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());

        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(entity));
    }

    [TestCase(2051)]
    [TestCase(3000)]
    public void UpdateTransactionCheckInputDataMaxRangeExceptionTest(int year)
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(year, 04, 19), Comment = "Comment",
            State = TransactionState.Planned
        };
        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());

        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTransactionCheckEntriesNullExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), Comment = "Comment",
            State = TransactionState.Planned, Entries = null
        };

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTransactionCheckEntriesCountLessThanTwoExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), Comment = "Commt",
            State = TransactionState.Planned
        };
        var entryFirst = new TransactionEntry { Transaction = entity, Amount = 2500, Rate = 1 };
        entity.Entries.Add(entryFirst);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTransactionCheckContainEntitiesStateExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = (TransactionState)100, Comment = "Commt"
        };
        var entryFirst = new TransactionEntry { Transaction = entity, Amount = 2500, Rate = 1 };
        var entryThird = new TransactionEntry { Transaction = entity, Amount = -2800, Rate = 1 };
        var entrySecond = new TransactionEntry { Transaction = entity, Amount = 300, Rate = 1 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTransactionCheckAndGetEntityByIdExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = TransactionState.Draft,
            Comment = "Commt"
        };
        var entryFirst = new TransactionEntry { Transaction = entity, Amount = 2500, Rate = 1 };
        var entryThird = new TransactionEntry { Transaction = entity, Amount = -2800, Rate = 1 };
        var entrySecond = new TransactionEntry { Transaction = entity, Amount = 300, Rate = 1 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new TransactionService(mockTransactionDataAccess.Object,
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [TestCase(0)]
    [TestCase(-1)]
    public void UpdateTransactionCheckCurrencyRateDataExceptionTest(int rate)
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = TransactionState.Draft,
            Comment = "Commt"
        };
        var entryFirst = new TransactionEntry { Transaction = entity, Amount = 2500, Rate = rate };
        var entryThird = new TransactionEntry { Transaction = entity, Amount = -2800, Rate = rate };
        var entrySecond = new TransactionEntry { Transaction = entity, Amount = 300, Rate = rate };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMainCurrencyIsoCode()).Returns(() => "CurrencyIsoCode");

        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => entity);

        var service = new TransactionService(mockTransactionDataAccess.Object,
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTransactionCheckInputForNullAccountExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = TransactionState.Draft,
            Comment = "Commt"
        };
        var entryFirst = new TransactionEntry { Transaction = entity, Amount = 2500, Rate = 1 };
        var entryThird = new TransactionEntry { Transaction = entity, Amount = -2800, Rate = 1 };
        var entrySecond = new TransactionEntry { Transaction = entity, Amount = 300, Rate = 1 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMainCurrencyIsoCode()).Returns(() => "CurrencyIsoCode");

        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => entity);

        var service = new TransactionService(mockTransactionDataAccess.Object,
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTransactionCheckAndGetEntityByIdAccountExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = TransactionState.Draft,
            Comment = "Commt"
        };
        var entityCurrency = new Currency { IsoCode = "CurrencyIsoCode" };
        var rateCurrency = new CurrencyRate { Rate = 1, Currency = entityCurrency };
        entityCurrency.Rates.Add(rateCurrency);

        var accounts = new List<Account>();
        var first = new Account { Id = Guid.NewGuid(), Name = "What", Currency = entityCurrency };
        var third = new Account { Id = Guid.NewGuid(), Name = "How", Currency = entityCurrency };
        accounts.Add(first);
        accounts.Add(third);

        var entryFirst = new TransactionEntry { Transaction = entity, Account = first, Amount = 2500, Rate = 1 };
        var entryThird = new TransactionEntry { Transaction = entity, Account = third, Amount = -2800, Rate = 1 };
        var entrySecond = new TransactionEntry { Transaction = entity, Account = first, Amount = 300, Rate = 1 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMainCurrencyIsoCode()).Returns(() => "CurrencyIsoCode");

        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => entity);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new TransactionService(mockTransactionDataAccess.Object,
            mockAccountDataAccess.Object,
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTransactionCheckCurrencyRateIsoCodeExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = TransactionState.Draft,
            Comment = "Commt"
        };
        var entityCurrency = new Currency { IsoCode = "CurrencyIsoCode" };
        var rateCurrency = new CurrencyRate { Rate = 1, Currency = entityCurrency };
        entityCurrency.Rates.Add(rateCurrency);

        var accounts = new List<Account>();
        var first = new Account { Id = Guid.NewGuid(), Name = "What", Currency = entityCurrency };
        var third = new Account { Id = Guid.NewGuid(), Name = "How", Currency = entityCurrency };
        accounts.Add(first);
        accounts.Add(third);

        var entryFirst = new TransactionEntry { Transaction = entity, Account = first, Amount = 2500, Rate = 1.01m };
        var entryThird = new TransactionEntry { Transaction = entity, Account = third, Amount = -2800, Rate = 1.01m };
        var entrySecond = new TransactionEntry { Transaction = entity, Account = first, Amount = 300, Rate = 1.01m };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMainCurrencyIsoCode()).Returns(() => "CurrencyIsoCode");

        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => entity);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>()))
            .Returns<Guid>(id => id == first.Id ? first : third);
        mockAccountDataAccess.Setup(ada => ada.LoadCurrency(It.IsAny<Account>()))
            .Callback<Account>(a => a.Currency.IsoCode = entityCurrency.IsoCode);

        var service = new TransactionService(mockTransactionDataAccess.Object,
            mockAccountDataAccess.Object,
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(entity));
    }

    private ITransactionDataAccess CreateTransactionDataAccess()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        return mockTransactionDataAccess.Object;
    }

    private IAccountDataAccess CreateAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private ISystemConfigDataAccess CreateSystemConfigDataAccess()
    {
        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        return mockSystemConfigDataAccess.Object;
    }

    private IGlobalDataAccess CreateGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }
}