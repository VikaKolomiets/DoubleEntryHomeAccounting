﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Common.Models.Enums;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.TransactionTests;

[TestFixture]
public class DeleteTransactionTests
{
    [Test]
    public void DeleteTransactionPositiveTest()
    {
        var isDeleted = false;
        var deletedEntity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), Comment = "", State = TransactionState.Planned,
            Entries = null
        };

        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(deletedEntity);
        mockTransactionDataAccess.Setup(tda => tda.Delete(It.IsAny<Transaction>())).Callback(() => isDeleted = true);

        var service = new TransactionService(mockTransactionDataAccess.Object,
            CreateAccountDataAccess(),
            CreateSystemConfigDataAccess(),
            CreateGlobalDataAccess());
        service.Delete(deletedEntity.Id);
        Assert.IsTrue(isDeleted);
    }

    [Test]
    public void DeleteTransactionCheckAndGetEntityByIdExceptionTest()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new TransactionService(mockTransactionDataAccess.Object,
            CreateAccountDataAccess(),
            CreateSystemConfigDataAccess(),
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Delete(Guid.NewGuid()));
    }

    private ITransactionDataAccess CreateTransactionDataAccess()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        return mockTransactionDataAccess.Object;
    }

    private IAccountDataAccess CreateAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private ISystemConfigDataAccess CreateSystemConfigDataAccess()
    {
        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        return mockSystemConfigDataAccess.Object;
    }

    private IGlobalDataAccess CreateGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }
}