﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Common.Models.Enums;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.TransactionTests;

[TestFixture]
public class AddTransactionTests
{
    [TestCase(300)]
    [TestCase(0)]
    public void AddTransactionPositiveTest(decimal checkAmount)
    {
        Transaction addedEntity = null;
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2023, 04, 19), Comment = "Commt",
            State = TransactionState.Planned
        };

        var entityCurrency = new Currency { IsoCode = "CurrencyIsoCode" };
        var rateCurrency = new CurrencyRate { Rate = 1, Currency = entityCurrency };
        entityCurrency.Rates.Add(rateCurrency);

        var accounts = new List<Account>();
        var first = new Account { Id = Guid.NewGuid(), Name = "What", Currency = entityCurrency };
        var third = new Account { Id = Guid.NewGuid(), Name = "How many", Currency = entityCurrency };
        accounts.Add(first);
        accounts.Add(third);

        var entryFirst = new TransactionEntry { Transaction = entity, Account = first, Amount = 2500, Rate = 1 };
        var entryThird = new TransactionEntry { Transaction = entity, Account = third, Amount = -2800, Rate = 1 };
        var entrySecond = new TransactionEntry
            { Transaction = entity, Account = first, Amount = checkAmount, Rate = 1 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMainCurrencyIsoCode()).Returns(() => "CurrencyIsoCode");

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns<Guid>(g => g == first.Id ? first : third);
        mockAccountDataAccess.Setup(ada => ada.LoadCurrency(It.IsAny<Account>()))
            .Callback<Account>(a => a.Currency.IsoCode = "CurrencyIsoCode");

        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Add(It.IsAny<Transaction>())).Callback(() => addedEntity = entity);

        var service = new TransactionService(mockTransactionDataAccess.Object,
            mockAccountDataAccess.Object,
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        service.Add(entity);

        Assert.AreEqual(entity.Entries.Count, addedEntity.Entries.Count);
        Assert.AreEqual(entity.State, addedEntity.State);
        Assert.AreEqual(entity.Comment, addedEntity.Comment);
        Assert.AreEqual(entity.Id, addedEntity.Id);
    }

    [Test]
    public void AddTransactionCheckInputForNullExceptionTest()
    {
        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            CreateSystemConfigDataAccess(),
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(null));
    }

    [TestCase(1985)]
    [TestCase(2051)]
    public void AddTransactionCheckDateTimeExceptionTest(int year)
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(year, 04, 19), Comment = "Commt",
            State = TransactionState.Planned
        };

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTransactionCheckEntriesCountLessTwoExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), Comment = "Commt",
            State = TransactionState.Planned
        };
        var entryFirst = new TransactionEntry { Transaction = entity, Amount = 2500, Rate = 1 };
        entity.Entries.Add(entryFirst);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTransactionCheckEntriesNullExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), Comment = "Commt",
            State = TransactionState.Planned, Entries = null
        };

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTransactionCheckContainEntitiesStateExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = (TransactionState)100, Comment = "Commt"
        };
        var entryFirst = new TransactionEntry { Transaction = entity, Amount = 2500, Rate = 1 };
        var entryThird = new TransactionEntry { Transaction = entity, Amount = -2800, Rate = 1 };
        var entrySecond = new TransactionEntry { Transaction = entity, Amount = 300, Rate = 1 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTransactionCheckEntityWithSameIdExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = TransactionState.Draft,
            Comment = "Commt"
        };
        var entryFirst = new TransactionEntry { Transaction = entity, Amount = 2500, Rate = 1 };
        var entryThird = new TransactionEntry { Transaction = entity, Amount = -2800, Rate = 1 };
        var entrySecond = new TransactionEntry { Transaction = entity, Amount = 300, Rate = 1 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));

        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => new Transaction { Id = entity.Id });

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            mockGlobalDataAccess.Object);
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }

    [TestCase(0)]
    [TestCase(-1)]
    public void AddTransactionCheckCurrencyRateDataExceptionTest(int rate)
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = TransactionState.Draft,
            Comment = "Commt"
        };
        var entryFirst = new TransactionEntry { Transaction = entity, Amount = 2500, Rate = rate };
        var entryThird = new TransactionEntry { Transaction = entity, Amount = -2800, Rate = rate };
        var entrySecond = new TransactionEntry { Transaction = entity, Amount = 300, Rate = rate };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMainCurrencyIsoCode()).Returns(() => "CurrencyIsoCode");

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }


    [Test]
    public void AddTransactionCheckInputForNullAccountExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = TransactionState.Draft,
            Comment = "Commt"
        };
        var entryFirst = new TransactionEntry { Transaction = entity, Amount = 2500, Rate = 1 };
        var entryThird = new TransactionEntry { Transaction = entity, Amount = -2800, Rate = 1 };
        var entrySecond = new TransactionEntry { Transaction = entity, Amount = 300, Rate = 1 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMainCurrencyIsoCode()).Returns(() => "CurrencyIsoCode");

        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTransactionCheckAndGetEntityByIdAccountExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = TransactionState.Draft,
            Comment = "Commt"
        };
        var entityCurrency = new Currency { IsoCode = "CurrencyIsoCode" };
        var rateCurrency = new CurrencyRate { Rate = 1, Currency = entityCurrency };
        entityCurrency.Rates.Add(rateCurrency);

        var accounts = new List<Account>();
        var first = new Account { Id = Guid.NewGuid(), Name = "What", Currency = entityCurrency };
        var third = new Account { Id = Guid.NewGuid(), Name = "How many", Currency = entityCurrency };
        accounts.Add(first);
        accounts.Add(third);
        var entryFirst = new TransactionEntry { Transaction = entity, Account = first, Amount = 2500, Rate = 1 };
        var entryThird = new TransactionEntry { Transaction = entity, Account = third, Amount = -2800, Rate = 1 };
        var entrySecond = new TransactionEntry { Transaction = entity, Account = first, Amount = 300, Rate = 1 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMainCurrencyIsoCode()).Returns(() => "CurrencyIsoCode");

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new TransactionService(CreateTransactionDataAccess(),
            mockAccountDataAccess.Object,
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTransactionCheckEntryRateForOneExceptionTest()
    {
        var entity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2020, 04, 19), State = TransactionState.Draft,
            Comment = "Commt"
        };
        var entityCurrency = new Currency { IsoCode = "CurrencyIsoCode" };
        var rateCurrency = new CurrencyRate { Rate = 0.05m, Currency = entityCurrency };
        entityCurrency.Rates.Add(rateCurrency);

        var accounts = new List<Account>();
        var first = new Account { Id = Guid.NewGuid(), Name = "What", Currency = entityCurrency };
        var third = new Account { Id = Guid.NewGuid(), Name = "How many", Currency = entityCurrency };
        accounts.Add(first);
        accounts.Add(third);
        var entryFirst = new TransactionEntry { Transaction = entity, Account = first, Amount = 2500, Rate = 0.05m };
        var entryThird = new TransactionEntry { Transaction = entity, Account = third, Amount = -2800, Rate = 0.05m };
        var entrySecond = new TransactionEntry { Transaction = entity, Account = first, Amount = 300, Rate = 0.05m };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        mockSystemConfigDataAccess.Setup(scda => scda.GetMinDate()).Returns(() => new DateTime(2020, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMaxDate()).Returns(() => new DateTime(2050, 01, 01));
        mockSystemConfigDataAccess.Setup(scda => scda.GetMainCurrencyIsoCode()).Returns(() => "CurrencyIsoCode");

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns<Guid>(g => g == first.Id ? first : third);
        mockAccountDataAccess.Setup(ada => ada.LoadCurrency(It.IsAny<Account>()))
            .Callback<Account>(a => a.Currency.IsoCode = "CurrencyIsoCode");

        var service = new TransactionService(CreateTransactionDataAccess(),
            mockAccountDataAccess.Object,
            mockSystemConfigDataAccess.Object,
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }

    private ITransactionDataAccess CreateTransactionDataAccess()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        return mockTransactionDataAccess.Object;
    }

    private IAccountDataAccess CreateAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private ISystemConfigDataAccess CreateSystemConfigDataAccess()
    {
        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        return mockSystemConfigDataAccess.Object;
    }

    private IGlobalDataAccess CreateGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }
}