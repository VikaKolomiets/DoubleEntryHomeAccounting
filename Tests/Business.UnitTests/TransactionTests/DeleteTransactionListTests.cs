﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Common.Models.Enums;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.TransactionTests;

[TestFixture]
public class DeleteTransactionListTests
{
    [Test]
    public void DeleteTransactionListPositiveTest()
    {
        var isListDeleted = false;
        var firstEntity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2023, 01, 19), Comment = "", State = TransactionState.Draft
        };
        var secondEntity = new Transaction
        {
            Id = Guid.NewGuid(), DateTime = new DateTime(2023, 02, 19), Comment = "", State = TransactionState.Planned
        };

        var deletedEntities = new List<Transaction> { firstEntity, secondEntity };
        var deletedEntityGuids = new List<Guid> { firstEntity.Id, secondEntity.Id };

        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>()))
            .Returns<Guid>(id => id == firstEntity.Id ? firstEntity : secondEntity);
        mockTransactionDataAccess.Setup(tda => tda.DeleteList(It.IsAny<List<Transaction>>()))
            .Callback(() => isListDeleted = true);

        var service = new TransactionService(mockTransactionDataAccess.Object,
            CreateAccountDataAccess(),
            CreateSystemConfigDataAccess(),
            CreateGlobalDataAccess());
        service.DeleteTransactionList(deletedEntityGuids);
        Assert.IsTrue(isListDeleted);
    }

    [Test]
    public void DeleteTransactionListCheckAndGetEntityByIdExceptionTest()
    {
        var deletedEntityGuids = new List<Guid> { Guid.NewGuid(), Guid.NewGuid() };
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new TransactionService(mockTransactionDataAccess.Object,
            CreateAccountDataAccess(),
            CreateSystemConfigDataAccess(),
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.DeleteTransactionList(deletedEntityGuids));
    }

    [Test]
    public void DeleteTransactionListCheckInputForNullExceptionTest()
    {
        var service = new TransactionService(CreateTransactionDataAccess(),
            CreateAccountDataAccess(),
            CreateSystemConfigDataAccess(),
            CreateGlobalDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.DeleteTransactionList(null));
    }

    private ITransactionDataAccess CreateTransactionDataAccess()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        return mockTransactionDataAccess.Object;
    }

    private IAccountDataAccess CreateAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private ISystemConfigDataAccess CreateSystemConfigDataAccess()
    {
        var mockSystemConfigDataAccess = new Mock<ISystemConfigDataAccess>();
        return mockSystemConfigDataAccess.Object;
    }

    private IGlobalDataAccess CreateGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }
}