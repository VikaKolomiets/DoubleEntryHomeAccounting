﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountSubGroupTests;

[TestFixture]
public class UpdateAccountSubGroupTests
{
    [Test]
    public void UpdateAccountSubGroupPositiveTest()
    {
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Rarely" };
        var updatedSubgroup = new AccountSubGroup { Id = Guid.NewGuid() };
        var subgroup = new AccountSubGroup
            { Id = updatedSubgroup.Id, Name = "Vacation", Description = "", IsFavorite = false, Parent = parent };
        parent.Children.Add(subgroup);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => subgroup);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.LoadParent(It.IsAny<AccountSubGroup>()));
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<AccountSubGroup> { subgroup });
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Update(It.IsAny<AccountSubGroup>()))
            .Callback<AccountSubGroup>(asg => updatedSubgroup = asg);


        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , CreateMockAccountGroupDataAccess());
        service.Update(subgroup);
        Assert.AreEqual(subgroup.Name, updatedSubgroup.Name);
        Assert.AreEqual(subgroup.Parent, updatedSubgroup.Parent);
        Assert.AreEqual(subgroup.IsFavorite, updatedSubgroup.IsFavorite);
        Assert.AreEqual(subgroup.Description, updatedSubgroup.Description);
    }

    [Test]
    public void UpdateAccountSubGroupCheckInputForNullExceptiobTest()
    {
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(null));
    }

    [Test]
    public void UpdateAccountSubGroupCheckInputNameForNullExceptiobTest()
    {
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.Update(new AccountSubGroup { Id = Guid.NewGuid(), Name = null }));
    }

    [Test]
    public void UpdateAccountSubGroupCheckInputForNullParentExceptiobTest()
    {
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.Update(new AccountSubGroup { Id = Guid.NewGuid(), Name = "Viki", Parent = null }));
    }

    [Test]
    public void UpdateAccountSubGroupCheckAndGetEntityByIdExceptiobTest()
    {
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Rarely" };
        var subgroup = new AccountSubGroup
            { Id = Guid.NewGuid(), Name = "Vacation", Description = "", IsFavorite = false, Parent = parent };
        parent.Children.Add(subgroup);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(subgroup));
    }

    [Test]
    public void UpdateAccountSubGroupCheckEntityWithSameNameExceptiobTest()
    {
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Rarely" };
        var subgroup = new AccountSubGroup
            { Id = Guid.NewGuid(), Name = "Vacation", Description = "", IsFavorite = false, Parent = parent };
        parent.Children.Add(subgroup);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => subgroup);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<AccountSubGroup> { new() { Id = Guid.NewGuid(), Name = "Vacation" } });
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(subgroup));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private IAccountGroupDataAccess CreateMockAccountGroupDataAccess()
    {
        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        return mockAccountGroupDataAccess.Object;
    }
}