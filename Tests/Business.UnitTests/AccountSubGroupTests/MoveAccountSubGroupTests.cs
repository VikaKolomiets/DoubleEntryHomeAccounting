﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountSubGroupTests;

[TestFixture]
public class MoveAccountSubGroupTests
{
    [TestCase(0)]
    [TestCase(120)]
    public void MoveAccountSubGroupPositiveTest(int order)
    {
        var isMoved = false;
        var toParent = new AccountGroup { Id = Guid.NewGuid(), Name = "Support" };
        var fromParent = new AccountGroup { Id = Guid.NewGuid(), Name = "Main" };
        var firstEntity = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Home", Order = 1, Parent = fromParent };
        var movedEntity = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Jewelry", Order = 3, Parent = fromParent };
        var secondEntity = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Work", Order = 2, Parent = fromParent };
        fromParent.Children.Add(movedEntity);
        fromParent.Children.Add(secondEntity);
        fromParent.Children.Add(firstEntity);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => movedEntity);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.LoadParent(It.IsAny<AccountSubGroup>()))
            .Callback<AccountSubGroup>(asg => fromParent = asg.Parent);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<AccountSubGroup>());
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.GetMaxOrder(It.IsAny<Guid>())).Returns(() => order);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Update(It.IsAny<AccountSubGroup>()))
            .Callback(() => isMoved = true);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.UpdateList(It.IsAny<List<AccountSubGroup>>()));

        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        mockAccountGroupDataAccess.Setup(agda => agda.Get(It.IsAny<Guid>())).Returns(() => toParent);
        mockAccountGroupDataAccess.Setup(agda => agda.LoadChildren(It.IsAny<AccountGroup>()));

        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , mockAccountGroupDataAccess.Object);
        service.MoveToAnotherParent(movedEntity.Id, toParent.Id);

        Assert.IsTrue(isMoved);
        Assert.AreEqual(order + 1, movedEntity.Order);
        Assert.AreEqual(2, fromParent.Children.Count);
        Assert.AreEqual(1, toParent.Children.Count);
        Assert.AreEqual(toParent.Name, movedEntity.Parent.Name);
    }

    [Test]
    public void MoveAccountSubGroupCheckAndGetEntityByIdTest()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.MoveToAnotherParent(Guid.NewGuid(), Guid.NewGuid()));
    }

    [Test]
    public void MoveAccountSubGroupCheckAndGetEntityByIdParentTest()
    {
        var movedEntity = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Jewelry", Order = 3 };
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => movedEntity);

        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        mockAccountGroupDataAccess.Setup(agda => agda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , mockAccountGroupDataAccess.Object);
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.MoveToAnotherParent(movedEntity.Id, Guid.NewGuid()));
    }

    [Test]
    public void MoveAccountSubGroupCheckAndGetEntityWithSameNameTest()
    {
        var toParent = new AccountGroup { Id = Guid.NewGuid(), Name = "Support" };
        var fromParent = new AccountGroup { Id = Guid.NewGuid(), Name = "Main" };
        var movedEntity = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Jewelry", Order = 3, Parent = fromParent };
        fromParent.Children.Add(movedEntity);
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => movedEntity);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<AccountSubGroup> { new() { Id = Guid.NewGuid() } });
        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        mockAccountGroupDataAccess.Setup(agda => agda.Get(It.IsAny<Guid>())).Returns(() => toParent);

        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , mockAccountGroupDataAccess.Object);
        Assert.ThrowsAsync<ArgumentException>(
            async () => await service.MoveToAnotherParent(movedEntity.Id, toParent.Id));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private IAccountGroupDataAccess CreateMockAccountGroupDataAccess()
    {
        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        return mockAccountGroupDataAccess.Object;
    }
}