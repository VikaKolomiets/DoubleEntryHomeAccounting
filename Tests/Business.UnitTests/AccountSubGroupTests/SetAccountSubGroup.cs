﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountSubGroupTests;

[TestFixture]
public class SetAccountSubGroup
{
    [TestCase(true)]
    public void SetIsFavoriteAccountSubGroupPositiveTest(bool isFavorite)
    {
        var isUpdated = false;
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Main" };
        var entity = new AccountSubGroup
            { Id = Guid.NewGuid(), Name = "Work", IsFavorite = false, Order = 2, Parent = parent };
        parent.Children.Add(entity);
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Update(It.IsAny<AccountSubGroup>()))
            .Callback(() => isUpdated = true);


        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , CreateMockAccountGroupDataAccess());
        service.SetFavoriteStatus(entity.Id, isFavorite);
        Assert.IsTrue(entity.IsFavorite);
        Assert.IsTrue(isUpdated);
    }

    [Test]
    public void SetIsFavoriteAccountSubGroupExceptionTest()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetFavoriteStatus(Guid.NewGuid(), true));
    }

    [TestCase(1)]
    [TestCase(3)]
    public void SetOrderAccountSubGroupPositiveTest(int order)
    {
        var isOdered = false;
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Main" };
        var orderedEntity = new AccountSubGroup
            { Id = Guid.NewGuid(), Name = "Work", IsFavorite = false, Order = 2, Parent = parent };
        var firstEntity = new AccountSubGroup
            { Id = Guid.NewGuid(), Name = "Home", IsFavorite = true, Order = 1, Parent = parent };
        var thirdEntity = new AccountSubGroup
            { Id = Guid.NewGuid(), Name = "Home", IsFavorite = true, Order = 3, Parent = parent };
        parent.Children.Add(orderedEntity);
        parent.Children.Add(firstEntity);
        parent.Children.Add(thirdEntity);
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => orderedEntity);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.LoadParent(It.IsAny<AccountSubGroup>()))
            .Callback<AccountSubGroup>(asg => parent = asg.Parent);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.UpdateList(It.IsAny<List<AccountSubGroup>>()))
            .Callback(() => isOdered = true);

        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        mockAccountGroupDataAccess.Setup(agda => agda.LoadChildren(It.IsAny<AccountGroup>()));

        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , mockAccountGroupDataAccess.Object);
        service.SetOrder(orderedEntity.Id, order);
        Assert.AreEqual(order, orderedEntity.Order);
        Assert.AreEqual(2, order == 1 ? firstEntity.Order : thirdEntity.Order);
        Assert.IsTrue(isOdered);
    }

    [Test]
    public void SetOrderAccountSubGroupExceptionTest()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOrder(Guid.NewGuid(), 2));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private IAccountGroupDataAccess CreateMockAccountGroupDataAccess()
    {
        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        return mockAccountGroupDataAccess.Object;
    }
}