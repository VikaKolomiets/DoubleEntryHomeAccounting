﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountSubGroupTests;

[TestFixture]
public class AddAccountSubGroupTests
{
    [TestCase(0)]
    [TestCase(8)]
    public void AddAccountSubGroupPositiveTest(int maxOrder)
    {
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Rarely" };
        var addedSubgroup = new AccountSubGroup { Id = Guid.NewGuid() };
        var subgroup = new AccountSubGroup
            { Id = addedSubgroup.Id, Name = "Vacation", Description = "", IsFavorite = false, Parent = parent };
        parent.Children.Add(subgroup);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<AccountSubGroup> { subgroup });
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.GetMaxOrder(It.IsAny<Guid>())).Returns(() => maxOrder);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Add(It.IsAny<AccountSubGroup>()))
            .Callback<AccountSubGroup>(asg => addedSubgroup = asg);

        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        mockAccountGroupDataAccess.Setup(agda => agda.Get(It.IsAny<Guid>())).Returns(() => parent);

        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , mockAccountGroupDataAccess.Object);
        service.Add(subgroup);

        Assert.AreEqual(subgroup.Parent.Id, addedSubgroup.Parent.Id);
        Assert.AreEqual(subgroup.Name, addedSubgroup.Name);
        Assert.AreEqual(subgroup.IsFavorite, addedSubgroup.IsFavorite);
        Assert.AreEqual(subgroup.Description, addedSubgroup.Description);
        Assert.AreEqual(maxOrder + 1, addedSubgroup.Order);
    }

    [Test]
    public void AddAccountSubGroupCheckInputForNullExceptionTest()
    {
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(null));
    }

    [Test]
    public void AddAccountSubGroupCheckInputNameForNullExceptionTest()
    {
        var subgroup = new AccountSubGroup { Id = Guid.NewGuid(), Name = null, Description = "", IsFavorite = false };
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(subgroup));
    }

    [Test]
    public void AddAccountSubGroupCheckInputForNullParentExceptionTest()
    {
        var subgroup = new AccountSubGroup
            { Id = Guid.NewGuid(), Name = "Vacation", Description = "", IsFavorite = false, Parent = null };
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(subgroup));
    }

    [Test]
    public void AddAccountSubGroupCheckEntityWithSameIdExceptionTest()
    {
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Rarely" };
        var subgroup = new AccountSubGroup
            { Id = Guid.NewGuid(), Name = "Vacation", Description = "", IsFavorite = false, Parent = parent };

        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => new Account { Id = Guid.NewGuid() });
        mockGlobalDataAccess.Setup(gda => gda.Save());

        var service = new AccountSubGroupService(mockGlobalDataAccess.Object
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(subgroup));
    }

    [Test]
    public void AddAccountSubGroupCheckAndGetEntityByIdExceptionTest()
    {
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Rarely" };
        var subgroup = new AccountSubGroup
            { Id = Guid.NewGuid(), Name = "Vacation", Description = "", IsFavorite = false, Parent = parent };

        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        mockAccountGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , mockAccountGroupDataAccess.Object);
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(subgroup));
    }

    [Test]
    public void AddAccountSubGroupCheckEntityWithSameNameExceptionTest()
    {
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Rarely" };
        var subgroup = new AccountSubGroup
            { Id = Guid.NewGuid(), Name = "Vacation", Description = "", IsFavorite = false, Parent = parent };

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<AccountSubGroup> { new() { Id = Guid.NewGuid() } });

        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        mockAccountGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => parent);

        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , mockAccountGroupDataAccess.Object);
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(subgroup));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private IAccountGroupDataAccess CreateMockAccountGroupDataAccess()
    {
        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        return mockAccountGroupDataAccess.Object;
    }
}