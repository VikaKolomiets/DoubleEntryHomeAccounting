﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountSubGroupTests;

[TestFixture]
public class DeleteAccountSubGroupTests
{
    [Test]
    public void DeleteAccountSubGroupPositiveTest()
    {
        var isDeleted = false;
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Main" };
        var deletedEntity = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Home", Order = 1, Parent = parent };
        var entity = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Work", Order = 2, Parent = parent };
        parent.Children.Add(deletedEntity);
        parent.Children.Add(entity);
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => deletedEntity);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.LoadParent(It.IsAny<AccountSubGroup>()));
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Delete(It.IsAny<AccountSubGroup>()))
            .Callback(() => isDeleted = true);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.UpdateList(It.IsAny<List<AccountSubGroup>>()))
            .Callback<List<AccountSubGroup>>(l => parent.Children.Remove(deletedEntity));

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetCount(It.IsAny<Guid>())).Returns(() => 0);

        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        mockAccountGroupDataAccess.Setup(agda => agda.LoadChildren(It.IsAny<AccountGroup>()));

        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , mockAccountGroupDataAccess.Object);
        service.Delete(deletedEntity.Id);
        Assert.IsTrue(isDeleted);
        Assert.AreEqual(1, entity.Order);
    }

    [Test]
    public void DeleteAccountSubGroupCheckAndGetEntityByIdExceptionTest()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Delete(Guid.NewGuid()));
    }


    [Test]
    public void DeleteAccountSubGroupСheckExistedChildrenInTheGroupExceptionTest()
    {
        var parent = new AccountGroup { Id = Guid.NewGuid(), Name = "Main" };
        var deletedEntity = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Home", Order = 1, Parent = parent };
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => deletedEntity);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetCount(It.IsAny<Guid>())).Returns(() => 1);
        var service = new AccountSubGroupService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , CreateMockAccountGroupDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Delete(deletedEntity.Id));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private IAccountGroupDataAccess CreateMockAccountGroupDataAccess()
    {
        var mockAccountGroupDataAccess = new Mock<IAccountGroupDataAccess>();
        return mockAccountGroupDataAccess.Object;
    }
}