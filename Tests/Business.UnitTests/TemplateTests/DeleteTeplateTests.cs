﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.TemplateTests;

[TestFixture]
public class DeleteTeplateTests
{
    [Test]
    public void DeleteTeplatePositiveTests()
    {
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var deletedEntity = new Template
        {
            Id = Guid.NewGuid(), Name = "Paris", Description = "", IsFavorite = true, Order = 1, Parent = entityParent
        };
        entityParent.Children.Add(deletedEntity);
        var entity = new Template
        {
            Id = Guid.NewGuid(), Name = "Lund", Description = "Sweden", IsFavorite = true, Order = 2,
            Parent = entityParent
        };
        entityParent.Children.Add(entity);

        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        mockTemplateDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => deletedEntity);
        mockTemplateDataAccess.Setup(tda => tda.LoadParent(It.IsAny<Template>()))
            .Callback<Template>(t => t.Parent = entityParent);
        mockTemplateDataAccess.Setup(tda => tda.Delete(It.IsAny<Template>()));
        mockTemplateDataAccess.Setup(tda => tda.UpdateList(It.IsAny<List<Template>>()))
            .Callback(() => entityParent.Children.Remove(deletedEntity));

        var mockTemplateGroupDataAccess = new Mock<ITemplateGroupDataAccess>();
        mockTemplateGroupDataAccess.Setup(tgda => tgda.LoadChildren(It.IsAny<TemplateGroup>()));

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            mockTemplateDataAccess.Object,
            mockTemplateGroupDataAccess.Object,
            CreateMockAccountDataAccess());
        service.Delete(deletedEntity.Id);

        Assert.AreEqual(1, entityParent.Children.Count);
        Assert.AreEqual(1, entity.Order);
    }

    [Test]
    public void DeleteTeplateCheckAndGetEntityByIdExceptionTests()
    {
        var deletedEntity = new Template
            { Id = Guid.NewGuid(), Name = "Paris", Description = "", IsFavorite = true, Order = 1 };
        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Delete(deletedEntity.Id));
    }

    private IGlobalDataAccess GreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private ITemplateDataAccess CreatMockEntityDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITemplateGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockTemplateGroupDataAccess = new Mock<ITemplateGroupDataAccess>();
        return mockTemplateGroupDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}