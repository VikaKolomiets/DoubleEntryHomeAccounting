﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.TemplateTests;

[TestFixture]
public class UpdateTemplateTests
{
    [TestCase("Paris", "", true, "Name", "Description", false)]
    [TestCase("Lund", "Sweden", true, "Paris-Saclay", "France", false)]
    public void UpdateTemplatePositiveTest(string name, string description, bool isFavorite, string originalName,
        string originalDescription, bool originalIsFavorite)
    {
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var newEntity = new Template
        {
            Id = Guid.NewGuid(), Name = name, Description = description, IsFavorite = isFavorite, Parent = entityParent
        };

        var originalEntity = new Template
        {
            Id = newEntity.Id, Name = originalName, Description = originalDescription, IsFavorite = originalIsFavorite
        };
        entityParent.Children.Add(originalEntity);
        originalEntity.Parent = entityParent;

        var accounts = new List<Account>();
        var first = new Account { Id = Guid.NewGuid(), Name = "What" };
        var third = new Account { Id = Guid.NewGuid(), Name = "How many" };
        accounts.Add(first);
        accounts.Add(third);

        var entryFirst = new TemplateEntry { Template = originalEntity, Account = first, Amount = 2500 };
        var entryThird = new TemplateEntry { Template = originalEntity, Account = third, Amount = -2800 };
        var entrySecond = new TemplateEntry { Template = originalEntity, Account = first, Amount = 300 };
        originalEntity.Entries.Add(entryFirst);
        originalEntity.Entries.Add(entrySecond);
        originalEntity.Entries.Add(entryThird);

        var newEntityFirst = new TemplateEntry { Template = newEntity, Account = first, Amount = 0 };
        var newEntitySecond = new TemplateEntry { Template = newEntity, Account = third, Amount = 0 };
        newEntity.Entries.Add(newEntityFirst);
        newEntity.Entries.Add(newEntitySecond);

        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        mockTemplateDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns((Delegate)(() => originalEntity));
        mockTemplateDataAccess.Setup(tda => tda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Template>());
        mockTemplateDataAccess.Setup(tda => tda.Update(It.IsAny<Template>()))
            .Callback(() => originalEntity = newEntity);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>()))
            .Returns<Guid>(id => id == first.Id ? first : third);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            mockTemplateDataAccess.Object,
            CreateMockParentEntityDataAccess(),
            mockAccountDataAccess.Object);
        service.Update(newEntity);

        Assert.AreEqual(newEntity.Name, originalEntity.Name);
        Assert.AreEqual(newEntity.Description, originalEntity.Description);
        Assert.AreEqual(2, originalEntity.Entries.Count);
    }

    [Test]
    public void UpdateTemplateCheckInputForNullExceptionTest()
    {
        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(null));
    }

    [Test]
    public void UpdateTemplateCheckInputNameForNullExceptionTest()
    {
        var entity = new Template { Id = Guid.NewGuid(), Name = null, Description = "description", IsFavorite = true };
        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTemplateCheckInputForNullParentExceptionTest()
    {
        var entity = new Template
            { Id = Guid.NewGuid(), Name = "name", Description = "description", IsFavorite = true };
        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTemplateEntriesCountLessTwoExceptionTest()
    {
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Template
            { Id = Guid.NewGuid(), Name = "name", Description = "description", IsFavorite = true };
        entityParent.Children.Add(entity);
        entity.Parent = entityParent;
        var entry = new TemplateEntry { Template = entity, Amount = 0 };
        entity.Entries.Add(entry);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTemplateCheckInputForNullAccountExceptionTest()
    {
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Template
            { Id = Guid.NewGuid(), Name = "name", Description = "description", IsFavorite = true };
        entityParent.Children.Add(entity);
        entity.Parent = entityParent;
        var entryFirst = new TemplateEntry { Template = entity, Account = null, Amount = 0 };
        var entryThird = new TemplateEntry { Template = entity, Account = null, Amount = 0 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entryThird);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void UpdateTemplateCheckAndGetAccountByIdExceptionTest()
    {
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Template
        {
            Id = Guid.NewGuid(), Name = "Name", Description = "description", IsFavorite = false, Parent = entityParent
        };
        entityParent.Children.Add(entity);
        var accounts = new List<Account>();
        var first = new Account { Id = Guid.NewGuid(), Name = "What" };
        var third = new Account { Id = Guid.NewGuid(), Name = "How many" };
        accounts.Add(first);
        accounts.Add(third);
        var entryFirst = new TemplateEntry { Template = entity, Account = first, Amount = 0 };
        var entryThird = new TemplateEntry { Template = entity, Account = third, Amount = 0 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entryThird);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            mockAccountDataAccess.Object);
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    private IGlobalDataAccess GreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private ITemplateDataAccess CreatMockEntityDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITemplateGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockTemplateGroupDataAccess = new Mock<ITemplateGroupDataAccess>();
        return mockTemplateGroupDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}