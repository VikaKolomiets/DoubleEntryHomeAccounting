﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.TemplateTests;

[TestFixture]
public class AddTemplateTests
{
    [TestCase("vacation", "", true, 2)]
    [TestCase("Shoping", "Mom", true, 0)]
    public void AddTemplatePossitiveTest(string name, string description, bool isfavorite, int maxOrder)
    {
        Template addedEntity = null;
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Template
        {
            Id = Guid.NewGuid(), Name = name, Description = description, IsFavorite = isfavorite, Parent = entityParent
        };
        entityParent.Children.Add(entity);

        var accounts = new List<Account>();
        var first = new Account { Id = Guid.NewGuid(), Name = "What" };
        var third = new Account { Id = Guid.NewGuid(), Name = "How many" };
        accounts.Add(first);
        accounts.Add(third);

        var entryFirst = new TemplateEntry { Template = entity, Account = first, Amount = 2500 };
        var entryThird = new TemplateEntry { Template = entity, Account = third, Amount = -2800 };
        var entrySecond = new TemplateEntry { Template = entity, Account = first, Amount = 300 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entrySecond);
        entity.Entries.Add(entryThird);

        var mockEntityDataAccess = new Mock<ITemplateDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Template> { entity });
        mockEntityDataAccess.Setup(eda => eda.GetMaxOrder(It.IsAny<Guid>())).Returns(() => maxOrder);
        mockEntityDataAccess.Setup(eda => eda.Add(It.IsAny<Template>())).Callback<Template>(e => addedEntity = e);

        var mockParentEntityDataAccess = new Mock<ITemplateGroupDataAccess>();
        mockParentEntityDataAccess.Setup(peda => peda.Get(It.IsAny<Guid>())).Returns(() => entityParent);
        mockParentEntityDataAccess.Setup(peda => peda.GetByName(It.IsAny<string>())).Returns(() => null);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>()))
            .Returns<Guid>(id => id == first.Id ? first : third);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object,
            mockAccountDataAccess.Object);
        service.Add(entity);

        Assert.AreEqual(maxOrder + 1, addedEntity.Order);
        Assert.AreEqual(entity.Id, addedEntity.Id);
        Assert.AreEqual(entity.Name, addedEntity.Name);
        Assert.AreEqual(entity.Description, addedEntity.Description);
        Assert.AreEqual(entity.IsFavorite, addedEntity.IsFavorite);
    }

    [Test]
    public void AddTemplateCheckInputForNullEntityExceptionTest()
    {
        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(null));
    }

    [Test]
    public void AddTemplateCheckInputNameForNullEntityExceptionTest()
    {
        var entity = new Template
            { Id = Guid.NewGuid(), Name = "name", Description = "description", IsFavorite = true };
        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTemplateCheckInputNameForNullParentExceptionTest()
    {
        var entity = new Template
            { Id = Guid.NewGuid(), Name = "name", Description = "description", IsFavorite = true };
        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTemplateCheckInputWithSameIdExceptionTest()
    {
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Template
            { Id = Guid.NewGuid(), Name = "name", Description = "description", IsFavorite = true };
        entityParent.Children.Add(entity);
        entity.Parent = entityParent;
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>()))
            .Returns(() => new Template { Id = Guid.NewGuid() });

        var service = new TemplateService(mockGlobalDataAccess.Object,
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTemplateEntriesCountLessTwoExceptionTest()
    {
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Template
            { Id = Guid.NewGuid(), Name = "name", Description = "description", IsFavorite = true };
        entityParent.Children.Add(entity);
        entity.Parent = entityParent;
        var entry = new TemplateEntry { Template = entity, Amount = 0 };
        entity.Entries.Add(entry);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTemplateCheckInputForNullAccountExceptionTest()
    {
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Template
        {
            Id = Guid.NewGuid(), Name = "Name", Description = "description", IsFavorite = false, Parent = entityParent
        };
        entityParent.Children.Add(entity);
        var entryFirst = new TemplateEntry { Template = entity, Account = null, Amount = 0 };
        var entryThird = new TemplateEntry { Template = entity, Account = null, Amount = 0 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entryThird);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddTemplateCheckAndGetAccountByIdExceptionTest()
    {
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Template
        {
            Id = Guid.NewGuid(), Name = "Name", Description = "description", IsFavorite = false, Parent = entityParent
        };
        entityParent.Children.Add(entity);
        var accounts = new List<Account>();
        var first = new Account { Id = Guid.NewGuid(), Name = "What" };
        var third = new Account { Id = Guid.NewGuid(), Name = "How many" };
        accounts.Add(first);
        accounts.Add(third);
        var entryFirst = new TemplateEntry { Template = entity, Account = first, Amount = 0 };
        var entryThird = new TemplateEntry { Template = entity, Account = third, Amount = 0 };
        entity.Entries.Add(entryFirst);
        entity.Entries.Add(entryThird);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            mockAccountDataAccess.Object);
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    private IGlobalDataAccess GreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private ITemplateDataAccess CreatMockEntityDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITemplateGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockTemplateGroupDataAccess = new Mock<ITemplateGroupDataAccess>();
        return mockTemplateGroupDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}