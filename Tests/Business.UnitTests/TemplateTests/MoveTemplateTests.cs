﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.TemplateTests;

[TestFixture]
public class MoveTemplateTests
{
    [TestCase(10)]
    [TestCase(0)]
    public void MoveTemplatePositiveTest(int maxOrder)
    {
        var toParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "To" };
        var fromParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "From" };
        var movedEntity = new Template
        {
            Id = Guid.NewGuid(), Name = "Name", Description = "description", IsFavorite = false, Order = 2,
            Parent = fromParent
        };
        var firstEntity = new Template
        {
            Id = Guid.NewGuid(), Name = "Paris", Description = "", IsFavorite = true, Order = 1, Parent = fromParent
        };
        var thirdEntity = new Template
        {
            Id = Guid.NewGuid(), Name = "Lund", Description = "Sweden", IsFavorite = true, Order = 3,
            Parent = fromParent
        };
        fromParent.Children.Add(movedEntity);
        fromParent.Children.Add(firstEntity);
        fromParent.Children.Add(thirdEntity);

        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        mockTemplateDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => movedEntity);
        mockTemplateDataAccess.Setup(tda => tda.LoadParent(It.IsAny<Template>()))
            .Callback<Template>(t => t.Parent = fromParent);
        mockTemplateDataAccess.Setup(tda => tda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Template>());
        mockTemplateDataAccess.Setup(tda => tda.GetMaxOrder(It.IsAny<Guid>())).Returns(() => maxOrder);
        mockTemplateDataAccess.Setup(tda => tda.Update(It.IsAny<Template>()));
        mockTemplateDataAccess.Setup(tda => tda.UpdateList(It.IsAny<List<Template>>()))
            .Callback(() => fromParent.Children.Remove(movedEntity));

        var mockTemplateGroupDataAccess = new Mock<ITemplateGroupDataAccess>();
        mockTemplateGroupDataAccess.Setup(tgda => tgda.Get(It.IsAny<Guid>())).Returns(() => toParent);
        mockTemplateGroupDataAccess.Setup(tgda => tgda.LoadChildren(It.IsAny<TemplateGroup>()));

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            mockTemplateDataAccess.Object,
            mockTemplateGroupDataAccess.Object,
            CreateMockAccountDataAccess());
        service.MoveToAnotherParent(movedEntity.Id, toParent.Id);

        Assert.AreEqual(maxOrder + 1, movedEntity.Order);
        Assert.AreEqual(2, fromParent.Children.Count);
        Assert.AreEqual(1, toParent.Children.Count);
    }

    [Test]
    public void MoveTemplateCheckAndGetEntityByIdEntityExceptionTest()
    {
        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.MoveToAnotherParent(Guid.NewGuid(), Guid.NewGuid()));
    }

    [Test]
    public void MoveTemplateCheckAndGetEntityByIdParentExceptionTest()
    {
        var movedEntity = new Template
            { Id = Guid.NewGuid(), Name = "Name", Description = "description", IsFavorite = false, Order = 2 };
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        mockTemplateDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => movedEntity);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            mockTemplateDataAccess.Object,
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.MoveToAnotherParent(movedEntity.Id, Guid.NewGuid()));
    }

    [Test]
    public void MoveTemplateCheckEntityWithSameNameExceptionTest()
    {
        var toParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "To" };
        var fromParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "From" };
        var movedEntity = new Template
        {
            Id = Guid.NewGuid(), Name = "Name", Description = "description", IsFavorite = false, Order = 2,
            Parent = fromParent
        };
        fromParent.Children.Add(movedEntity);

        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        mockTemplateDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(() => movedEntity);
        mockTemplateDataAccess.Setup(tda => tda.LoadParent(It.IsAny<Template>()))
            .Callback<Template>(t => t.Parent = fromParent);
        mockTemplateDataAccess.Setup(tda => tda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Template> { new() { Name = "Paris", Description = "", IsFavorite = true } });
        var mockTemplateGroupDataAccess = new Mock<ITemplateGroupDataAccess>();
        mockTemplateGroupDataAccess.Setup(tgda => tgda.Get(It.IsAny<Guid>())).Returns(() => toParent);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            mockTemplateDataAccess.Object,
            mockTemplateGroupDataAccess.Object,
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentException>(
            async () => await service.MoveToAnotherParent(movedEntity.Id, toParent.Id));
    }

    private IGlobalDataAccess GreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private ITemplateDataAccess CreatMockEntityDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITemplateGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockTemplateGroupDataAccess = new Mock<ITemplateGroupDataAccess>();
        return mockTemplateGroupDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}