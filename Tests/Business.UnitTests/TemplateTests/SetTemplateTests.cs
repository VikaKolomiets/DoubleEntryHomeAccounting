﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.TemplateTests;

[TestFixture]
public class SetTemplateTests
{
    [Test]
    public void SetIsFavoriteTemplatePositiveTest()
    {
        var entity = new Template
            { Id = Guid.NewGuid(), Name = "Name", Description = "description", IsFavorite = false };
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        mockTemplateDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(entity);

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            mockTemplateDataAccess.Object,
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        service.SetFavoriteStatus(entity.Id, true);
        Assert.IsTrue(entity.IsFavorite);
    }

    [Test]
    public void SetIsFavoriteTemplateCheckAndGetEntityByIdExseptionTest()
    {
        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetFavoriteStatus(Guid.NewGuid(), true));
    }

    [TestCase(1)]
    [TestCase(3)]
    public async Task SetOrderTemplatePositiveTest(int order)
    {
        var entityParent = new TemplateGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var orderedEntity = new Template
        {
            Id = Guid.NewGuid(), Name = "Name", Description = "description", IsFavorite = false, Order = 2,
            Parent = entityParent
        };
        var firstEntity = new Template
        {
            Id = Guid.NewGuid(), Name = "Paris", Description = "", IsFavorite = true, Order = 1, Parent = entityParent
        };
        var thirdEntity = new Template
        {
            Id = Guid.NewGuid(), Name = "Lund", Description = "Sweden", IsFavorite = true, Order = 3,
            Parent = entityParent
        };
        entityParent.Children.Add(orderedEntity);
        entityParent.Children.Add(firstEntity);
        entityParent.Children.Add(thirdEntity);

        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        mockTemplateDataAccess.Setup(tda => tda.Get(It.IsAny<Guid>())).Returns(orderedEntity);
        mockTemplateDataAccess.Setup(tda => tda.LoadParent(It.IsAny<Template>()))
            .Callback<Template>(t => t.Parent = entityParent);
        mockTemplateDataAccess.Setup(tda => tda.UpdateList(It.IsAny<List<Template>>()));

        var mockTemplateGroupDataAccess = new Mock<ITemplateGroupDataAccess>();
        mockTemplateGroupDataAccess.Setup(tgda => tgda.LoadChildren(It.IsAny<TemplateGroup>()));

        var service = new TemplateService(GreateMockGlobalDataAccess(),
            mockTemplateDataAccess.Object,
            mockTemplateGroupDataAccess.Object,
            CreateMockAccountDataAccess());
        await service.SetOrder(orderedEntity.Id, order);
        Assert.AreEqual(order, orderedEntity.Order);
    }

    [Test]
    public void SetOrderTemplateCheckAndGetEntityByIdExseptionTest()
    {
        var service = new TemplateService(GreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOrder(Guid.NewGuid(), 2));
    }

    private IGlobalDataAccess GreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private ITemplateDataAccess CreatMockEntityDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITemplateGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockTemplateGroupDataAccess = new Mock<ITemplateGroupDataAccess>();
        return mockTemplateGroupDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}