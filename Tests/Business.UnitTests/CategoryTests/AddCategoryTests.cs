﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.CategoryTests;

[TestFixture]
public class AddCategoryTests
{
    [TestCase("Kate", "Holliday", true, 8)]
    [TestCase("Name", "Description", false, 0)]
    public void AddCategoryPositiveTest(string name, string description, bool isfavorite, int maxOrder)
    {
        Category addedEntity = null;
        var entityParent = new CategoryGroup { Name = "Parent" };
        var entity = new Category
            { Id = Guid.NewGuid(), Name = name, Description = description, IsFavorite = isfavorite };
        entity.Parent = entityParent;
        entityParent.Children.Add(entity);

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Category> { entity });
        mockEntityDataAccess.Setup(eda => eda.GetMaxOrder(It.IsAny<Guid>())).Returns(() => maxOrder);
        mockEntityDataAccess.Setup(eda => eda.Add(It.IsAny<Category>())).Callback<Category>(e => addedEntity = e);

        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockParentEntityDataAccess.Setup(peda => peda.Get(It.IsAny<Guid>())).Returns(() => entityParent);
        mockParentEntityDataAccess.Setup(peda => peda.GetByName(It.IsAny<string>())).Returns(() => null);

        var service = new CategoryService(CreateMockGlobalDataAccess(),
            mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object,
            CreateMockAccountDataAccess());
        service.Add(entity);

        Assert.AreEqual(entity.Id, addedEntity.Id);
        Assert.AreEqual(entity.Name, addedEntity.Name);
        Assert.AreEqual(entity.Description, addedEntity.Description);
        Assert.AreEqual(entity.IsFavorite, addedEntity.IsFavorite);
        Assert.AreEqual(maxOrder + 1, addedEntity.Order);
    }

    [Test]
    public void AddCategoryCheckInputForNullExceptionTest()
    {
        Category entity = null;
        var service = new CategoryService(CreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddCategoryCheckInputNameForNullExceptionTest()
    {
        var entity = new Category { Id = Guid.NewGuid(), Name = null, Description = "description" };
        var service = new CategoryService(CreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddCategoryCheckEntityWithSameIdExceptionTest()
    {
        var entity = new Category { Id = Guid.NewGuid(), Name = "name", Description = "description" };

        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>()))
            .Returns(() => new CategoryGroup { Id = entity.Id });

        var service = new CategoryService(mockGlobalDataAccess.Object,
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddCategoryCheckAndGetEntityByIdExceptionTest()
    {
        var entity = new Category { Id = Guid.NewGuid(), Name = "Name", Description = "description" };
        var entityParent = new CategoryGroup { Name = "Parent" };
        entity.Parent = entityParent;

        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockParentEntityDataAccess.Setup(peda => peda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new CategoryService(CreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            mockParentEntityDataAccess.Object,
            CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddCategoryCheckEntityWithSameNameExceptionTest()
    {
        Category addedEntity = null;
        var entityParent = new CategoryGroup { Name = "Parent" };
        var entity = new Category { Id = Guid.NewGuid(), Name = "name", Description = "description" };
        entity.Parent = entityParent;
        entityParent.Children.Add(entity);

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.GetByName(It.IsAny<Guid>(), It.IsAny<string>())).Returns(() =>
            new List<Category> { entity, new() { Id = Guid.NewGuid(), Name = "UOps" } });
        mockEntityDataAccess.Setup(eda => eda.Add(It.IsAny<Category>())).Callback(() => addedEntity = entity);

        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockParentEntityDataAccess.Setup(peda => peda.Get(It.IsAny<Guid>())).Returns(() => entityParent);
        mockParentEntityDataAccess.Setup(peda => peda.GetByName(It.IsAny<string>())).Returns(() => null);

        var service = new CategoryService(CreateMockGlobalDataAccess(),
            mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object,
            CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private ICategoryGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        return mockParentEntityDataAccess.Object;
    }

    private ICategoryDataAccess CreatMockEntityDataAccess()
    {
        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        return mockEntityDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}