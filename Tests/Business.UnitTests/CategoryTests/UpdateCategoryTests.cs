﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.CategoryTests;

[TestFixture]
public class UpdateCategoryTests
{
    [TestCase("Kate", "Holliday", true, "Name", "Description", false)]
    [TestCase("Name", "Description", true, "Name", "", false)]
    public void UpdateCategoryPositiveTest(string name, string description, bool isFavorite, string originalName,
        string originalDescription, bool originalIsFavorite)
    {
        var originalEntity = new Category
            { Name = originalName, Description = originalDescription, IsFavorite = originalIsFavorite };
        var newEntity = new Category
            { Id = originalEntity.Id, Name = name, Description = description, IsFavorite = isFavorite };
        var entityParent = new CategoryGroup { Name = "Parent" };

        originalEntity.Parent = entityParent;
        newEntity.Parent = originalEntity.Parent;
        entityParent.Children.Add(originalEntity);

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => originalEntity);
        mockEntityDataAccess.Setup(eda => eda.LoadParent(It.IsAny<Category>()));
        mockEntityDataAccess.Setup(eda => eda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Category>());
        mockEntityDataAccess.Setup(eda => eda.Update(It.IsAny<Category>()))
            .Callback<Category>(c => originalEntity = newEntity);

        var service = new CategoryService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(), CreateMockAccountDataAccess());
        service.Update(newEntity);

        Assert.AreEqual(newEntity.Name, originalEntity.Name);
        Assert.AreEqual(newEntity.Description, originalEntity.Description);
        Assert.AreEqual(newEntity.IsFavorite, originalEntity.IsFavorite);
    }

    [Test]
    public void UpdateCategoryCheckInputForNullExceptionTest()
    {
        Category newEntity = null;
        var service = new CategoryService(CreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(newEntity));
    }

    [Test]
    public void UpdateCategoryCheckInputNameForNullExceptionTest()
    {
        var newEntity = new Category { Name = null, Description = "Description" };
        var service = new CategoryService(CreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(newEntity));
    }

    [Test]
    public void UpdateCategoryCheckInputForNullParentExceptionTest()
    {
        var newEntity = new Category { Name = "Name", Description = "Description" };
        CategoryGroup entityParent = null;
        newEntity.Parent = entityParent;
        var service = new CategoryService(CreateMockGlobalDataAccess(),
            CreatMockEntityDataAccess(),
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(newEntity));
    }

    [Test]
    public void UpdateCategoryCheckAndGetEntityByIdExceptionTest()
    {
        var newEntity = new Category
            { Id = Guid.NewGuid(), Name = "Name", Description = "Description", IsFavorite = true };
        var entityParent = new CategoryGroup { Name = "Parent" };
        newEntity.Parent = entityParent;
        entityParent.Children.Add(newEntity);

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new CategoryService(CreateMockGlobalDataAccess(),
            mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(newEntity));
    }

    [Test]
    public void UpdateCategoryCheckEntityWithSameNameExceptionTest()
    {
        var originalEntity = new Category
            { Id = Guid.NewGuid(), Name = "Name", Description = "Description", IsFavorite = true };
        var newEntity = new Category
            { Id = originalEntity.Id, Name = "Name", Description = "Description", IsFavorite = true };
        var entityParent = new CategoryGroup { Name = "Parent" };
        originalEntity.Parent = entityParent;
        newEntity.Parent = originalEntity.Parent;
        entityParent.Children.Add(originalEntity);

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => originalEntity);
        mockEntityDataAccess.Setup(eda => eda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Category> { new() { Id = Guid.NewGuid() } });
        var service = new CategoryService(CreateMockGlobalDataAccess(),
            mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(newEntity));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private ICategoryGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        return mockParentEntityDataAccess.Object;
    }

    private ICategoryDataAccess CreatMockEntityDataAccess()
    {
        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        return mockEntityDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}