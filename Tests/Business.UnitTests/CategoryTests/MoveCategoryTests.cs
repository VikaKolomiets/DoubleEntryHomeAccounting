﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.CategoryTests;

[TestFixture]
public class MoveCategoryTests
{
    [TestCase(5)]
    [TestCase(0)]
    public void MoveCategoryPositiveTest(int maxOrder)
    {
        var fromParent = new CategoryGroup { Id = Guid.NewGuid(), Name = "From Parent" };
        var toParent = new CategoryGroup { Id = Guid.NewGuid(), Name = "To Parent" };
        var entity = new Category
            { Id = Guid.NewGuid(), Name = "Name", Description = "", IsFavorite = true, Order = 2, Parent = fromParent };
        fromParent.Children.Add(entity);
        fromParent.Children.Add(new Category
        {
            Id = Guid.NewGuid(), Name = "Kate", Description = "Vacation", IsFavorite = false, Order = 3,
            Parent = fromParent
        });
        fromParent.Children.Add(new Category
        {
            Id = Guid.NewGuid(), Name = "Mom", Description = "Health", IsFavorite = true, Order = 1, Parent = fromParent
        });

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockEntityDataAccess.Setup(eda => eda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Category>());
        mockEntityDataAccess.Setup(eda => eda.GetMaxOrder(It.IsAny<Guid>())).Returns(() => maxOrder);
        mockEntityDataAccess.Setup(eda => eda.Update(It.IsAny<Category>()));
        mockEntityDataAccess.Setup(eda => eda.UpdateList(It.IsAny<List<Category>>()))
            .Callback(() => fromParent.Children.Remove(entity));

        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockParentEntityDataAccess.Setup(peda => peda.Get(It.IsAny<Guid>())).Returns(() => toParent);
        mockParentEntityDataAccess.Setup(peda => peda.LoadChildren(It.IsAny<CategoryGroup>()));

        var service = new CategoryService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object, CreateMockAccountDataAccess());
        service.MoveToAnotherParent(entity.Id, toParent.Id);

        Assert.AreEqual(maxOrder + 1, entity.Order);
        Assert.AreEqual(2, fromParent.Children.Count);
        Assert.AreEqual(1, toParent.Children.Count);
    }

    [Test]
    public void MoveCategoryCheckAndGetEntityByIdCategoryExceptionTest()
    {
        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new CategoryService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(), CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.MoveToAnotherParent(Guid.NewGuid(), Guid.NewGuid()));
    }

    [Test]
    public void MoveCategoryCheckAndGetEntityByIdCategoryGroupExceptionTest()
    {
        var entity = new Category
            { Id = Guid.NewGuid(), Name = "Name", Description = "", IsFavorite = true, Order = 2 };

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => entity);
        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockParentEntityDataAccess.Setup(peda => peda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new CategoryService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object, CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.MoveToAnotherParent(entity.Id, Guid.NewGuid()));
    }

    [Test]
    public void MoveCategoryCheckEntityWithSameNameExceptionTest()
    {
        var fromParent = new CategoryGroup { Id = Guid.NewGuid(), Name = "From Parent" };
        var toParent = new CategoryGroup { Id = Guid.NewGuid(), Name = "To Parent" };
        var entity = new Category
            { Id = Guid.NewGuid(), Name = "Name", Description = "", IsFavorite = true, Order = 2, Parent = fromParent };
        fromParent.Children.Add(entity);

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockEntityDataAccess.Setup(eda => eda.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Category> { new() { Name = entity.Name } });

        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockParentEntityDataAccess.Setup(peda => peda.Get(It.IsAny<Guid>())).Returns(() => toParent);

        var service = new CategoryService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object, CreateMockAccountDataAccess());

        Assert.ThrowsAsync<ArgumentException>(async () => await service.MoveToAnotherParent(entity.Id, toParent.Id));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private ICategoryGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        return mockParentEntityDataAccess.Object;
    }

    private ICategoryDataAccess CreatMockEntityDataAccess()
    {
        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        return mockEntityDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}