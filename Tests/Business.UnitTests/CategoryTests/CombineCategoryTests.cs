﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.CategoryTests;

[TestFixture]
public class CombineCategoryTests
{
    [Test]
    public void CombineCategoryPositiveTest()
    {
        var secondaryParent = new CategoryGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var primaryEntity = new Category
        {
            Id = Guid.NewGuid(), Name = "Fiona", Description = "", IsFavorite = false, Order = 1,
            Parent = secondaryParent
        };
        var secondaryEntity = new Category
        {
            Id = Guid.NewGuid(), Name = "Name", Description = "", IsFavorite = true, Order = 2, Parent = secondaryParent
        };
        secondaryParent.Children.Add(secondaryEntity);
        secondaryParent.Children.Add(new Category
        {
            Id = Guid.NewGuid(), Name = "Kate", Description = "Vacation", IsFavorite = false, Order = 3,
            Parent = secondaryParent
        });
        secondaryParent.Children.Add(new Category
        {
            Id = Guid.NewGuid(), Name = "Mom", Description = "Health", IsFavorite = true, Order = 1,
            Parent = secondaryParent
        });
        var accounts = new List<Account>
        {
            new() { Id = Guid.NewGuid(), Name = "What", Category = secondaryEntity },
            new() { Id = Guid.NewGuid(), Name = "Why", Category = secondaryEntity },
            new() { Id = Guid.NewGuid(), Name = "When", Category = secondaryEntity }
        };

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>()))
            .Returns<Guid>(id => id == primaryEntity.Id ? primaryEntity : secondaryEntity);
        mockEntityDataAccess.Setup(eda => eda.LoadParent(It.IsAny<Category>()));
        mockEntityDataAccess.Setup(eda => eda.Delete(It.IsAny<Category>()));
        mockEntityDataAccess.Setup(eda => eda.UpdateList(It.IsAny<List<Category>>()))
            .Callback(() => secondaryParent.Children.Remove(secondaryEntity));

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetAccountsByCategory(It.IsAny<Category>())).Returns(accounts);
        mockAccountDataAccess.Setup(ada => ada.Update(It.IsAny<Account>()))
            .Callback(() => accounts.ForEach(a => a.Category = primaryEntity));

        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockParentEntityDataAccess.Setup(peda => peda.LoadChildren(It.IsAny<CategoryGroup>()));

        var service = new CategoryService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object, mockAccountDataAccess.Object);
        service.CombineTwoEntities(primaryEntity.Id, secondaryEntity.Id);

        Assert.AreEqual(2, secondaryParent.Children.Count);
        Assert.AreEqual(primaryEntity.Id, accounts[0].Category.Id);
        Assert.AreEqual(primaryEntity.Name, accounts[0].Category.Name);
    }

    [Test]
    public void CombineCategoryCheckAndGetEntityByIdExceptionTest()
    {
        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new CategoryService(CreateMockGlobalDataAccess(), mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(), CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.CombineTwoEntities(Guid.NewGuid(), Guid.NewGuid()));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private ICategoryGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        return mockParentEntityDataAccess.Object;
    }

    private ICategoryDataAccess CreatMockEntityDataAccess()
    {
        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        return mockEntityDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}