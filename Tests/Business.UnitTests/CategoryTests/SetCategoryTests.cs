﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.CategoryTests;

[TestFixture]
public class SetCategoryTests
{
    [Test]
    public void SetFavoriteStatusCategoryPositiveTests()
    {
        var setEntity = new Category
            { Id = Guid.NewGuid(), Name = "Name", Description = "", IsFavorite = true, Order = 2 };

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => setEntity);
        var service = new CategoryService(CreateMockGlobalDataAccess(),
            mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        service.SetFavoriteStatus(setEntity.Id, false);
        Assert.IsFalse(setEntity.IsFavorite);
    }

    [Test]
    public void SetFavoriteStatusCategoryGetExceptionTests()
    {
        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new CategoryService(CreateMockGlobalDataAccess(),
            mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetFavoriteStatus(Guid.NewGuid(), false));
    }

    [TestCase(1)]
    [TestCase(3)]
    public void SetOrderCategoryPositiveTest(int order)
    {
        var parent = new CategoryGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var setEntity = new Category
            { Id = Guid.NewGuid(), Name = "Name", Description = "", IsFavorite = true, Order = 2, Parent = parent };
        parent.Children.Add(setEntity);
        parent.Children.Add(new Category
        {
            Id = Guid.NewGuid(), Name = "Kate", Description = "Vacation", IsFavorite = false, Order = 3, Parent = parent
        });
        parent.Children.Add(new Category
        {
            Id = Guid.NewGuid(), Name = "Mom", Description = "Health", IsFavorite = true, Order = 1, Parent = parent
        });

        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => setEntity);
        mockEntityDataAccess.Setup(eda => eda.LoadParent(It.IsAny<Category>()));
        mockEntityDataAccess.Setup(eda => eda.UpdateList(It.IsAny<List<Category>>()));

        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        mockParentEntityDataAccess.Setup(pda => pda.LoadChildren(It.IsAny<CategoryGroup>()));

        var service = new CategoryService(CreateMockGlobalDataAccess(),
            mockEntityDataAccess.Object,
            mockParentEntityDataAccess.Object,
            CreateMockAccountDataAccess());
        service.SetOrder(setEntity.Id, order);

        Assert.AreEqual(order, setEntity.Order);
    }

    [Test]
    public void SetOrderCategoryGetExceptionTest()
    {
        var mockEntityDataAccess = new Mock<ICategoryDataAccess>();
        mockEntityDataAccess.Setup(eda => eda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new CategoryService(CreateMockGlobalDataAccess(),
            mockEntityDataAccess.Object,
            CreateMockParentEntityDataAccess(),
            CreateMockAccountDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOrder(Guid.NewGuid(), 1));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Load());
        mockGlobalDataAccess.Setup(gda => gda.Save());
        return mockGlobalDataAccess.Object;
    }

    private ICategoryGroupDataAccess CreateMockParentEntityDataAccess()
    {
        var mockParentEntityDataAccess = new Mock<ICategoryGroupDataAccess>();
        return mockParentEntityDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }
}