﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountTests;

[TestFixture]
public class UpdateAccountTests
{
    [Test]
    public void UpdateAccountPositiveTest()
    {
        var updatedEntity = new Account { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var category = new Category { Id = Guid.NewGuid(), Name = "Home" };
        var correspondent = new Correspondent { Id = Guid.NewGuid(), Name = "Mom" };
        var project = new Project { Id = Guid.NewGuid(), Name = "Event" };
        var entity = new Account
        {
            Id = updatedEntity.Id,
            Name = "Dad",
            Description = "What&Why",
            IsFavorite = true,
            Category = category,
            Correspondent = correspondent,
            Project = project,
            Parent = parent
        };
        parent.Children.Add(entity);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockAccountDataAccess.Setup(ada => ada.LoadParent(It.IsAny<Account>()));
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account>());
        mockAccountDataAccess.Setup(ada => ada.Update(It.IsAny<Account>())).Callback<Account>(a => updatedEntity = a);

        var mockCategoryDataAccess = new Mock<ICategoryDataAccess>();
        mockCategoryDataAccess.Setup(catda => catda.Get(It.IsAny<Guid>())).Returns(() => category);

        var mockProjectDataAccess = new Mock<IProjectDataAccess>();
        mockProjectDataAccess.Setup(pda => pda.Get(It.IsAny<Guid>())).Returns(() => project);

        var mockCorrespondentDataAccess = new Mock<ICorrespondentDataAccess>();
        mockCorrespondentDataAccess.Setup(corda => corda.Get(It.IsAny<Guid>())).Returns(() => correspondent);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , mockCategoryDataAccess.Object
            , mockProjectDataAccess.Object
            , mockCorrespondentDataAccess.Object
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        service.Update(entity);
        Assert.IsTrue(updatedEntity.IsFavorite);
        Assert.AreEqual(entity.Name, updatedEntity.Name);
        Assert.AreEqual(entity.Description, updatedEntity.Description);
        Assert.AreEqual(entity.Category, updatedEntity.Category);
        Assert.AreEqual(entity.Parent, updatedEntity.Parent);
        Assert.AreEqual(entity.Correspondent, updatedEntity.Correspondent);
        Assert.AreEqual(entity.Project, updatedEntity.Project);
    }

    [Test]
    public void AddAccountCheckInputForNullExceptionTest()
    {
        var service = new AccountService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(null));
    }

    [Test]
    public void AddAccountCheckInputNameForNullExceptionTest()
    {
        var entity = new Account { Id = Guid.NewGuid(), Name = null };
        var service = new AccountService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void AddAccountCheckAndGetEntityByIdExceptionTest()
    {
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Description = "What&Why",
            IsFavorite = true
        };
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void AddAccountCheckEntityWithSameNameExceptionTest()
    {
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Description = "What&Why",
            IsFavorite = true,
            Parent = parent
        };
        parent.Children.Add(entity);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockAccountDataAccess.Setup(ada => ada.LoadParent(It.IsAny<Account>()));
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account> { new() { Id = Guid.NewGuid() } });

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Update(entity));
    }

    [Test]
    public void AddAccountCheckAndGetEntityByIdCategoryExceptionTest()
    {
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Description = "What&Why",
            IsFavorite = true,
            Parent = parent,
            Category = new Category { Id = Guid.NewGuid() }
        };
        parent.Children.Add(entity);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockAccountDataAccess.Setup(ada => ada.LoadParent(It.IsAny<Account>()));
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account>());

        var mockCategoryDataAccess = new Mock<ICategoryDataAccess>();
        mockCategoryDataAccess.Setup(catda => catda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , mockCategoryDataAccess.Object
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void AddAccountCheckAndGetEntityByIdCorrespondentExceptionTest()
    {
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Description = "What&Why",
            IsFavorite = true,
            Parent = parent,
            Correspondent = new Correspondent { Id = Guid.NewGuid() }
        };
        parent.Children.Add(entity);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockAccountDataAccess.Setup(ada => ada.LoadParent(It.IsAny<Account>()));
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account>());

        var mockCorrespondentDataAccess = new Mock<ICorrespondentDataAccess>();
        mockCorrespondentDataAccess.Setup(corda => corda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , mockCorrespondentDataAccess.Object
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    [Test]
    public void AddAccountCheckAndGetEntityByIdProjectExceptionTest()
    {
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Description = "What&Why",
            IsFavorite = true,
            Parent = parent,
            Project = new Project { Id = Guid.NewGuid() }
        };
        parent.Children.Add(entity);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockAccountDataAccess.Setup(ada => ada.LoadParent(It.IsAny<Account>()));
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account>());

        var mockProjectDataAccess = new Mock<IProjectDataAccess>();
        mockProjectDataAccess.Setup(pda => pda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , mockProjectDataAccess.Object
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Update(entity));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private ICategoryDataAccess CreateMockCategoryDataAccess()
    {
        var mockCategoryDataAccess = new Mock<ICategoryDataAccess>();
        return mockCategoryDataAccess.Object;
    }

    private IProjectDataAccess CreateMockProjectDataAccess()
    {
        var mockProjectDataAccess = new Mock<IProjectDataAccess>();
        return mockProjectDataAccess.Object;
    }

    private ICorrespondentDataAccess CreateMockCorrespondentDataAccess()
    {
        var mockCorrespondentDataAccess = new Mock<ICorrespondentDataAccess>();
        return mockCorrespondentDataAccess.Object;
    }

    private ITemplateDataAccess CreateMockTemplateDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITransactionDataAccess CreateMockTransactionDataAccess()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        return mockTransactionDataAccess.Object;
    }

    private ICurrencyDataAccess CreateMockCurrencyDataAccess()
    {
        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        return mockCurrencyDataAccess.Object;
    }
}