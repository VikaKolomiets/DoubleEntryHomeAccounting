﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountTests;

[TestFixture]
public class AddAccountTests
{
    [TestCase(5)]
    [TestCase(0)]
    public void AddAccountPositiveTest(int maxOrder)
    {
        Account addedEntity = null;
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var category = new Category { Id = Guid.NewGuid(), Name = "Home" };
        var correspondent = new Correspondent { Id = Guid.NewGuid(), Name = "Mom" };
        var project = new Project { Id = Guid.NewGuid(), Name = "Event" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Currency = currency,
            Category = category,
            Correspondent = correspondent,
            Project = project,
            Parent = parent
        };
        parent.Children.Add(entity);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => parent);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account>());
        mockAccountDataAccess.Setup(ada => ada.GetMaxOrder(It.IsAny<Guid>())).Returns(() => maxOrder);
        mockAccountDataAccess.Setup(ada => ada.Add(It.IsAny<Account>())).Callback<Account>(a => addedEntity = a);

        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        mockCurrencyDataAccess.Setup(currda => currda.Get(It.IsAny<Guid>())).Returns(() => currency);

        var mockCategoryDataAccess = new Mock<ICategoryDataAccess>();
        mockCategoryDataAccess.Setup(catda => catda.Get(It.IsAny<Guid>())).Returns(() => category);

        var mockProjectDataAccess = new Mock<IProjectDataAccess>();
        mockProjectDataAccess.Setup(pda => pda.Get(It.IsAny<Guid>())).Returns(() => project);

        var mockCorrespondentDataAccess = new Mock<ICorrespondentDataAccess>();
        mockCorrespondentDataAccess.Setup(corda => corda.Get(It.IsAny<Guid>())).Returns(() => correspondent);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , mockCategoryDataAccess.Object
            , mockProjectDataAccess.Object
            , mockCorrespondentDataAccess.Object
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , mockCurrencyDataAccess.Object);
        service.Add(entity);
        Assert.AreEqual(entity.Id, addedEntity.Id);
        Assert.AreEqual(maxOrder + 1, addedEntity.Order);
        Assert.AreEqual(project.Name, addedEntity.Project.Name);
        Assert.AreEqual(category.Name, addedEntity.Category.Name);
        Assert.AreEqual(correspondent.Name, addedEntity.Correspondent.Name);
        Assert.AreEqual(currency.Id, addedEntity.Currency.Id);
    }

    [Test]
    public void AddAccountCheckInputForNullExceptionTest()
    {
        var service = new AccountService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(null));
    }

    [Test]
    public void AddAccountCheckInputNameForNullExceptionTest()
    {
        var service = new AccountService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.Add(new Account { Id = Guid.NewGuid(), Name = null }));
    }

    [Test]
    public void AddAccountCheckInputForNullParentExceptionTest()
    {
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Parent = null
        };
        var service = new AccountService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }


    [Test]
    public void AddAccountCheckInputForNullCurrencyExceptionTest()
    {
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Parent = parent,
            Currency = null
        };
        parent.Children.Add(entity);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddAccountCheckEntityWithSameIdExceptionTest()
    {
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Parent = parent,
            Currency = currency
        };
        parent.Children.Add(entity);
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>()))
            .Returns(() => new Currency { Id = Guid.NewGuid() });

        var service = new AccountService(mockGlobalDataAccess.Object
            , CreateMockAccountDataAccess()
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddAccountCheckAndGetEntityByIdParentExceptionTest()
    {
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Currency = currency,
            Parent = parent
        };
        parent.Children.Add(entity);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , CreateMockAccountDataAccess()
            , mockAccountSubGroupDataAccess.Object
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddAccountCheckEntityWithSameNameExceptionTest()
    {
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Currency = currency,
            Parent = parent
        };
        parent.Children.Add(entity);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => parent);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account> { new() { Id = Guid.NewGuid() } });

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentException>(async () => await service.Add(entity));
    }


    [Test]
    public void AddAccountCheckAndGetEntityByIdCurrencyExceptionTest()
    {
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Currency = currency,
            Parent = parent
        };
        parent.Children.Add(entity);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => parent);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account>());
        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        mockCurrencyDataAccess.Setup(currda => currda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , mockCurrencyDataAccess.Object);
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddAccountCheckAndGetEntityByIdCategoryExceptionTest()
    {
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Currency = currency,
            Parent = parent,
            Category = new Category()
        };
        parent.Children.Add(entity);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => parent);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account>());
        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        mockCurrencyDataAccess.Setup(currda => currda.Get(It.IsAny<Guid>())).Returns(() => currency);
        var mockCategoryDataAccess = new Mock<ICategoryDataAccess>();
        mockCategoryDataAccess.Setup(catda => catda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , mockCategoryDataAccess.Object
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , mockCurrencyDataAccess.Object);
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddAccountCheckAndGetEntityByIdProjectExceptionTest()
    {
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Currency = currency,
            Parent = parent,
            Category = null,
            Project = new Project()
        };
        parent.Children.Add(entity);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => parent);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account>());
        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        mockCurrencyDataAccess.Setup(currda => currda.Get(It.IsAny<Guid>())).Returns(() => currency);
        var mockProjectDataAccess = new Mock<IProjectDataAccess>();
        mockProjectDataAccess.Setup(pda => pda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , CreateMockCategoryDataAccess()
            , mockProjectDataAccess.Object
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , mockCurrencyDataAccess.Object);
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    [Test]
    public void AddAccountCheckAndGetEntityByIdCorrespondentExceptionTest()
    {
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Currency = currency,
            Parent = parent,
            Category = null,
            Project = null,
            Correspondent = new Correspondent { Id = Guid.NewGuid() }
        };
        parent.Children.Add(entity);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => parent);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account>());
        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        mockCurrencyDataAccess.Setup(currda => currda.Get(It.IsAny<Guid>())).Returns(() => currency);
        var mockCorrespondentDataAccess = new Mock<ICorrespondentDataAccess>();
        mockCorrespondentDataAccess.Setup(corda => corda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , mockCurrencyDataAccess.Object);
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(entity));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private ICategoryDataAccess CreateMockCategoryDataAccess()
    {
        var mockCategoryDataAccess = new Mock<ICategoryDataAccess>();
        return mockCategoryDataAccess.Object;
    }

    private IProjectDataAccess CreateMockProjectDataAccess()
    {
        var mockProjectDataAccess = new Mock<IProjectDataAccess>();
        return mockProjectDataAccess.Object;
    }

    private ICorrespondentDataAccess CreateMockCorrespondentDataAccess()
    {
        var mockCorrespondentDataAccess = new Mock<ICorrespondentDataAccess>();
        return mockCorrespondentDataAccess.Object;
    }

    private ITemplateDataAccess CreateMockTemplateDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITransactionDataAccess CreateMockTransactionDataAccess()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        return mockTransactionDataAccess.Object;
    }

    private ICurrencyDataAccess CreateMockCurrencyDataAccess()
    {
        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        return mockCurrencyDataAccess.Object;
    }
}