﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountTests;

[TestFixture]
public class DeleteAccountTests
{
    [Test]
    public void DeleteAccountPossitiveTest()
    {
        var isDeleted = false;
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var category = new Category { Id = Guid.NewGuid(), Name = "Home" };
        var correspondent = new Correspondent { Id = Guid.NewGuid(), Name = "Mom" };
        var project = new Project { Id = Guid.NewGuid(), Name = "Event" };
        var deletedEntity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Currency = currency,
            Category = category,
            Correspondent = correspondent,
            Project = project,
            Parent = parent,
            Order = 2
        };
        var first = new Account
            { Id = Guid.NewGuid(), Name = "First", Currency = currency, Parent = parent, Order = 1 };
        var second = new Account
            { Id = Guid.NewGuid(), Name = "Second", Currency = currency, Parent = parent, Order = 3 };
        parent.Children.Add(deletedEntity);
        parent.Children.Add(first);
        parent.Children.Add(second);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => deletedEntity);
        mockAccountDataAccess.Setup(ada => ada.LoadParent(It.IsAny<Account>()));
        mockAccountDataAccess.Setup(ada => ada.Delete(It.IsAny<Account>())).Callback(() => isDeleted = true);
        mockAccountDataAccess.Setup(ada => ada.UpdateList(It.IsAny<List<Account>>()));

        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.GetTransactionEntriesCount(It.IsAny<Guid>())).Returns(() => 0);

        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        mockTemplateDataAccess.Setup(tda => tda.GetTemplateEntriesCount(It.IsAny<Guid>())).Returns(() => 0);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.LoadChildren(It.IsAny<AccountSubGroup>()));

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , mockTemplateDataAccess.Object
            , mockTransactionDataAccess.Object
            , CreateMockCurrencyDataAccess());
        service.Delete(deletedEntity.Id);
        Assert.IsTrue(isDeleted);
        Assert.AreEqual(2, parent.Children.Count);
        Assert.AreEqual(2, second.Order);
    }

    [Test]
    public void DeleteAccountCheckAndGetEntityByIdExceptionTest()
    {
        var deletedEntity = new Account { Id = Guid.NewGuid(), Name = "Dad" };
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Delete(deletedEntity.Id));
    }

    [Test]
    public void DeleteAccountCheckTransactionEntriesCountExceptionTest()
    {
        var deletedEntity = new Account { Id = Guid.NewGuid(), Name = "Dad" };
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => deletedEntity);
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.GetTransactionEntriesCount(It.IsAny<Guid>())).Returns(() => 1);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , mockTransactionDataAccess.Object
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<Exception>(async () => await service.Delete(deletedEntity.Id));
    }

    [Test]
    public void DeleteAccountCheckTemplateEntriesCountExceptionTest()
    {
        var deletedEntity = new Account { Id = Guid.NewGuid(), Name = "Dad" };
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => deletedEntity);
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(tda => tda.GetTransactionEntriesCount(It.IsAny<Guid>())).Returns(() => 0);
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        mockTemplateDataAccess.Setup(tda => tda.GetTemplateEntriesCount(It.IsAny<Guid>())).Returns(() => 1);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , mockTemplateDataAccess.Object
            , mockTransactionDataAccess.Object
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<Exception>(async () => await service.Delete(deletedEntity.Id));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private ICategoryDataAccess CreateMockCategoryDataAccess()
    {
        var mockCategoryDataAccess = new Mock<ICategoryDataAccess>();
        return mockCategoryDataAccess.Object;
    }

    private IProjectDataAccess CreateMockProjectDataAccess()
    {
        var mockProjectDataAccess = new Mock<IProjectDataAccess>();
        return mockProjectDataAccess.Object;
    }

    private ICorrespondentDataAccess CreateMockCorrespondentDataAccess()
    {
        var mockCorrespondentDataAccess = new Mock<ICorrespondentDataAccess>();
        return mockCorrespondentDataAccess.Object;
    }

    private ITemplateDataAccess CreateMockTemplateDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITransactionDataAccess CreateMockTransactionDataAccess()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        return mockTransactionDataAccess.Object;
    }

    private ICurrencyDataAccess CreateMockCurrencyDataAccess()
    {
        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        return mockCurrencyDataAccess.Object;
    }
}