﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountTests;

[TestFixture]
public class CombineAccountTests
{
    [Test]
    public void CombineTwoEntitiesAccountPositiveTest()
    {
        var isDeleted = false;

        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var primaryEntity = new Account
            { Id = Guid.NewGuid(), Name = "Primary", Currency = currency, Parent = parent, Order = 1 };
        var otherEntity = new Account
            { Id = Guid.NewGuid(), Name = "Other", Currency = currency, Parent = parent, Order = 3 };
        var secondaryEntity = new Account
            { Id = Guid.NewGuid(), Name = "Secondary", Currency = currency, Parent = parent, Order = 2 };
        parent.Children.Add(primaryEntity);
        parent.Children.Add(secondaryEntity);
        parent.Children.Add(otherEntity);

        var secondaryTemplates = new List<TemplateEntry>
        {
            new() { Id = Guid.NewGuid(), Account = secondaryEntity },
            new() { Id = Guid.NewGuid(), Account = secondaryEntity }
        };
        var secondaryTransactions = new List<TransactionEntry>
        {
            new() { Id = Guid.NewGuid(), Account = secondaryEntity },
            new() { Id = Guid.NewGuid(), Account = secondaryEntity }
        };

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>()))
            .Returns<Guid>(id => id == primaryEntity.Id ? primaryEntity : secondaryEntity);
        mockAccountDataAccess.Setup(ada => ada.LoadCurrency(It.IsAny<Account>()))
            .Callback<Account>(a => currency = a.Currency);
        mockAccountDataAccess.Setup(ada => ada.Delete(It.IsAny<Account>())).Callback(() => isDeleted = true);

        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        mockTemplateDataAccess.Setup(temda => temda.GetEntriesByAccount(It.IsAny<Account>()))
            .Returns(() => secondaryTemplates);

        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        mockTransactionDataAccess.Setup(trada => trada.GetEntriesByAccount(It.IsAny<Account>()))
            .Returns(() => secondaryTransactions);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , mockTemplateDataAccess.Object
            , mockTransactionDataAccess.Object
            , CreateMockCurrencyDataAccess());
        service.CombineTwoEntities(primaryEntity.Id, secondaryEntity.Id);

        Assert.IsTrue(isDeleted);
        Assert.AreEqual(2, parent.Children.Count);
        Assert.AreEqual(primaryEntity, secondaryTemplates[0].Account);
        Assert.AreEqual(primaryEntity, secondaryTransactions[0].Account);
        Assert.AreEqual(2, otherEntity.Order);
    }

    [Test]
    public void CombineTwoEntitesCheckAndGetEntityByIdPrimaryExceptionTest()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => null);
        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.CombineTwoEntities(Guid.NewGuid(), Guid.NewGuid()));
    }

    [Test]
    public void CombineTwoEntitesCheckAndGetEntityByIdSecondaryExceptionTest()
    {
        var primaryEntity = new Account { Id = Guid.NewGuid(), Name = "Primary" };
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>()))
            .Returns<Guid>(id => id == primaryEntity.Id ? primaryEntity : null);
        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());

        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.CombineTwoEntities(primaryEntity.Id, Guid.NewGuid()));
    }

    [Test]
    public void ConbineTwoEntitiesCheckForSameCurrencyExceptionTest()
    {
        var primaryEntity = new Account
            { Id = Guid.NewGuid(), Name = "Primary", Currency = new Currency { Id = Guid.NewGuid() } };
        var secondaryEntity = new Account
            { Id = Guid.NewGuid(), Name = "Secondary", Currency = new Currency { Id = Guid.NewGuid() } };

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>()))
            .Returns<Guid>(id => id == primaryEntity.Id ? primaryEntity : secondaryEntity);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());

        Assert.ThrowsAsync<ArgumentException>(async () =>
            await service.CombineTwoEntities(primaryEntity.Id, Guid.NewGuid()));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private ICategoryDataAccess CreateMockCategoryDataAccess()
    {
        var mockCategoryDataAccess = new Mock<ICategoryDataAccess>();
        return mockCategoryDataAccess.Object;
    }

    private IProjectDataAccess CreateMockProjectDataAccess()
    {
        var mockProjectDataAccess = new Mock<IProjectDataAccess>();
        return mockProjectDataAccess.Object;
    }

    private ICorrespondentDataAccess CreateMockCorrespondentDataAccess()
    {
        var mockCorrespondentDataAccess = new Mock<ICorrespondentDataAccess>();
        return mockCorrespondentDataAccess.Object;
    }

    private ITemplateDataAccess CreateMockTemplateDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITransactionDataAccess CreateMockTransactionDataAccess()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        return mockTransactionDataAccess.Object;
    }

    private ICurrencyDataAccess CreateMockCurrencyDataAccess()
    {
        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        return mockCurrencyDataAccess.Object;
    }
}