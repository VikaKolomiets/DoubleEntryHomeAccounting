﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountTests;

[TestFixture]
public class SetAccountTests
{
    [Test]
    public void SetFavoriteStatusAccountPositiveTest()
    {
        var isSetFavoriteStatus = false;
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var entity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Currency = currency,
            Parent = parent,
            IsFavorite = true
        };
        parent.Children.Add(entity);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => entity);
        mockAccountDataAccess.Setup(ada => ada.Update(It.IsAny<Account>())).Callback(() => isSetFavoriteStatus = true);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        service.SetFavoriteStatus(entity.Id, false);

        Assert.IsTrue(isSetFavoriteStatus);
        Assert.IsFalse(entity.IsFavorite);
    }

    [Test]
    public void SetFavoriteStatusAccountCheckAndGetEntityByIdExceptionTest()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetFavoriteStatus(Guid.NewGuid(), true));
    }

    [TestCase(2, 1, 2, 3)]
    [TestCase(2, 3, 1, 2)]
    public void SetOrderAccountPositiveTest(int oldOrder, int newEntityOrder, int newFirstOrder, int newSecondOrder)
    {
        var isSetOrder = false;
        var currency = new Currency { Id = Guid.NewGuid() };
        var parent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "Parent" };
        var orderedEntity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Dad",
            Currency = currency,
            Parent = parent,
            Order = oldOrder
        };
        var first = new Account
            { Id = Guid.NewGuid(), Name = "First", Currency = currency, Parent = parent, Order = 1 };
        var second = new Account
            { Id = Guid.NewGuid(), Name = "Second", Currency = currency, Parent = parent, Order = 3 };
        parent.Children.Add(orderedEntity);
        parent.Children.Add(first);
        parent.Children.Add(second);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => orderedEntity);
        mockAccountDataAccess.Setup(ada => ada.LoadParent(It.IsAny<Account>()));
        mockAccountDataAccess.Setup(ada => ada.UpdateList(It.IsAny<List<Account>>())).Callback(() => isSetOrder = true);

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.LoadChildren(It.IsAny<AccountSubGroup>()));

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        service.SetOrder(orderedEntity.Id, newEntityOrder);

        Assert.IsTrue(isSetOrder);
        Assert.AreEqual(newEntityOrder, orderedEntity.Order);
        Assert.AreEqual(newFirstOrder, first.Order);
        Assert.AreEqual(newSecondOrder, second.Order);
    }

    [Test]
    public void SetOrderAccountCheckAndGetEntityByIdExceptionTest()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOrder(Guid.NewGuid(), 2));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private ICategoryDataAccess CreateMockCategoryDataAccess()
    {
        var mockCategoryDataAccess = new Mock<ICategoryDataAccess>();
        return mockCategoryDataAccess.Object;
    }

    private IProjectDataAccess CreateMockProjectDataAccess()
    {
        var mockProjectDataAccess = new Mock<IProjectDataAccess>();
        return mockProjectDataAccess.Object;
    }

    private ICorrespondentDataAccess CreateMockCorrespondentDataAccess()
    {
        var mockCorrespondentDataAccess = new Mock<ICorrespondentDataAccess>();
        return mockCorrespondentDataAccess.Object;
    }

    private ITemplateDataAccess CreateMockTemplateDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITransactionDataAccess CreateMockTransactionDataAccess()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        return mockTransactionDataAccess.Object;
    }

    private ICurrencyDataAccess CreateMockCurrencyDataAccess()
    {
        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        return mockCurrencyDataAccess.Object;
    }
}