﻿using Business.Services;
using Common.DataAccess;
using Common.Models;
using Moq;
using NUnit.Framework;

namespace Business.UnitTests.AccountTests;

[TestFixture]
public class MoveAccountTests
{
    [TestCase(10)]
    public void MoveToAnotherParentPositiveTest(int maxOrder)
    {
        var isUpdatedEntity = false;
        var toParent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "To Parent" };
        var fromParent = new AccountSubGroup { Id = Guid.NewGuid(), Name = "From Parent" };

        var movedEntity = new Account
        {
            Id = Guid.NewGuid(),
            Name = "Mom",
            Order = 2,
            Parent = fromParent
        };
        var firstEntity = new Account { Id = Guid.NewGuid(), Name = "First", Parent = fromParent, Order = 1 };
        var secondEntity = new Account { Id = Guid.NewGuid(), Name = "Second", Parent = fromParent, Order = 3 };
        fromParent.Children.Add(movedEntity);
        fromParent.Children.Add(firstEntity);
        fromParent.Children.Add(secondEntity);

        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => movedEntity);
        mockAccountDataAccess.Setup(ada => ada.LoadParent(It.IsAny<Account>()));
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account>());
        mockAccountDataAccess.Setup(ada => ada.GetMaxOrder(It.IsAny<Guid>())).Returns(() => maxOrder);
        mockAccountDataAccess.Setup(ada => ada.Update(It.IsAny<Account>())).Callback(() => isUpdatedEntity = true);
        mockAccountDataAccess.Setup(ada => ada.UpdateList(It.IsAny<List<Account>>()));

        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => toParent);
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.LoadChildren(It.IsAny<AccountSubGroup>()));

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        service.MoveToAnotherParent(movedEntity.Id, toParent.Id);

        Assert.IsTrue(isUpdatedEntity);
        Assert.AreEqual(toParent.Id, movedEntity.Parent.Id);
        Assert.AreEqual(maxOrder + 1, movedEntity.Order);
        Assert.AreEqual(2, fromParent.Children.Count);
        Assert.AreEqual(1, toParent.Children.Count);
        Assert.AreEqual(2, secondEntity.Order);
    }

    [Test]
    public void MoveToAnotherParentCheckAndGetEntityByIdExceptionTest()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , CreateMockAccountSubGroupDataAccess()
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.MoveToAnotherParent(Guid.NewGuid(), Guid.NewGuid()));
    }

    [Test]
    public void MoveToAnotherParentCheckAndGetEntityByIdParentExceptionTest()
    {
        var firstEntity = new Account { Id = Guid.NewGuid(), Name = "First", Order = 1 };
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => firstEntity);
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => null);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentNullException>(async () =>
            await service.MoveToAnotherParent(firstEntity.Id, Guid.NewGuid()));
    }

    [Test]
    public void MoveToAnotherParentCheckEntityWithSameNameExceptionTest()
    {
        var toParent = new AccountSubGroup { Id = Guid.NewGuid() };
        var fromParent = new AccountSubGroup { Id = Guid.NewGuid() };
        var movedEntity = new Account { Id = Guid.NewGuid(), Name = "First", Parent = fromParent, Order = 1 };
        fromParent.Children.Add(movedEntity);
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        mockAccountDataAccess.Setup(ada => ada.Get(It.IsAny<Guid>())).Returns(() => movedEntity);
        mockAccountDataAccess.Setup(ada => ada.LoadParent(It.IsAny<Account>()));
        mockAccountDataAccess.Setup(ada => ada.GetByName(It.IsAny<Guid>(), It.IsAny<string>()))
            .Returns(() => new List<Account> { new() { Id = Guid.NewGuid() } });
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        mockAccountSubGroupDataAccess.Setup(asgda => asgda.Get(It.IsAny<Guid>())).Returns(() => toParent);

        var service = new AccountService(CreateMockGlobalDataAccess()
            , mockAccountDataAccess.Object
            , mockAccountSubGroupDataAccess.Object
            , CreateMockCategoryDataAccess()
            , CreateMockProjectDataAccess()
            , CreateMockCorrespondentDataAccess()
            , CreateMockTemplateDataAccess()
            , CreateMockTransactionDataAccess()
            , CreateMockCurrencyDataAccess());
        Assert.ThrowsAsync<ArgumentException>(
            async () => await service.MoveToAnotherParent(movedEntity.Id, toParent.Id));
    }

    private IGlobalDataAccess CreateMockGlobalDataAccess()
    {
        var mockGlobalDataAccess = new Mock<IGlobalDataAccess>();
        mockGlobalDataAccess.Setup(gda => gda.Get(It.IsAny<Guid>())).Returns(() => null);
        return mockGlobalDataAccess.Object;
    }

    private IAccountDataAccess CreateMockAccountDataAccess()
    {
        var mockAccountDataAccess = new Mock<IAccountDataAccess>();
        return mockAccountDataAccess.Object;
    }

    private IAccountSubGroupDataAccess CreateMockAccountSubGroupDataAccess()
    {
        var mockAccountSubGroupDataAccess = new Mock<IAccountSubGroupDataAccess>();
        return mockAccountSubGroupDataAccess.Object;
    }

    private ICategoryDataAccess CreateMockCategoryDataAccess()
    {
        var mockCategoryDataAccess = new Mock<ICategoryDataAccess>();
        return mockCategoryDataAccess.Object;
    }

    private IProjectDataAccess CreateMockProjectDataAccess()
    {
        var mockProjectDataAccess = new Mock<IProjectDataAccess>();
        return mockProjectDataAccess.Object;
    }

    private ICorrespondentDataAccess CreateMockCorrespondentDataAccess()
    {
        var mockCorrespondentDataAccess = new Mock<ICorrespondentDataAccess>();
        return mockCorrespondentDataAccess.Object;
    }

    private ITemplateDataAccess CreateMockTemplateDataAccess()
    {
        var mockTemplateDataAccess = new Mock<ITemplateDataAccess>();
        return mockTemplateDataAccess.Object;
    }

    private ITransactionDataAccess CreateMockTransactionDataAccess()
    {
        var mockTransactionDataAccess = new Mock<ITransactionDataAccess>();
        return mockTransactionDataAccess.Object;
    }

    private ICurrencyDataAccess CreateMockCurrencyDataAccess()
    {
        var mockCurrencyDataAccess = new Mock<ICurrencyDataAccess>();
        return mockCurrencyDataAccess.Object;
    }
}