﻿using DataAccess.InMemoryDb.Comparison;
using DataAccess.InMemoryDb.Xml.Persistence;
using Tests.DataAccess.InMemoryDb.Xml.Persistence;

namespace DataAccess.Xml.Tests.PersistenceTests;

[TestFixture]
public class XmlSerializerTest
{
    [TestCase("MyXmlBig")]
    public void XmlSerializerDeserializeLedgerExsistTest(string xmlPath)
    {
        IXmlStorage storage = new XmlResourceStorage(xmlPath);
        var document = storage.Load();
        IXmlSerializer serializer = new XmlSerializer();
        var ledger = serializer.Deserialize(document);
        Assert.IsNotNull(ledger);
    }

    [TestCase("MyXmlBig", 5)]
    public void XmlSerializerDeserializeProjectGroupCountTest(string xmlPath, int expectedCount)
    {
        IXmlStorage storage = new XmlResourceStorage(xmlPath);
        var document = storage.Load();
        IXmlSerializer serializer = new XmlSerializer();
        var ledger = serializer.Deserialize(document);
        Assert.AreEqual(expectedCount, ledger.ProjectGroups.Count);
    }

    [TestCase("MyXmlBig", 4)]
    public void XmlSerializerDeserializeAccountGroupCountTest(string xmlPath, int expectedCount)
    {
        IXmlStorage storage = new XmlResourceStorage(xmlPath);
        var document = storage.Load();
        IXmlSerializer serializer = new XmlSerializer();
        var ledger = serializer.Deserialize(document);
        Assert.AreEqual(expectedCount, ledger.AccountGroups.Count);
    }

    [TestCase("MyXmlBig", 5)]
    public void XmlSerializerDeserializeTemplateGroupCountTest(string xmlPath, int expectedCount)
    {
        IXmlStorage storage = new XmlResourceStorage(xmlPath);
        var document = storage.Load();
        IXmlSerializer serializer = new XmlSerializer();
        var ledger = serializer.Deserialize(document);
        Assert.AreEqual(expectedCount, ledger.TemplateGroups.Count);
    }

    [TestCase("MyXmlBig", 7)]
    public void XmlSerializerDeserializeTransactionCountTest(string xmlPath, int expectedCount)
    {
        IXmlStorage storage = new XmlResourceStorage(xmlPath);
        var document = storage.Load();
        IXmlSerializer serializer = new XmlSerializer();
        var ledger = serializer.Deserialize(document);
        Assert.AreEqual(expectedCount, ledger.Transactions.Count);
    }

    [TestCase("MyXmlBig", 7)]
    public void XmlSerializerDeserializeAccountGroupTest(string xmlPath, int expectedCount)
    {
        IXmlStorage storage = new XmlResourceStorage(xmlPath);
        var document = storage.Load();
        IXmlSerializer serializer = new XmlSerializer();
        var ledger = serializer.Deserialize(document);
        Assert.AreEqual(expectedCount, ledger.Transactions.Count);
    }

    [TestCase("MyXmlBig", "UAH")]
    public void XmlSerializerDeserializeCurrencyIsoCodeTest(string xmlPath, string expectedName)
    {
        IXmlStorage storage = new XmlResourceStorage(xmlPath);
        var document = storage.Load();
        IXmlSerializer serializer = new XmlSerializer();
        var ledger = serializer.Deserialize(document);
        Assert.AreEqual(expectedName, ledger.Currencies[0].IsoCode);
    }

    [TestCase("MyXmlBig", 36.8200)]
    public void XmlSerializerDeserializeCurrencyRateTest(string xmlPath, decimal expectedRate)
    {
        IXmlStorage storage = new XmlResourceStorage(xmlPath);
        var document = storage.Load();
        IXmlSerializer serializer = new XmlSerializer();
        var ledger = serializer.Deserialize(document);
        Assert.AreEqual(expectedRate, ledger.CurrencyRates[0].Rate);
    }

    [TestCase("MyXmlBig")]
    public void XmlSerializerSeriallizeDocumentExsistTest(string xmlPath)
    {
        IXmlStorage storage = new XmlResourceStorage(xmlPath);
        var document = storage.Load();
        IXmlSerializer serializer = new XmlSerializer();
        var ledgerSource = serializer.Deserialize(document);

        var xmlDocument = serializer.Serialize(ledgerSource);
        var ledgerDest = serializer.Deserialize(xmlDocument);
        ILedgerComparer ledgerComparer = new LedgerComparer();
        Assert.IsTrue(ledgerComparer.Compare(ledgerSource, ledgerDest).Count == 0);
    }
}