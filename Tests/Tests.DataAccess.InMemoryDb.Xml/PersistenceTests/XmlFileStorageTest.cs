using System.Xml.Linq;
using DataAccess.InMemoryDb.Xml.Persistence;
using Tests.DataAccess.InMemoryDb.Xml.Persistence;

namespace DataAccess.Xml.Tests.PersistenceTests;

public class XmlFileStorageTest
{
    [TestCase(null)]
    [TestCase("")]
    public void ExceptionXmlFileStorageTests(string xmlPath)
    {
        Assert.Throws<ArgumentNullException>(() => new XmlFileStorage(xmlPath));
    }

    [TestCase("MyXmlBig")]
    [TestCase("MyXmlMiddle")]
    public void LoadTests(string xmlPath)
    {
        IXmlStorage storage = new XmlResourceStorage(xmlPath);
        var document = storage.Load();
        Assert.NotNull(document);
    }

    [TestCase("MyXmlBig", @"c:\\_Vita\Git\DoubleEntryHomeAccounting\MyXmlBig.xml")]
    public void SaveAfterLoadTest(string xmlPath, string xmlSavePath)
    {
        IXmlStorage storage1 = new XmlResourceStorage(xmlPath);
        var docExpected = storage1.Load();
        IXmlStorage storage2 = new XmlFileStorage(xmlSavePath);
        storage2.Save(docExpected);
        var docResult = storage2.Load();

        Assert.IsTrue(XNode.DeepEquals(docExpected, docResult));
    }
}