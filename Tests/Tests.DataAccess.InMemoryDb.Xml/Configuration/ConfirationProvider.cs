﻿using DataAccess.InMemoryDb;
using DataAccess.InMemoryDb.Xml;
using DataAccess.InMemoryDb.Xml.Persistence;
using Microsoft.Extensions.DependencyInjection;
using Tests.DataAccess.InMemoryDb.Xml.Persistence;

namespace DataAccess.Xml.Tests.Configuration;

public class ConfirationProvider
{
    public static ServiceProvider Configure(string xmlPath = "MyXmlBig")
    {
        var serviceCollection = new ServiceCollection(); //DI container

        serviceCollection.AddScoped<IXmlStorage>(sp => new XmlResourceStorage(xmlPath)); //XmlFileStorage
        serviceCollection.AddScoped<IXmlValidator, XmlValidator>();
        serviceCollection.AddScoped<IXmlSerializer, XmlSerializer>();
        serviceCollection.AddScoped<ILedgerFactory, LedgerFactory>();


        var provider = serviceCollection.BuildServiceProvider();

        return provider;
    }
}