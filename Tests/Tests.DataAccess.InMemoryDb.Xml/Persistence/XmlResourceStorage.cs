﻿using System.Xml.Linq;
using DataAccess.InMemoryDb.Xml.Persistence;
using Tests.Resources;

namespace Tests.DataAccess.InMemoryDb.Xml.Persistence;

public class XmlResourceStorage : IXmlStorage
{
    public XmlResourceStorage(string resourcePath)
    {
        if (string.IsNullOrEmpty(resourcePath)) throw new ArgumentNullException(nameof(resourcePath));
        ResourcePath = resourcePath;
    }

    public string ResourcePath { get; }

    public XDocument Load()
    {
        var xml = XmlTestResource.ResourceManager.GetString(ResourcePath);
        var doc = XDocument.Parse(xml);
        return doc;
    }

    public void Save(XDocument doc)
    {
    }
}