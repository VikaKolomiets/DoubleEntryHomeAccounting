﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Utils.Validation;

namespace DataAccess.InMemoryDb.Validation
{
    public interface ILedgerValidator
    {
        List<ValidationError> Validate(Ledger ledger);
    }
}
