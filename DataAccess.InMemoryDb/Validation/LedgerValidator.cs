﻿using Common.Models.Interfaces;
using Common.Utils.Validation;

namespace DataAccess.InMemoryDb.Validation;

public class LedgerValidator : ILedgerValidator
{
    public List<ValidationError> Validate(Ledger ledger)
    {
        List<ValidationError> errors = new();

        validateMissingEntityId(ledger, errors);
        validateDuplicatedEntityId(ledger, errors);

        //Existing and > 0 Name for all named element
        //Existing and Correct TimeStamp for all Tracked elements
        //Existing of children collection
        //Existing of children in common collection
        //Existing of parents
        //Existing of parents in common collection
        //Correct Order of all ordered elements
        //Unique Name in collection
        //DateTime in the range
        //Correct rate > 0
        //Correct rate for home  currency in transaction
        //correct transactions

        return errors;

    }

    private static void validateMissingEntityId(Ledger ledger, List<ValidationError> errors)
    {
        foreach (List<IEntity> entities in ledger.AllLists)
        {
            foreach (IEntity entity in entities)
            {
                if (entity.Id == Guid.Empty)
                {
                    errors.Add(new ValidationError(ValidationErrorType.MissedId, Guid.Empty, entity.GetType().Name));
                }
            }
        }
    }

    private static void validateDuplicatedEntityId(Ledger ledger, List<ValidationError> errors)
    {
        HashSet<Guid> set = new();
        foreach (List<IEntity> entities in ledger.AllLists)
        {
            foreach (IEntity entity in entities)
            {
                if (set.Contains(entity.Id))
                {
                    errors.Add(new ValidationError(ValidationErrorType.DuplicatedId, entity.Id, entity.GetType().Name));
                }
                set.Add(entity.Id);
            }
        }
    }
}