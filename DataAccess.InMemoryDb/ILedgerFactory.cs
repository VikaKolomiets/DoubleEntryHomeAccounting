﻿namespace DataAccess.InMemoryDb;

public interface ILedgerFactory
{
    Ledger Get();
    void Save(Ledger ledger);
}