﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbCategoryRepository : MemoryDbChildEntityRepository<Category, CategoryGroup>, ICategoryRepository
{
    public MemoryDbCategoryRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<Category> GetEntities()
    {
        return GetLedger().Categories;
    }
}