﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbTemplateRepository : MemoryDbChildEntityRepository<Template, TemplateGroup>, ITemplateRepository
{
    public MemoryDbTemplateRepository(ILedgerFactory factory) : base(factory)
    {
    }

    public Task<List<TemplateEntry>> GetEntriesByAccount(Account account)
    {
        return Task.FromResult(account != null ? GetNotNullAccountEntries(account.Id).ToList() : GetNullAccountEntries().ToList());
    }

    public Task<int> GetTemplateEntriesCount(Guid accountId)
    {
        return Task.FromResult(accountId != Guid.Empty ? GetNotNullAccountEntries(accountId).Count() : GetNullAccountEntries().Count());
    }

    protected override List<Template> GetEntities()
    {
        return GetLedger().Templates;
    }

    private IEnumerable<TemplateEntry> GetNotNullAccountEntries(Guid accountId)
    {
        return GetEntities()
            .SelectMany(e => e.Entries)
            .Where(e => e.Account != null && e.Account.Id == accountId)
            .ToList();
    }

    private IEnumerable<TemplateEntry> GetNullAccountEntries()
    {
        return GetEntities()
            .SelectMany(e => e.Entries)
            .Where(e => e.Account == null);
    }
}