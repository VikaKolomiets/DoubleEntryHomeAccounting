﻿using Common.DataAccess;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbSystemConfigRepository : ISystemConfigRepository
{
    private readonly ILedgerFactory _factory;

    public MemoryDbSystemConfigRepository(ILedgerFactory factory)
    {
        _factory = factory;
    }

    public Task<string> GetMainCurrencyIsoCode()
    {
        return Task.FromResult(_factory.Get().SystemConfig.MainCurrencyIsoCode);
    }

    public Task<DateTime> GetMinDate()
    {
        return Task.FromResult( _factory.Get().SystemConfig.MinDate);
    }

    public Task<DateTime> GetMaxDate()
    {
        return Task.FromResult(_factory.Get().SystemConfig.MaxDate);
    }
}