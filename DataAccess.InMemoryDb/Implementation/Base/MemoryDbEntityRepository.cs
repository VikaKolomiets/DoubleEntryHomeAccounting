﻿using Common.DataAccess.Base;
using Common.Models.Interfaces;
using Common.Utils.Convertors;

namespace DataAccess.InMemoryDb.Implementation.Base;

public abstract class MemoryDbEntityRepository<T> : IEntityRepository<T>
    where T : IEntity, ITrackedEntity
{
    private readonly ILedgerFactory _factory;

    protected MemoryDbEntityRepository(ILedgerFactory factory)
    {
        _factory = factory;
    }

    public Task<T> Get(Guid id)
    {
        return Task.FromResult(GetEntities().FirstOrDefault(e => e.Id == id));
    }

    public Task<List<T>> FindBy(Func<T, bool> predicate, int top = 0)
    {
        IEnumerable<T> entities = this.GetEntities().Where(predicate);
        if (top > 0)
        {
            entities = entities.Take(top);
        }
        return Task.FromResult(entities.ToList());
    }

    public Task Add(T entity)
    {
        entity.TimeStamp = DateTimeConvertor.ConvertToString();
        this.GetEntities().Add(entity);
        this.GetAddedIds().Add(entity.Id);
        return Task.FromResult((object)null);
    }

    public Task AddList(List<T> list)
    {
        string dateTimeStamp = DateTimeConvertor.ConvertToString();
        foreach (T e in list)
        {
            e.TimeStamp = dateTimeStamp;
            this.GetEntities().Add(e);
            this.GetAddedIds().Add(e.Id);
        }
        return Task.FromResult((object)null);
    }

    public Task Update(T entity)
    {
        entity.TimeStamp = DateTimeConvertor.ConvertToString();
        this.GetUpdatedIds().Add(entity.Id);
        return Task.FromResult((object)null);
    }

    public Task UpdateList(List<T> list)
    {
        string dateTimeStamp = DateTimeConvertor.ConvertToString();
        foreach (T e in list)
        {
            e.TimeStamp = dateTimeStamp;
            this.GetUpdatedIds().Add(e.Id);
        }
        return Task.FromResult((object)null);
    }

    public Task Delete(T entity)
    {
        this.GetEntities().Remove(entity);
        this.GetDeletedIds().Add(entity.Id);
        return Task.FromResult((object)null);
    }

    public Task DeleteList(List<T> list)
    {
        foreach (T e in list)
        {
            this.GetEntities().Remove(e);
            this.GetDeletedIds().Add(e.Id);
        }
        return Task.FromResult((object)null);
    }

    #region Protected Virtual

    protected abstract List<T> GetEntities();

    #endregion

    #region Protected non-virtual

    protected Ledger GetLedger()
    {
        return _factory.Get();
    }

    protected List<Guid> GetAddedIds()
    {
        Ledger ledger = _factory.Get();
        return ledger.AddedIds;
    }

    protected List<Guid> GetUpdatedIds()
    {
        Ledger ledger = _factory.Get();
        return ledger.UpdatedIds;
    }

    protected List<Guid> GetDeletedIds()
    {
        Ledger ledger = _factory.Get();
        return ledger.DeletedIds;
    }
    #endregion
}
