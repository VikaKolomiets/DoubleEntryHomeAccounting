﻿using Common.DataAccess.Base;
using Common.Models.Interfaces;

namespace DataAccess.InMemoryDb.Implementation.Base;

public abstract class MemoryDbParentEntityRepository<T> : MemoryDbEntityRepository<T>, IParentEntityRepository<T>
    where T: IEntity, ITrackedEntity, INamedEntity, IOrderedEntity
{
    protected MemoryDbParentEntityRepository(ILedgerFactory factory) : base(factory)
    {
    }

    public Task<List<T>> GetByName(string name)
    {
        return Task.FromResult(GetEntities().Where(e=>e.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)).ToList());
    }

    public Task<int> GetMaxOrder()
    {
        List<T> list = GetEntities();
        if (list.Count == 0)
        {
            Task.FromResult(0);
        }
        return Task.FromResult(list.Max(e => e.Order));
    }

    public Task<int> GetCount()
    {
        return Task.FromResult(GetEntities().Count);
    }

    public Task<List<T>> GetList()
    {
        return Task.FromResult(GetEntities());
    }

    public Task LoadChildren(T entity)
    {
        return Task.FromResult((object)null);
        //Only for Relational Db
    }
}