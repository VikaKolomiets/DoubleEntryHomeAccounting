﻿using Common.DataAccess.Base;
using Common.Models.Interfaces;

namespace DataAccess.InMemoryDb.Implementation.Base;

public abstract class MemoryDbChildEntityRepository<T, TParent> : MemoryDbEntityRepository<T>, IChildEntityRepository<T>
    where T : IEntity, ITrackedEntity, INamedEntity, IOrderedEntity, IChildEntity<TParent>
    where TParent: IEntity
{
    protected MemoryDbChildEntityRepository(ILedgerFactory factory) : base(factory)
    {
    }

    public Task<List<T>> GetByName(Guid parentId, string name)
    {
        return Task.FromResult(GetEntities(parentId).Where(e => e.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)).ToList());
    }

    public Task<int> GetMaxOrder(Guid parentId)
    {
        List<T> list = GetEntities(parentId);
        if (list.Count == 0)
        {
            return Task.FromResult(0);
        }
        return Task.FromResult(list.Max(e => e.Order));

    }

    public Task<int> GetCount(Guid parentId)
    {
        return Task.FromResult(GetEntities(parentId).Count);
    }

    public Task<List<T>> GetList(Guid parentId)
    {
        return Task.FromResult(GetEntities(parentId));
    }

    public Task LoadParent(T entity)
    {
        return Task.FromResult((object)null);
        //Only for Relational Db
    }

    #region Private Members

    protected List<T> GetEntities(Guid parentId)
    {
        return GetEntities().Where(e=>e.Parent.Id == parentId).ToList();
    }

    #endregion

}