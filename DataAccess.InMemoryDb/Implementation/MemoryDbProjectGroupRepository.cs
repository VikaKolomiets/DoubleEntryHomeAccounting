﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbProjectGroupRepository : MemoryDbParentEntityRepository<ProjectGroup>, IProjectGroupRepository
{
    public MemoryDbProjectGroupRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<ProjectGroup> GetEntities()
    {
        return GetLedger().ProjectGroups;
    }
}