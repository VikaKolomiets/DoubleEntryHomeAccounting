﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbCorrespondentGroupRepository : MemoryDbParentEntityRepository<CorrespondentGroup>, ICorrespondentGroupRepository
{
    public MemoryDbCorrespondentGroupRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<CorrespondentGroup> GetEntities()
    {
        return GetLedger().CorrespondentGroups;
    }
}