﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbCorrespondentRepository :  MemoryDbChildEntityRepository<Correspondent, CorrespondentGroup>, ICorrespondentRepository
{
    public MemoryDbCorrespondentRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<Correspondent> GetEntities()
    {
        return GetLedger().Correspondents;
    }
}