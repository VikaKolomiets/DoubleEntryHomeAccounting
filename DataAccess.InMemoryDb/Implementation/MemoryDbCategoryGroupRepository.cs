﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbCategoryGroupRepository : MemoryDbParentEntityRepository<CategoryGroup>, ICategoryGroupRepository
{
    public MemoryDbCategoryGroupRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<CategoryGroup> GetEntities()
    {
        return GetLedger().CategoryGroups;
    }
}