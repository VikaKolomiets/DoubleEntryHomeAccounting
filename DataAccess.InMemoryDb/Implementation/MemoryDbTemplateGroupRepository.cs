﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbTemplateGroupRepository : MemoryDbParentEntityRepository<TemplateGroup>, ITemplateGroupRepository
{
    public MemoryDbTemplateGroupRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<TemplateGroup> GetEntities()
    {
        return GetLedger().TemplateGroups;
    }
}