﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbAccountGroupRepository : MemoryDbParentEntityRepository<AccountGroup>, IAccountGroupRepository
{
    public MemoryDbAccountGroupRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<AccountGroup> GetEntities()
    {
        return GetLedger().AccountGroups;
    }
}