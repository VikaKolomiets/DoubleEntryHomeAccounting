﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbCurrencyRateRepository : MemoryDbEntityRepository<CurrencyRate>, ICurrencyRateRepository
{
    public MemoryDbCurrencyRateRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<CurrencyRate> GetEntities()
    {
        return GetLedger().CurrencyRates;
    }
}