﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbCurrencyRepository : MemoryDbEntityRepository<Currency>, ICurrencyRepository
{
    public MemoryDbCurrencyRepository(ILedgerFactory factory) : base(factory)
    {
    }

    public Task<Currency> GetByIsoCode(string isoCode)
    {
        return Task.FromResult(GetEntities().FirstOrDefault(g=>g.IsoCode.Equals(isoCode, StringComparison.InvariantCultureIgnoreCase)));
    }

    public Task<int> GetMaxOrder()
    {
        List<Currency> list = GetEntities();
        if (list.Count == 0)
        {
            return Task.FromResult(0);
        }
        return Task.FromResult(list.Max(e => e.Order));
    }

    public Task<int> GetCount()
    {
        return Task.FromResult(GetEntities().Count);
    }

    public Task<List<Currency>> GetList()
    {
        return Task.FromResult(GetEntities());
    }

    protected override List<Currency> GetEntities()
    {
        return GetLedger().Currencies;
    }
}