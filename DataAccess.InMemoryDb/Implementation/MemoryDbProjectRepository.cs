﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbProjectRepository :  MemoryDbChildEntityRepository<Project, ProjectGroup>, IProjectRepository
{
    public MemoryDbProjectRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<Project> GetEntities()
    {
        return GetLedger().Projects;
    }
}