﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbAccountSubGroupRepository : MemoryDbChildEntityRepository<AccountSubGroup, AccountGroup>, IAccountSubGroupRepository
{
    public MemoryDbAccountSubGroupRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<AccountSubGroup> GetEntities()
    {
        return GetLedger().AccountSubGroups;
    }

    public Task LoadChildren(AccountSubGroup entity)
    {
        return Task.FromResult((object)null);
        //Only for Relational Db
    }

    public Task<List<AccountSubGroup>> GetByName(string name)
    {
        //TODO: Implementation
        throw new NotImplementedException();
    }

    public Task<int> GetMaxOrder()
    {
        //TODO: Implementation
        throw new NotImplementedException();
    }

    public Task<int> GetCount()
    {
        //TODO: Implementation
        throw new NotImplementedException();
    }

    public Task<List<AccountSubGroup>> GetList()
    {
        //TODO: Implementation
        throw new NotImplementedException();
    }
}