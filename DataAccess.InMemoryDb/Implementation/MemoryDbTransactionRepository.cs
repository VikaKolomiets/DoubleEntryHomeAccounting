﻿using DataAccess.InMemoryDb.Implementation.Base;
using Common.Models;
using Common.DataAccess;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbTransactionRepository : MemoryDbEntityRepository<Transaction>, ITransactionRepository
{
    public MemoryDbTransactionRepository(ILedgerFactory factory) : base(factory)
    {
    }

    public Task<List<TransactionEntry>> GetEntriesByAccount(Account account)
    {
        return Task.FromResult(account != null ? GetNotNullAccountEntries(account.Id).ToList() : GetNullAccountEntries().ToList());
    }

    public Task<int> GetTransactionEntriesCount(Guid accountId)
    {
        return Task.FromResult(accountId != Guid.Empty ? GetNotNullAccountEntries(accountId).Count() : GetNullAccountEntries().Count());
    }

    protected override List<Transaction> GetEntities()
    {
        return GetLedger().Transactions;
    }

    private IEnumerable<TransactionEntry> GetNotNullAccountEntries(Guid accountId)
    {
        return GetEntities()
            .SelectMany(e => e.Entries)
            .Where(e => e.Account != null && e.Account.Id == accountId)
            .ToList();
    }

    private IEnumerable<TransactionEntry> GetNullAccountEntries()
    {
        return GetEntities()
            .SelectMany(e => e.Entries)
            .Where(e => e.Account == null);
    }
}