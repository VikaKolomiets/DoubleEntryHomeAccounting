﻿using Common.DataAccess;
using Common.Models;
using DataAccess.InMemoryDb.Implementation.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.InMemoryDb.Implementation;

public class MemoryDbAccountRepository : MemoryDbChildEntityRepository<Account, AccountSubGroup>, IAccountRepository
{
    public MemoryDbAccountRepository(ILedgerFactory factory) : base(factory)
    {
    }

    protected override List<Account> GetEntities()
    {
        return GetLedger().Accounts;
    }

    public Task<List<Account>> GetAccountsByCorrespondent(Correspondent correspondent)
    {
        return Task.FromResult(GetEntities().Where(e=> e.Correspondent.Id == correspondent.Id).ToList());
    }

    public Task<List<Account>> GetAccountsByCategory(Category category)
    {
        return Task.FromResult(GetEntities().Where(e => e.Category.Id == category.Id).ToList());
    }

    public Task<List<Account>> GetAccountsByProject(Project project)
    {
        return Task.FromResult(GetEntities().Where(e => e.Project.Id == project.Id).ToList());
    }

    public Task<List<Account>> GetAccountsByCurrency(Currency currency)
    {
        return Task.FromResult(GetEntities().Where(e => e.Currency.Id == currency.Id).ToList());
    }

    public Task LoadCurrency(Account account)
    {
        return Task.FromResult((object)null);
        //Only for Relational Db
    }
}