﻿using Common.Models;
using Common.Models.Config;
using Common.Models.Interfaces;

namespace DataAccess.InMemoryDb;

public class Ledger
{
    public List<AccountGroup> AccountGroups { get; } = new();
    public List<AccountSubGroup> AccountSubGroups { get; } = new();
    public List<Account> Accounts { get; } = new();
    public List<CategoryGroup> CategoryGroups { get; } = new();
    public List<Category> Categories { get; } = new();
    public List<CorrespondentGroup> CorrespondentGroups { get; } = new();
    public List<Correspondent> Correspondents { get; } = new();
    public List<Currency> Currencies { get; } = new();
    public List<CurrencyRate> CurrencyRates { get; } = new();
    public List<ProjectGroup> ProjectGroups { get; } = new();
    public List<Project> Projects { get; } = new();
    public List<TemplateGroup> TemplateGroups { get; } = new();
    public List<Template> Templates { get; } = new();
    public List<TemplateEntry> TemplateEntries { get; } = new();
    public List<Transaction> Transactions { get; } = new();
    public List<TransactionEntry> TransactionEntries { get; } = new();

    public List<List<IEntity>> AllLists { get; } = new();

    public List<Guid> AddedIds { get; } = new();
    public List<Guid> UpdatedIds { get; } = new();
    public List<Guid> DeletedIds { get; } = new();

    public SystemConfig SystemConfig { get; } = new();
    public UserConfig UserConfig { get; } = new();
}