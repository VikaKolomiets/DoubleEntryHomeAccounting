﻿using Common.Queries;
using Common.Infos;
using DataAccess.Mappers;

namespace DataAccess.InMemoryDb.Queries;

public class LedgerQueries : ILedgerQueries
{
    private readonly ILedgerFactory _factory;

    public LedgerQueries(ILedgerFactory factory)
    {
        _factory = factory;
    }

    public LedgerInfo Get()
    {
        LedgerInfo ledgerInfo = new LedgerInfo();
        Ledger ledger = _factory.Get();

        ledgerInfo.AccountGroups.AddRange(ledger.AccountGroups.Select(e => e.Map()));
        ledgerInfo.AccountSubGroups.AddRange(ledger.AccountSubGroups.Select(e => e.Map()));
        ledgerInfo.Accounts.AddRange(ledger.Accounts.Select(e => e.Map()));

        ledgerInfo.Currencies.AddRange(ledger.Currencies.Select(e => e.Map()));
        ledgerInfo.CurrencyRates.AddRange(ledger.CurrencyRates.Select(e => e.Map()));
            
        ledgerInfo.CategoryGroups.AddRange(ledger.CategoryGroups.Select(e => e.Map()));
        ledgerInfo.Categories.AddRange(ledger.Categories.Select(e => e.Map()));

        ledgerInfo.CorrespondentGroups.AddRange(ledger.CorrespondentGroups.Select(e => e.Map()));
        ledgerInfo.Correspondents.AddRange(ledger.Correspondents.Select(e => e.Map()));

        ledgerInfo.ProjectGroups.AddRange(ledger.ProjectGroups.Select(e => e.Map()));
        ledgerInfo.Projects.AddRange(ledger.Projects.Select(e => e.Map()));

        ledgerInfo.TemplateGroups.AddRange(ledger.TemplateGroups.Select(e => e.Map()));
        ledgerInfo.Templates.AddRange(ledger.Templates.Select(e => e.Map()));
        ledgerInfo.TemplateEntries.AddRange(ledger.TemplateEntries.Select(e => e.Map()));

        ledgerInfo.Transactions.AddRange(ledger.Transactions.Select(e => e.Map()));
        ledgerInfo.TransactionEntries.AddRange(ledger.TransactionEntries.Select(e => e.Map()));

        return ledgerInfo;
    }
}