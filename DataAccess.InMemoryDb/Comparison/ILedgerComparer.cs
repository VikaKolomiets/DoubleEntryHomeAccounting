﻿namespace DataAccess.InMemoryDb.Comparison
{
    public interface ILedgerComparer
    {
        List<String> Compare(Ledger first, Ledger second);
    }
}
