﻿using System.Globalization;
using System.Xml.Linq;
using Common.Models;
using Common.Models.Base;
using Common.Models.Enums;
using Common.Models.Interfaces;

namespace XmlConsole;

public class XmlSerializer
{
    public Ledger Deserialize(XDocument xDocument)
    {
        var ledger = new Ledger();
        var root = xDocument.Root;

        DeserializeProjectGroups(root, ledger);
        DeserializeCategoryGroups(root, ledger);
        DeserializeCorrespondentGroups(root, ledger);
        DeserializeCurrencies(root, ledger);
        DeserializeAccountGroups(root, ledger);
        DeserializeTemplateGroups(root, ledger);
        DeserializeTransactions(root, ledger);

        return ledger;
    }

    public XDocument Serialize(Ledger ledger)
    {
        var root = new XElement(Ledger);
        var doc = new XDocument(root);

        SerializeProjectSection(root, ledger);
        SerializeCorrespondentSection(root, ledger);
        SerializeCategorySection(root, ledger);
        SerializeCurrencySection(root, ledger);
        SerializeAccountSection(root, ledger);
        SerializeTemplateSection(root, ledger);
        SerializeTransactionSection(root, ledger);

        return doc;
    }

    #region Node Constants

    public const string Ledger = "Ledger";
    public const string ProjectSection = "ProjectSection";
    public const string ProjectGroup = "ProjectGroup";
    public const string Project = "Project";
    public const string CorrespondentSection = "CorrespondentSection";
    public const string CorrespondentGroup = "CorrespondentGroup";
    public const string Correspondent = "Correspondent";
    public const string CategorySection = "CategorySection";
    public const string CategoryGroup = "CategoryGroup";
    public const string Category = "Category";
    public const string CurrencySection = "CurrencySection";
    public const string Currency = "Currency";
    public const string RateCurrency = "RateCurrency";
    public const string TemplateSection = "TemplateSection";
    public const string TemplateGroup = "TemplateGroup";
    public const string Template = "Template";
    public const string TemplateEntry = "TemplateEntry";
    public const string AccountSection = "AccountSection";
    public const string AccountGroup = "AccountGroup";
    public const string AccountSubGroup = "AccountSubGroup";
    public const string Account = "Account";
    public const string TransactionSection = "TransactionSection";
    public const string Transaction = "Transaction";
    public const string TransactionEntry = "TransactionEntry";

    public const string Id = "Id";
    public const string Name = "Name";
    public const string Description = "Description";
    public const string TimeStamp = "TimeStamp";
    public const string Order = "Order";
    public const string IsFavorite = "IsFavorite";
    public const string IsoCode = "IsoCode";
    public const string Symbol = "Symbol";
    public const string Rate = "Rate";
    public const string Date = "Date";
    public const string Comment = "Comment";
    public const string ProjectId = "ProjectId";
    public const string CorrespondentId = "CorrespondentId";
    public const string CategoryId = "CategoryId";
    public const string CurrencyId = "CurrencyId";
    public const string AccountId = "AccountId";
    public const string Amount = "Amount";
    public const string DateTime = "DateTime";
    public const string State = "TransactionState";
    public const string TransactionId = "TransactionId";

    public const string DateTimeFormat = "yyyyMMddHHmmss";
    public const string TimeStampFormat = "yyyyMMddHHmmssfff";
    public const string DateFormat = "yyyyMMdd";
    public const string GuidFormat = "n";
    public const string DecimalFormat = "N4";

    #endregion

    #region Serialize

    public static void SerializeProjectSection(XElement root, Ledger ledger)
    {
        var xmlProjectSection = new XElement(ProjectSection);
        root.Add(xmlProjectSection);
        foreach (var projectGroup in ledger.ProjectGroups)
        {
            var xmlProjectGroup = CreateElement(ProjectGroup, projectGroup);
            xmlProjectSection.Add(xmlProjectGroup);
            foreach (var project in projectGroup.Children) xmlProjectGroup.Add(CreateElement(Project, project));
        }
    }

    public static void SerializeCategorySection(XElement root, Ledger ledger)
    {
        var xmlCategorySection = new XElement(CategorySection);
        root.Add(xmlCategorySection);
        foreach (var categoryGroup in ledger.CategoryGroups)
        {
            var xmlCategotyGroup = CreateElement(CategoryGroup, categoryGroup);
            xmlCategorySection.Add(xmlCategotyGroup);
            foreach (var category in categoryGroup.Children) xmlCategotyGroup.Add(CreateElement(Category, category));
        }
    }

    public static void SerializeCorrespondentSection(XElement root, Ledger ledger)
    {
        var xmlCorrespondentSection = new XElement(CorrespondentSection);
        root.Add(xmlCorrespondentSection);
        foreach (var correspondentGroup in ledger.CorrespondentGroups)
        {
            var xmlCorrespondentGroup = CreateElement(CorrespondentGroup, correspondentGroup);
            xmlCorrespondentSection.Add(xmlCorrespondentGroup);

            foreach (var correspondent in correspondentGroup.Children)
            {
                var xmlCorrespondent = CreateElement(Correspondent, correspondent);
                xmlCorrespondentGroup.Add(xmlCorrespondent);
            }
        }
    }

    public static void SerializeCurrencySection(XElement root, Ledger ledger)
    {
        var xmlCurrencySection = new XElement(CurrencySection);
        root.Add(xmlCurrencySection);
        foreach (var currency in ledger.Currencies)
        {
            var xmlCurrency = new XElement(Currency);
            xmlCurrency.Add(new XAttribute(Id, currency.Id.ToString(GuidFormat)));
            xmlCurrency.Add(new XAttribute(IsoCode, currency.IsoCode));
            xmlCurrency.Add(new XAttribute(Symbol, currency.Symbol));
            xmlCurrency.Add(new XAttribute(TimeStamp, currency.TimeStamp));
            xmlCurrency.Add(new XAttribute(Order, currency.Order));
            xmlCurrency.Add(new XAttribute(IsFavorite, currency.IsFavorite));
            xmlCurrencySection.Add(xmlCurrency);

            foreach (var rateCurrency in currency.Rates)
            {
                var xmlRateCurrency = new XElement(RateCurrency);
                xmlRateCurrency.Add(new XAttribute(Id, rateCurrency.Id.ToString(GuidFormat)));
                xmlRateCurrency.Add(new XAttribute(Rate,
                    rateCurrency.Rate.ToString(DecimalFormat, CultureInfo.InvariantCulture)));
                if (rateCurrency.Comment != null) xmlRateCurrency.Add(new XAttribute(Comment, rateCurrency.Comment));
                xmlRateCurrency.Add(new XAttribute(TimeStamp, rateCurrency.TimeStamp));
                xmlRateCurrency.Add(new XAttribute(Date, rateCurrency.Date.ToString(DateFormat)));
                xmlCurrency.Add(xmlRateCurrency);
            }
        }
    }

    public static void SerializeAccountSection(XElement root, Ledger ledger)
    {
        var xmlAccountSection = new XElement(AccountSection);
        root.Add(xmlAccountSection);
        foreach (var accountGroup in ledger.AccountGroups)
        {
            var xmlAccoutGroup = CreateElement(AccountGroup, accountGroup);
            xmlAccountSection.Add(xmlAccoutGroup);
            foreach (var accountSubGroup in accountGroup.Children)
            {
                var xmlAccountSubGroup = CreateElement(AccountSubGroup, accountSubGroup);
                xmlAccoutGroup.Add(xmlAccountSubGroup);
                foreach (var account in accountSubGroup.Children)
                {
                    var xmlAccount = CreateElement(Account, account);
                    xmlAccount.Add(new XAttribute(CurrencyId, account.Currency.Id.ToString(GuidFormat)));
                    if (account.Correspondent != null)
                        xmlAccount.Add(new XAttribute(CorrespondentId, account.Correspondent.Id.ToString(GuidFormat)));
                    if (account.Category != null)
                        xmlAccount.Add(new XAttribute(CategoryId, account.Category.Id.ToString(GuidFormat)));
                    if (account.Project != null)
                        xmlAccount.Add(new XAttribute(ProjectId, account.Project.Id.ToString(GuidFormat)));
                    xmlAccountSubGroup.Add(xmlAccount);
                }
            }
        }
    }

    public static void SerializeTemplateSection(XElement root, Ledger ledger)
    {
        var xmlTemplateSection = new XElement(TemplateSection);
        root.Add(xmlTemplateSection);
        foreach (var templateGroup in ledger.TemplateGroups)
        {
            var xmlTemplateGroup = CreateElement(TemplateGroup, templateGroup);
            xmlTemplateSection.Add(xmlTemplateGroup);
            foreach (var template in templateGroup.Children)
            {
                var xmlTemplate = CreateElement(Template, template);
                xmlTemplateGroup.Add(xmlTemplate);
                foreach (var templateEntry in template.Entries)
                {
                    var xmlTemplateEntry = new XElement(TemplateEntry);
                    xmlTemplateEntry.Add(new XAttribute(Id, templateEntry.Id.ToString(GuidFormat)));
                    xmlTemplateEntry.Add(new XAttribute(AccountId, templateEntry.Account.Id.ToString(GuidFormat)));
                    if (templateEntry.Amount != null)
                        xmlTemplateEntry.Add(new XAttribute(Amount,
                            templateEntry.Amount.ToString(DecimalFormat, CultureInfo.InvariantCulture)));
                    xmlTemplate.Add(xmlTemplateEntry);
                }
            }
        }
    }

    public static void SerializeTransactionSection(XElement root, Ledger ledger)
    {
        var xmlTransactionSection = new XElement(TransactionSection);
        root.Add(xmlTransactionSection);
        foreach (var transaction in ledger.Transactions)
        {
            var xmlTransaction = new XElement(Transaction);
            xmlTransaction.Add(new XAttribute(Id, transaction.Id.ToString(GuidFormat)));
            xmlTransaction.Add(new XAttribute(TimeStamp, transaction.TimeStamp));
            xmlTransaction.Add(new XAttribute(DateTime, transaction.DateTime.ToString(DateTimeFormat)));
            xmlTransaction.Add(new XAttribute(State, transaction.State.ToString()));
            if (transaction.Comment != null) xmlTransaction.Add(new XAttribute(Comment, transaction.Comment));
            xmlTransactionSection.Add(xmlTransaction);
            foreach (var transactionEntry in transaction.Entries)
            {
                var xmlTransactionEntry = new XElement(TransactionEntry);
                xmlTransactionEntry.Add(new XAttribute(Id, transactionEntry.Id.ToString(GuidFormat)));
                xmlTransactionEntry.Add(new XAttribute(AccountId, transactionEntry.Account.Id.ToString(GuidFormat)));
                xmlTransactionEntry.Add(new XAttribute(Amount,
                    transactionEntry.Amount.ToString(DecimalFormat, CultureInfo.InvariantCulture)));
                xmlTransactionEntry.Add(new XAttribute(Rate,
                    transactionEntry.Rate.ToString(DecimalFormat, CultureInfo.InvariantCulture)));
                xmlTransaction.Add(xmlTransactionEntry);
            }
        }
    }

    public static XElement CreateElement(string elementName, ReferenceEntity entity)
    {
        var xmlElement = new XElement(elementName);
        xmlElement.Add(new XAttribute(Id, entity.Id.ToString(GuidFormat)));
        xmlElement.Add(new XAttribute(Name, entity.Name));
        if (!string.IsNullOrEmpty(entity.Description)) xmlElement.Add(new XAttribute(Description, entity.Description));
        xmlElement.Add(new XAttribute(TimeStamp, entity.TimeStamp));
        xmlElement.Add(new XAttribute(Order, entity.Order));
        xmlElement.Add(new XAttribute(IsFavorite, entity.IsFavorite));

        return xmlElement;
    }

    #endregion


    #region Deserialize

    public static void DeserializeProjectGroups(XElement root, Ledger ledger)
    {
        var projectSection = root.Element(ProjectSection);
        var projectGroups = projectSection.Elements().ToList();
        foreach (var xmlGroup in projectGroups)
        {
            var projectGroup = new ProjectGroup();
            SetDirectoryEntityData(xmlGroup, projectGroup);
            ledger.ProjectGroups.Add(projectGroup);
            DeserializeProjects(xmlGroup, ledger, projectGroup);
        }
    }

    public static void DeserializeProjects(XElement xmlGroup, Ledger ledger, ProjectGroup projectGroup)
    {
        var projects = xmlGroup.Elements().ToList();
        foreach (var xmlProject in projects)
        {
            var project = new Project();
            SetDirectoryEntityData(xmlProject, project);
            project.Parent = projectGroup;
            projectGroup.Children.Add(project);
            ledger.Projects.Add(project);
        }
    }

    public static void DeserializeCategoryGroups(XElement root, Ledger ledger)
    {
        var caterorySection = root.Element(CategorySection);
        var categoryGroups = caterorySection.Elements().ToList();
        foreach (var xmlGroup in categoryGroups)
        {
            var categoryGroup = new CategoryGroup();
            SetDirectoryEntityData(xmlGroup, categoryGroup);
            ledger.CategoryGroups.Add(categoryGroup);
            DeserializeCategories(xmlGroup, ledger, categoryGroup);
        }
    }

    public static void DeserializeCategories(XElement xmlGroup, Ledger ledger, CategoryGroup categoryGroup)
    {
        var categories = xmlGroup.Elements().ToList();
        foreach (var xmlCategory in categories)
        {
            var category = new Category();
            SetDirectoryEntityData(xmlCategory, category);
            category.Parent = categoryGroup;
            categoryGroup.Children.Add(category);
            ledger.Categories.Add(category);
        }
    }

    public static void DeserializeCorrespondentGroups(XElement root, Ledger ledger)
    {
        var correspondentSection = root.Element(CorrespondentSection);
        var correspondentGroups = correspondentSection.Elements().ToList();
        foreach (var xmlGroup in correspondentGroups)
        {
            var correspondentGroup = new CorrespondentGroup();
            SetDirectoryEntityData(xmlGroup, correspondentGroup);
            ledger.CorrespondentGroups.Add(correspondentGroup);
            DeserializeCorrespondents(xmlGroup, ledger, correspondentGroup);
        }
    }

    public static void DeserializeCorrespondents(XElement xmlGroup, Ledger ledger,
        CorrespondentGroup correspondentGroup)
    {
        var correspondents = xmlGroup.Elements().ToList();
        foreach (var xmlCorrespondet in correspondents)
        {
            var correspondent = new Correspondent();
            SetDirectoryEntityData(xmlCorrespondet, correspondent);
            correspondent.Parent = correspondentGroup;
            correspondentGroup.Children.Add(correspondent);
            ledger.Correspondents.Add(correspondent);
        }
    }

    public static void DeserializeAccountGroups(XElement root, Ledger ledger)
    {
        var accountSection = root.Element(AccountSection);
        var accountGroups = accountSection.Elements().ToList();
        foreach (var xmlGroup in accountGroups)
        {
            var accountGroup = new AccountGroup();
            SetDirectoryEntityData(xmlGroup, accountGroup);
            ledger.AccountGroups.Add(accountGroup);
            DeserializeAccountSubGroups(xmlGroup, ledger, accountGroup);
        }
    }

    public static void DeserializeAccountSubGroups(XElement xmlGroup, Ledger ledger, AccountGroup accountGroup)
    {
        var accountSubGroups = xmlGroup.Elements().ToList();
        foreach (var xmlSubGroup in accountSubGroups)
        {
            var accountSubGroup = new AccountSubGroup();
            SetDirectoryEntityData(xmlSubGroup, accountSubGroup);
            accountSubGroup.Parent = accountGroup;
            accountGroup.Children.Add(accountSubGroup);
            ledger.AccountSubGroups.Add(accountSubGroup);
            DeserializeAccount(xmlSubGroup, ledger, accountSubGroup);
        }
    }

    public static void DeserializeAccount(XElement xmlSubGroup, Ledger ledger, AccountSubGroup accountSubGroup)
    {
        var accounts = xmlSubGroup.Elements().ToList();
        foreach (var xmlAccount in accounts)
        {
            var account = new Account();
            SetDirectoryEntityData(xmlAccount, account);

            var currencyId = Guid.Parse(xmlAccount.Attribute(CurrencyId).Value);
            account.Currency = ledger.Currencies.First(c => c.Id == currencyId);

            if (xmlAccount.Attribute(CorrespondentId) != null)
            {
                var correspondentId = Guid.Parse(xmlAccount.Attribute(CorrespondentId).Value);
                account.Correspondent = ledger.Correspondents.First(cr => cr.Id == correspondentId);
            }

            if (xmlAccount.Attribute(ProjectId) != null)
            {
                var projectGuid = Guid.Parse(xmlAccount.Attribute(ProjectId).Value);
                account.Project = ledger.Projects.First(p => p.Id == projectGuid);
            }

            if (xmlAccount.Attribute(CategoryId) != null)
            {
                var categoryGuid = Guid.Parse(xmlAccount.Attribute(CategoryId).Value);
                account.Category = ledger.Categories.First(ct => ct.Id == categoryGuid);
            }

            account.Parent = accountSubGroup;
            accountSubGroup.Children.Add(account);
            ledger.Accounts.Add(account);
        }
    }

    public static void DeserializeCurrencies(XElement root, Ledger ledger)
    {
        var currencySection = root.Element(CurrencySection);
        var currencies = currencySection.Elements().ToList();
        foreach (var xmlCurrency in currencies)
        {
            var currency = new Currency();
            SetId(xmlCurrency, currency);
            SetOrder(xmlCurrency, currency);
            SetDateStamp(xmlCurrency, currency);
            SetFavoriteEntity(xmlCurrency, currency);
            currency.IsoCode = xmlCurrency.Attribute(IsoCode).Value;
            currency.Symbol = xmlCurrency.Attribute(Symbol).Value;
            ledger.Currencies.Add(currency);
            DeserializeRateCurrencies(xmlCurrency, ledger, currency);
        }
    }

    public static void DeserializeRateCurrencies(XElement xmlCurrency, Ledger ledger, Currency currency)
    {
        var rateCurrencies = xmlCurrency.Elements().ToList();
        foreach (var xmlRate in rateCurrencies)
        {
            var rateCurrency = new CurrencyRate();
            SetId(xmlRate, rateCurrency);
            SetDateStamp(xmlRate, rateCurrency);
            rateCurrency.Comment = xmlRate.Attribute(Comment).Value;
            rateCurrency.Rate = decimal.Parse(xmlRate.Attribute(Rate).Value, CultureInfo.InvariantCulture);
            var date = xmlRate.Attribute(Date).Value;
            rateCurrency.Date = System.DateTime.ParseExact(date, DateFormat, CultureInfo.InvariantCulture);
            ledger.RateCurrencies.Add(rateCurrency);
            currency.Rates.Add(rateCurrency);
            rateCurrency.Currency = currency;
        }
    }

    public static void DeserializeTemplateGroups(XElement root, Ledger ledger)
    {
        var templateSection = root.Element(TemplateSection);
        var templateGroups = templateSection.Elements().ToList();
        foreach (var xmlGroup in templateGroups)
        {
            var templateGroup = new TemplateGroup();
            SetDirectoryEntityData(xmlGroup, templateGroup);
            ledger.TemplateGroups.Add(templateGroup);
            DeserializeTemplates(xmlGroup, templateGroup, ledger);
        }
    }

    public static void DeserializeTemplates(XElement xmlGroup, TemplateGroup templateGroup, Ledger ledger)
    {
        var templates = xmlGroup.Elements().ToList();
        foreach (var xmlTemplate in templates)
        {
            var template = new Template();
            SetDirectoryEntityData(xmlTemplate, template);
            template.Parent = templateGroup;
            templateGroup.Children.Add(template);
            ledger.Templates.Add(template);
            DeserializeTemplateEntries(xmlTemplate, template, ledger);
        }
    }

    public static void DeserializeTemplateEntries(XElement xmlTemplate, Template template, Ledger ledger)
    {
        var templateEntries = xmlTemplate.Elements().ToList();
        foreach (var xmlEntry in templateEntries)
        {
            var templateEntry = new TemplateEntry();
            SetId(xmlEntry, templateEntry);
            templateEntry.Amount = decimal.Parse(xmlEntry.Attribute(Amount).Value, CultureInfo.InvariantCulture);
            if (xmlEntry.Attribute(AccountId) != null)
            {
                var accoutGuid = Guid.Parse(xmlEntry.Attribute(AccountId).Value);
                templateEntry.Account = ledger.Accounts.First(ac => ac.Id == accoutGuid);
            }

            templateEntry.Template = template;
            template.Entries.Add(templateEntry);
            ledger.TemplateEntries.Add(templateEntry);
        }
    }

    public static void DeserializeTransactions(XElement root, Ledger ledger)
    {
        var transactionSection = root.Element(TransactionSection);
        var transactions = transactionSection.Elements().ToList();
        foreach (var xmlTransaction in transactions)
        {
            var transaction = new Transaction();
            SetId(xmlTransaction, transaction);
            SetDateStamp(xmlTransaction, transaction);
            if (xmlTransaction.Attribute(Comment) != null)
                transaction.Comment = xmlTransaction.Attribute(Comment).Value;
            var date = xmlTransaction.Attribute(DateTime).Value;
            transaction.DateTime = System.DateTime.ParseExact(date, DateTimeFormat, CultureInfo.InvariantCulture);
            var value = xmlTransaction.Attribute(State).Value;
            transaction.State = (TransactionState)Enum.Parse(typeof(TransactionState), value);
            ledger.Transactions.Add(transaction);
            DeserializeTransactionEntries(xmlTransaction, transaction, ledger);
        }
    }

    public static void DeserializeTransactionEntries(XElement xmlTransaction, Transaction transaction, Ledger ledger)
    {
        var transactionEntries = xmlTransaction.Elements().ToList();
        foreach (var xmlEntry in transactionEntries)
        {
            var transactionEntry = new TransactionEntry();
            SetId(xmlEntry, transactionEntry);
            transactionEntry.Rate = decimal.Parse(xmlEntry.Attribute(Rate).Value, CultureInfo.InvariantCulture);
            transactionEntry.Amount = decimal.Parse(xmlEntry.Attribute(Amount).Value, CultureInfo.InvariantCulture);
            var accountGuid = Guid.Parse(xmlEntry.Attribute(AccountId).Value);
            transactionEntry.Account = ledger.Accounts.First(a => a.Id == accountGuid);
            transactionEntry.Transaction = transaction;
            transaction.Entries.Add(transactionEntry);
            ledger.TransactionEntries.Add(transactionEntry);
        }
    }

    private static void SetDirectoryEntityData(XElement element, ReferenceEntity entity)
    {
        SetId(element, entity);
        SetName(element, entity);
        SetOrder(element, entity);
        SetDateStamp(element, entity);
        SetFavoriteEntity(element, entity);
    }

    private static void SetId(XElement element, IEntity entity)
    {
        entity.Id = Guid.Parse(element.Attribute(Id).Value);
    }

    private static void SetName(XElement element, INamedEntity entity)
    {
        entity.Name = element.Attribute(Name).Value;
        entity.Description = element.Attribute(Description)?.Value;
    }

    private static void SetOrder(XElement element, IOrderedEntity entity)
    {
        entity.Order = int.Parse(element.Attribute(Order).Value);
    }

    private static void SetDateStamp(XElement element, ITrackedEntity entity)
    {
        entity.TimeStamp = element.Attribute(TimeStamp).Value;
    }

    private static void SetFavoriteEntity(XElement element, IFavoriteEntity entity)
    {
        entity.IsFavorite = bool.Parse(element.Attribute(IsFavorite).Value);
    }

    #endregion
}