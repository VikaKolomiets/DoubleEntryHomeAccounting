﻿namespace XmlConsole;

public static class Loader
{
    public static Ledger Proceed()
    {
        var xmlFileStorage = new XmlFileStorage(@"c:\\MyXml.xml");
        var xmlDoc = xmlFileStorage.Load();

        var serializer = new XmlSerializer();
        var ledger = serializer.Deserialize(xmlDoc);
        return ledger;
    }
}