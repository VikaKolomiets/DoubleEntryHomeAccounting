﻿using Common.Models;

namespace XmlConsole;

public class Ledger
{
    public List<AccountGroup> AccountGroups { get; set; } = new();
    public List<AccountSubGroup> AccountSubGroups { get; set; } = new();
    public List<Account> Accounts { get; set; } = new();
    public List<CategoryGroup> CategoryGroups { get; set; } = new();
    public List<Category> Categories { get; set; } = new();
    public List<CorrespondentGroup> CorrespondentGroups { get; set; } = new();
    public List<Correspondent> Correspondents { get; set; } = new();
    public List<Currency> Currencies { get; set; } = new();
    public List<CurrencyRate> RateCurrencies { get; set; } = new();
    public List<ProjectGroup> ProjectGroups { get; set; } = new();
    public List<Project> Projects { get; set; } = new();
    public List<TemplateGroup> TemplateGroups { get; set; } = new();
    public List<Template> Templates { get; set; } = new();
    public List<TemplateEntry> TemplateEntries { get; set; } = new();
    public List<Transaction> Transactions { get; set; } = new();
    public List<TransactionEntry> TransactionEntries { get; set; } = new();
}