﻿using System.Xml.Linq;

namespace XmlConsole;

public class XmlFileStorage
{
    public XmlFileStorage(string XmlPath)
    {
        if (string.IsNullOrEmpty(XmlPath)) throw new ArgumentNullException("XmlPath is absent");
        FilePath = XmlPath;
    }

    public string FilePath { get; }

    public XDocument Load()
    {
        using (var stream = File.Open(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
        {
            var doc = XDocument.Load(stream);
            return doc;
        }
    }

    public void Save(XDocument doc)
    {
        using (var stream = File.Open(FilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
        {
            doc.Save(stream);
        }
    }
}