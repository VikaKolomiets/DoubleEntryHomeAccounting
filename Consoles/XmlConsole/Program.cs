﻿namespace XmlConsole;

internal class Program
{
    private static void Main(string[] args)
    {
        //Creator.Proceed();
        var ledger = Loader.Proceed();
        Saver.Proceed(ledger);
    }
}