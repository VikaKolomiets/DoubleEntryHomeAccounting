﻿using System.Globalization;
using System.Xml.Linq;
using Common.Models.Enums;

namespace XmlConsole;

public static class Creator
{
    public const string UahCurrency = "55bcacf26317410eace7e72de34a1c6c";
    public const string UsdCurrency = "c5b2fc83fc744d6b92a1e5c7bb59681b";
    public const string EurCurrency = "ef66e2b94c3549fab64d658f28119b08";

    public const string Mom = "e800358a6d3a42f784d60d0ff1f32b6f";
    public const string Dad = "8feb894330c04c7182c3f93fe1543cfa";


    public static void Proceed()
    {
        var root = new XElement(Ledger);
        var doc = new XDocument(root);

        var projectSection = CreateProjectGroups(root);
        CreateProjects(projectSection);
        var correspondentSection = CreateCorrespondentGroups(root);
        GreateCorrespondents(correspondentSection);
        var categorySection = CreateCategoryGroups(root);
        CreateCategories(categorySection);
        var currencySection = CreateCurrencySection(root);
        CreateRates(currencySection);
        var accountSection = CreateAccountGroups(root);
        CreateAccountSubGroups(accountSection);
        CreateAccounts(accountSection);
        var templateSection = CreateTemplateGroups(root);
        CreateTeplates(templateSection);
        CreateTemplateEntries(templateSection);
        var transactionSection = CreateTransactions(root);
        CreateTransactionEntries(transactionSection);

        using (var stream = File.Open(@"c:\\MyXml.xml", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read))
        {
            doc.Save(stream);
        }
    }


    #region Private

    public static XElement CreateElement(string elementName, string id, string name, string description,
        DateTime timeStamp, int order, bool isFavorite = false)
    {
        var element = new XElement(elementName);
        element.Add(new XAttribute(Id, id));
        element.Add(new XAttribute(Name, name));
        if (!string.IsNullOrEmpty(description)) element.Add(new XAttribute(Description, description));
        element.Add(new XAttribute(TimeStamp, timeStamp.ToString(TimeStampFormat)));
        element.Add(new XAttribute(Order, order));
        element.Add(new XAttribute(IsFavorite, isFavorite));
        return element;
    }

    #endregion

    #region Node Names

    public const string Ledger = "Ledger";
    public const string ProjectSection = "ProjectSection";
    public const string ProjectGroup = "ProjectGroup";
    public const string Project = "Project";
    public const string CorrespondentSection = "CorrespondentSection";
    public const string CorrespondentGroup = "CorrespondentGroup";
    public const string Correspondent = "Correspondent";
    public const string CategorySection = "CategorySection";
    public const string CategoryGroup = "CategoryGroup";
    public const string Category = "Category";
    public const string CurrencySection = "CurrencySection";
    public const string Currency = "Currency";
    public const string RateCurrency = "RateCurrency";
    public const string TemplateSection = "TemplateSection";
    public const string TemplateGroup = "TemplateGroup";
    public const string Template = "Template";
    public const string TemplateEntry = "TemplateEntry";
    public const string AccountSection = "AccountSection";
    public const string AccountGroup = "AccountGroup";
    public const string AccountSubGroup = "AccountSubGroup";
    public const string Account = "Account";
    public const string TransactionSection = "TransactionSection";
    public const string Transaction = "Transaction";
    public const string TransactionEntry = "TransactionEntry";

    public const string Id = "Id";
    public const string Name = "Name";
    public const string Description = "Description";
    public const string TimeStamp = "TimeStamp";
    public const string Order = "Order";
    public const string IsFavorite = "IsFavorite";
    public const string IsoCode = "IsoCode";
    public const string Symbol = "Symbol";
    public const string Rate = "Rate";
    public const string Date = "Date";
    public const string Comment = "Comment";
    public const string ProjectId = "ProjectId";
    public const string CorrespondentId = "CorrespondentId";
    public const string CategoryId = "CategoryId";
    public const string CurrencyId = "CurrencyId";
    public const string AccountId = "AccountId";
    public const string Amount = "Amount";
    public const string DateTime = "DateTime";
    public const string State = "TransactionState";
    public const string TransactionId = "transactionId";

    public const string DateTimeFormat = "yyyyMMddHHmmss";
    public const string TimeStampFormat = "yyyyMMddHHmmssfff";
    public const string DateFormat = "yyyyMMdd";
    public const string GuidFormat = "n";
    public const string DecimalFormat = "N4";

    #endregion

    #region ProjectSection

    public static XElement CreateProjectGroups(XElement root)
    {
        var projectSection = new XElement(ProjectSection);
        root.Add(projectSection);

        projectSection.Add(CreateElement(ProjectGroup, "14230dba01534710a3222e33ae364820", "House", null,
            new DateTime(2023, 01, 01, 10, 00, 00, 111), 1, true));
        projectSection.Add(CreateElement(ProjectGroup, "42cac30c3f1c41e0b5048427c62f38ba", "Study", null,
            new DateTime(2023, 01, 02, 10, 00, 00, 111), 3, true));
        projectSection.Add(CreateElement(ProjectGroup, "21b3265daa7d4b19a39f2f7fce2a8a71", "Health", null,
            new DateTime(2023, 01, 02, 13, 10, 00, 111), 2, true));
        projectSection.Add(CreateElement(ProjectGroup, "06ba51eba4b34bc19a9e8cde15abcbb3", "Festival", null,
            new DateTime(2023, 02, 12, 14, 05, 00, 000), 4, true));
        projectSection.Add(CreateElement(ProjectGroup, "8e1544cb300b40dda405ac2365877d58", "Other", null,
            new DateTime(2023, 01, 01, 11, 05, 15, 111), 5, true));

        return projectSection;
    }

    public static void CreateProjects(XElement projectSection)
    {
        var projectGroups = projectSection.Elements().ToList();

        projectGroups[0].Add(CreateElement(Project, "a82f5b43f98c44baad7e8743b7303474"
            , "Apartment", "Rent, repair, equipmants", new DateTime(2023, 01, 02, 12, 00, 30, 111), 1));
        projectGroups[0].Add(CreateElement(Project, "0479d8b25e684d01993d76dbee81a2a5"
            , "HouseCredit", "montly payment by credit", new DateTime(2023, 01, 01, 12, 00, 30, 000), 2, true));
        projectGroups[1].Add(CreateElement(Project, "ae4960589dfc4188ad7374a222cf198b"
            , "Univercity", "spend on education", new DateTime(2023, 01, 02, 12, 15, 30, 111), 1, true));
        projectGroups[1].Add(CreateElement(Project, "2489ab974f5e4a69ab51150360747515"
            , "ITcourses", "spend on education", new DateTime(2023, 01, 02, 12, 15, 30, 111), 2, true));
        projectGroups[2].Add(CreateElement(Project, "2d61cff820b94dc9adb3583d84c2e18a"
            , "Pharmacy", null, new DateTime(2023, 01, 01, 12, 15, 30, 111), 1, true));
        projectGroups[2].Add(CreateElement(Project, "a7675628e6b845e9956fa5c5202ef0ad"
            , "Doctor", null, new DateTime(2023, 01, 01, 12, 15, 30, 111), 2, true));
        projectGroups[2].Add(CreateElement(Project, "2fa381f0fe2543978c3715392b39b2a1"
            , "Hospital", null, new DateTime(2023, 01, 01, 12, 18, 30, 211), 3, true));
        projectGroups[3].Add(CreateElement(Project, "8bc10d426f7343e782408d3ffec333e8"
            , "Vacation", null, new DateTime(2023, 01, 01, 12, 30, 00, 111), 1));
        projectGroups[3].Add(CreateElement(Project, "c12b2fa0e99b4d69b6825dbe4076fa21"
            , "Party", "decor, presents", new DateTime(2023, 01, 01, 12, 15, 30, 111), 2));
    }

    #endregion

    #region CorrespondentSection

    public static XElement CreateCorrespondentGroups(XElement root)
    {
        var correspondentSection = new XElement(CorrespondentSection);
        root.Add(correspondentSection);

        correspondentSection.Add(CreateElement(CorrespondentGroup, "09f19ed516594e68b47ef6d4b863bb21", "We",
            "live together", new DateTime(2023, 01, 02, 12, 00, 30, 111), 1, true));
        correspondentSection.Add(CreateElement(CorrespondentGroup, "d4ff7639a21646d58a2e63760f2b11eb", "Family", null,
            new DateTime(2023, 01, 02, 12, 00, 30, 111), 2, true));
        correspondentSection.Add(CreateElement(CorrespondentGroup, "25b7ef15c59c40949dac4877d076140b", "Relatives",
            null, new DateTime(2023, 01, 02, 12, 00, 30, 111), 6, true));
        correspondentSection.Add(CreateElement(CorrespondentGroup, "70158f7f0e154a418370fbf8c4981647", "Friends", null,
            new DateTime(2023, 01, 02, 12, 00, 30, 111), 5, true));
        correspondentSection.Add(CreateElement(CorrespondentGroup, "e794d74cb1de459b8bef1fa5b7cf88b8", "Works", null,
            new DateTime(2023, 01, 02, 12, 00, 30, 111), 7, true));
        correspondentSection.Add(CreateElement(CorrespondentGroup, "074e720058834bcb8451060bfdd8a736", "Bank",
            "all animals", new DateTime(2023, 01, 02, 12, 00, 30, 111), 4, true));
        correspondentSection.Add(CreateElement(CorrespondentGroup, "42db2caa796e42b99011a9af8a98e724", "Common", null,
            new DateTime(2023, 01, 02, 12, 00, 30, 111), 3, true));

        return correspondentSection;
    }

    public static void GreateCorrespondents(XElement correspondentSection)
    {
        var correspondentGroups = correspondentSection.Elements().ToList();
        correspondentGroups[0].Add(CreateElement(Correspondent, "e800358a6d3a42f784d60d0ff1f32b6f", "Mom", null,
            new DateTime(2022, 10, 01, 12, 00, 30, 000), 1));
        correspondentGroups[0].Add(CreateElement(Correspondent, "8feb894330c04c7182c3f93fe1543cfa", "Dad", null,
            new DateTime(2022, 10, 01, 12, 00, 30, 000), 2));
        correspondentGroups[1].Add(CreateElement(Correspondent, "7c7cb932ee094048a960526fc9bd02cc", "Сhild", null,
            new DateTime(2022, 10, 01, 12, 30, 30, 000), 1));
        correspondentGroups[1].Add(CreateElement(Correspondent, "c59fac239b2442788a0b41f19de538a9", "grmLuba", null,
            new DateTime(2022, 10, 01, 10, 30, 30, 000), 2));
        correspondentGroups[1].Add(CreateElement(Correspondent, "8104a11d1ce24959a4dd54a2a8884032", "grmKrava", null,
            new DateTime(2023, 01, 01, 10, 00, 30, 000), 3));
        correspondentGroups[2].Add(CreateElement(Correspondent, "11841ed05237473694bab3303c0a4d85", "Vasya", null,
            new DateTime(2023, 02, 01, 10, 00, 30, 000), 1));
        correspondentGroups[2].Add(CreateElement(Correspondent, "1cc4bc8c59ae4933a89d0de0dfb19404", "Dasha", null,
            new DateTime(2023, 01, 04, 10, 00, 30, 111), 2));
        correspondentGroups[2].Add(CreateElement(Correspondent, "cde00b99f1a64061ae6f13da767a60fe", "Sasha", null,
            new DateTime(2023, 01, 01, 10, 00, 30, 000), 3));
        correspondentGroups[3].Add(CreateElement(Correspondent, "db88bdcbc3504e89b5de5a861e8ae39d", "Oleg", null,
            new DateTime(2023, 01, 04, 10, 00, 30, 000), 1));
        correspondentGroups[3].Add(CreateElement(Correspondent, "7c71bdb3ec39416f8743ddf4a360ab0d", "Nicolya", null,
            new DateTime(2023, 01, 05, 10, 00, 30, 000), 2));
        correspondentGroups[4].Add(CreateElement(Correspondent, "3b0c6076db334d63b24b04afbae93352", "MyCompany", null,
            new DateTime(2023, 01, 05, 10, 00, 30, 000), 1));
        correspondentGroups[4].Add(CreateElement(Correspondent, "636d9e8159514fe8bf523db26e90e784", "Freelince", null,
            new DateTime(2023, 01, 06, 10, 00, 30, 111), 2));
        correspondentGroups[6].Add(CreateElement(Correspondent, "41393bd1dd67473593943f2424973575", "Aval", null,
            new DateTime(2022, 12, 31, 10, 00, 30, 000), 1));
        correspondentGroups[6].Add(CreateElement(Correspondent, "1fbb532c66344b11a3de90562016aff1", "Private", null,
            new DateTime(2022, 12, 31, 10, 00, 30, 000), 2));
        correspondentGroups[6].Add(CreateElement(Correspondent, "797c4236b07d4c72895d0b2a4ff61ca6", "Animals", null,
            new DateTime(2022, 12, 31, 10, 00, 30, 000), 3));
    }

    #endregion

    #region CategorySection

    public static XElement CreateCategoryGroups(XElement root)
    {
        var categorySection = new XElement(CategorySection);
        root.Add(categorySection);

        categorySection.Add(CreateElement(CategoryGroup, "27fa9b1dba7647ddbcad885c1bcd9c8e", "Goods", "life support",
            new DateTime(2023, 01, 01, 12, 20, 20, 029), 1));
        categorySection.Add(CreateElement(CategoryGroup, "bbfdb6acf6d14a51aea032acef567e57", "Services",
            "price more than 15$", new DateTime(2023, 01, 01, 12, 20, 20, 008), 2));
        categorySection.Add(CreateElement(CategoryGroup, "67b80f5264964e93b5ae35f26b6c2a3f", "Others", "consumables",
            new DateTime(2023, 01, 01, 12, 20, 20, 017), 3, true));

        return categorySection;
    }

    public static void CreateCategories(XElement categorySection)
    {
        var categoryGroups = categorySection.Elements().ToList();

        categoryGroups[0].Add(CreateElement(Category, "7a3ec88c30844cc1b5194a491996de32", "Food", null,
            new DateTime(2022, 01, 01, 12, 10, 11, 211), 1, true));
        categoryGroups[0].Add(CreateElement(Category, "3d4ade51fb65459a8850dc5a0c1d4ba0", "Clothes", null,
            new DateTime(2022, 01, 01, 12, 10, 12, 311), 2));
        categoryGroups[0].Add(CreateElement(Category, "77d742972f1148c8a0d7b6cdaffdd315", "Things", null,
            new DateTime(2022, 01, 01, 12, 10, 13, 141), 3, true));
        categoryGroups[0].Add(CreateElement(Category, "ad3555fc93e94843a1fb4df9855fb8fa", "BigThings", null,
            new DateTime(2022, 01, 01, 12, 10, 14, 189), 4));

        categoryGroups[1].Add(CreateElement(Category, "3ae347954f4c4d22a4f7ed31db78ab7a", "Rent", null,
            new DateTime(2022, 01, 01, 12, 10, 18, 101), 1, true));
        categoryGroups[1].Add(CreateElement(Category, "57c9dd71b84a435bbf8f6a28c4cb65f1", "Health", null,
            new DateTime(2022, 01, 01, 12, 10, 18, 110), 2, true));
        categoryGroups[1].Add(CreateElement(Category, "7417a52c573f49c2b3caec0014807f1b", "Fun", null,
            new DateTime(2022, 01, 01, 12, 10, 18, 112), 3));
        categoryGroups[1].Add(CreateElement(Category, "20a32c006bb14bc3bc2869181293b9db", "Study", null,
            new DateTime(2022, 01, 01, 12, 10, 18, 121), 4, true));
        categoryGroups[1].Add(CreateElement(Category, "6721b3afc1974d8597ec1f332a8e35f5", "Care", null,
            new DateTime(2022, 01, 01, 12, 10, 18, 113), 5, true));
        categoryGroups[1].Add(CreateElement(Category, "b9d32d0060ec4d59adafd35266934396", "Transport", null,
            new DateTime(2022, 01, 01, 12, 10, 18, 131), 6));
        categoryGroups[1].Add(CreateElement(Category, "4ab2083c122840cbbfbef56c3fbc47d4", "Common", null,
            new DateTime(2022, 01, 01, 12, 10, 18, 114), 7));

        categoryGroups[2].Add(CreateElement(Category, "39ed82962389496bb29c1f9b50fd301c", "Charity", null,
            new DateTime(2022, 01, 01, 10, 10, 10, 128), 1));
        categoryGroups[2].Add(CreateElement(Category, "95bd785b6de448dbbb17fd610a033956", "Present", null,
            new DateTime(2022, 01, 01, 10, 15, 10, 135), 2));
        categoryGroups[2].Add(CreateElement(Category, "6ca3dd9a799545a8bcf4c39a9bcb8c66", "WriteOff", null,
            new DateTime(2022, 01, 01, 10, 25, 10, 451), 3, true));
        categoryGroups[2].Add(CreateElement(Category, "75b11429488448328716fce65d6a6424", "Other", null,
            new DateTime(2022, 01, 01, 10, 35, 10, 161), 4));
    }

    #endregion

    #region CurrencySection

    public static XElement CreateCurrencySection(XElement root)
    {
        var currencySection = new XElement(CurrencySection);
        root.Add(currencySection);
        currencySection.Add(CreateCurrency(Currency, "55bcacf26317410eace7e72de34a1c6c", "UAH", "₴",
            new DateTime(2023, 01, 01, 12, 00, 00, 120), 1, true));
        currencySection.Add(CreateCurrency(Currency, "c5b2fc83fc744d6b92a1e5c7bb59681b", "USD", "$",
            new DateTime(2023, 01, 01, 12, 01, 00, 030), 2, true));
        currencySection.Add(CreateCurrency(Currency, "ef66e2b94c3549fab64d658f28119b08", "EUR", "€",
            new DateTime(2023, 01, 01, 12, 02, 00, 002), 3, true));
        currencySection.Add(CreateCurrency(Currency, "2bf8bd6e3c554964b18f6b393762314c", "GBP", "£",
            new DateTime(2023, 01, 01, 12, 03, 00, 004), 4));
        return currencySection;
    }

    public static XElement CreateCurrency(string elementName, string id, string isoCode, string symbol,
        DateTime timeStamp, int order, bool isFavorite = false)
    {
        var element = new XElement(elementName);
        element.Add(new XAttribute(Id, id));
        element.Add(new XAttribute(IsoCode, isoCode));
        element.Add(new XAttribute(Symbol, symbol));
        element.Add(new XAttribute(TimeStamp, timeStamp.ToString(TimeStampFormat)));
        element.Add(new XAttribute(Order, order));
        element.Add(new XAttribute(IsFavorite, isFavorite));
        return element;
    }

    public static void CreateRates(XElement carencySection)
    {
        var currencies = carencySection.Elements().ToList();
        currencies[1].Add(CreateRateCurrency(RateCurrency, "2367085325a5490b8d47763ff84b0c73", 36.8200m, "USD from UAH",
            new DateTime(2023, 01, 01, 12, 02, 00, 008), new DateTime(2023, 02, 01)));
        currencies[1].Add(CreateRateCurrency(RateCurrency, "8cd92039cd354e3180c60ca40cf802bb", 36.8200m, "USD from UAH",
            new DateTime(2023, 02, 01, 12, 02, 00, 025), new DateTime(2023, 02, 01)));
        currencies[2].Add(CreateRateCurrency(RateCurrency, "69e28e8874644606afbb2dfcd2ffbb26", 38.5100m, "EUR from UAH",
            new DateTime(2023, 01, 01, 12, 02, 00, 021), new DateTime(2023, 02, 01)));
        currencies[2].Add(CreateRateCurrency(RateCurrency, "71607fb19ddf45148f6f6a48192be9a9", 38.3900m, "EUR from UAH",
            new DateTime(2023, 02, 01, 12, 02, 00, 015), new DateTime(2023, 02, 01)));
    }

    public static XElement CreateRateCurrency(string elementName, string id, decimal rate, string comment,
        DateTime timeStamp, DateTime date)
    {
        var element = new XElement(elementName);
        element.Add(new XAttribute(Id, id));
        element.Add(new XAttribute(Rate, rate.ToString(DecimalFormat, CultureInfo.InvariantCulture)));
        if (!string.IsNullOrEmpty(comment)) element.Add(new XAttribute(Comment, comment));

        element.Add(new XAttribute(TimeStamp, timeStamp.ToString(TimeStampFormat)));
        element.Add(new XAttribute(Date, date.ToString(DateFormat)));
        return element;
    }

    #endregion

    #region AccountSection

    public static XElement CreateAccountGroups(XElement root)
    {
        var accoutSection = new XElement(AccountSection);
        root.Add(accoutSection);

        accoutSection.Add(CreateElement(AccountGroup, "64e26c6cabd147e0b2d7f0b33daf1dbd", "Cash/Card",
            "operational data", new DateTime(2022, 12, 20, 12, 10, 11, 258), 1));
        accoutSection.Add(CreateElement(AccountGroup, "3f48281a9f2d447087a4c3e6c644cfe8", "Asset", "longterm money",
            new DateTime(2022, 12, 20, 12, 15, 11, 250), 2));
        accoutSection.Add(CreateElement(AccountGroup, "af0ca9388e534705b9b8c39b61353caa", "Debts",
            "money owed to/from others", new DateTime(2022, 12, 20, 12, 20, 21, 158), 3));
        accoutSection.Add(CreateElement(AccountGroup, "879885602e4c4f2b895a3aae1fa319e4", "Income/Outcome", null,
            new DateTime(2022, 12, 20, 12, 30, 21, 158), 4));
        return accoutSection;
    }

    public static void CreateAccountSubGroups(XElement accoutSection)
    {
        var accountGroups = accoutSection.Elements().ToList();

        accountGroups[0].Add(CreateElement(AccountSubGroup, "bad603ba89c44548aa6319e66b7c4c55", "Life", null,
            new DateTime(2022, 12, 20, 12, 10, 15, 258), 1, true)); //0
        accountGroups[0].Add(CreateElement(AccountSubGroup, "ef6cf3d40e7a4f2e8995458b6006685d", "Health", null,
            new DateTime(2022, 12, 20, 12, 11, 15, 258), 2, true)); //1
        accountGroups[0].Add(CreateElement(AccountSubGroup, "04b508e706ba4ea7b282d54504d656cd", "Study", null,
            new DateTime(2022, 12, 20, 12, 12, 15, 258), 3, true)); //2
        accountGroups[0].Add(CreateElement(AccountSubGroup, "074dac29c58a4cd7bd90829434a88033", "Service", null,
            new DateTime(2022, 12, 20, 12, 13, 15, 258), 4, true)); //3
        accountGroups[0].Add(CreateElement(AccountSubGroup, "34b23c5093994073956be3fe6ae914c1", "Other", null,
            new DateTime(2022, 12, 20, 12, 14, 15, 258), 5, true)); //4

        accountGroups[1].Add(CreateElement(AccountSubGroup, "8aae08e5e9a64e7fb56b22ab0b95f5ac", "USD", null,
            new DateTime(2022, 12, 20, 12, 20, 15, 258), 1)); //5
        accountGroups[1].Add(CreateElement(AccountSubGroup, "14e734be80ee4adfb0a879713e0b271e", "EUR", null,
            new DateTime(2022, 12, 20, 12, 21, 15, 258), 2)); //6
        accountGroups[1].Add(CreateElement(AccountSubGroup, "ff86e0b48a76445f883bd0b53b22414b", "Other", null,
            new DateTime(2022, 12, 20, 12, 22, 15, 258), 3)); //7

        accountGroups[2].Add(CreateElement(AccountSubGroup, "ab43d6962e0b468f9bdb826cef87f650", "Person", null,
            new DateTime(2022, 12, 20, 12, 33, 15, 258), 1)); //8
        accountGroups[2].Add(CreateElement(AccountSubGroup, "4ea9ed64daf84c35859733b695e1bd82", "Legal", null,
            new DateTime(2022, 12, 20, 12, 34, 15, 258), 2)); //9

        accountGroups[3].Add(CreateElement(AccountSubGroup, "f01173bf071048d6880698cd8a67138c", "Cash", null,
            new DateTime(2022, 12, 20, 12, 33, 15, 258), 1, true)); //10
        accountGroups[3].Add(CreateElement(AccountSubGroup, "2e35a804cc8c4b458f86f880a469dc17", "Card", null,
            new DateTime(2022, 12, 20, 12, 34, 15, 258), 2, true)); //11
        accountGroups[3].Add(CreateElement(AccountSubGroup, "db52f37e82df486691937c399302d4bd", "Work", null,
            new DateTime(2022, 12, 20, 12, 35, 25, 250), 3)); //12
    }

    public static void CreateAccounts(XElement accountSection)
    {
        var accountSubGroups = accountSection.Elements().SelectMany(ag => ag.Elements()).ToList();

        accountSubGroups[0].Add(CreateAccount(Account, "2090834e2bd741b7a310c14f8623f821", "Food", null,
            new DateTime(2022, 12, 30, 12, 35, 25, 250), 1
            , UahCurrency
            , "7a3ec88c30844cc1b5194a491996de32"
            , null
            , null, true));
        accountSubGroups[0].Add(CreateAccount(Account, "9d2f433b1aa14ddd9bac11d49348fde2", "Things", null,
            new DateTime(2022, 11, 30, 12, 35, 15, 250), 2
            , UahCurrency
            , "77d742972f1148c8a0d7b6cdaffdd315"
            , null
            , null, true));
        accountSubGroups[1].Add(CreateAccount(Account, "a09054e7554d44519f7b5b672893dd9c", "Doctor", null,
            new DateTime(2022, 12, 29, 10, 05, 25, 260), 1
            , UahCurrency
            , "57c9dd71b84a435bbf8f6a28c4cb65f1"
            , "c59fac239b2442788a0b41f19de538a9"
            , null, true));
        accountSubGroups[2].Add(CreateAccount(Account, "3ca754b1052b4046af06e1d084a78386", "Swithing", null,
            new DateTime(2023, 01, 20, 10, 05, 20, 160), 1
            , UahCurrency
            , "20a32c006bb14bc3bc2869181293b9db"
            , Mom
            , "2489ab974f5e4a69ab51150360747515"));
        accountSubGroups[2].Add(CreateAccount(Account, "ebc2696f1dfa4350a42b46d4dadf6666", "UniverSon", null,
            new DateTime(2022, 10, 15, 10, 05, 20, 000), 2
            , EurCurrency
            , "20a32c006bb14bc3bc2869181293b9db"
            , "7c7cb932ee094048a960526fc9bd02cc"
            , "ae4960589dfc4188ad7374a222cf198b"));
        accountSubGroups[5].Add(CreateAccount(Account, "aab9c90661a44cf792fa8ab8b478843b", "CellBank", null,
            new DateTime(2022, 10, 15, 10, 05, 20, 000), 1
            , UsdCurrency
            , null
            , Dad
            , null, true));
        accountSubGroups[6].Add(CreateAccount(Account, "7069d84091344c77b92ab9b9224829d8", "CellBank", null,
            new DateTime(2022, 10, 15, 10, 15, 20, 111), 1
            , EurCurrency
            , null
            , Dad
            , null, true));
        accountSubGroups[8].Add(CreateAccount(Account, "1a6166e407ec4cd083758cf56079038a", "Loan", null,
            new DateTime(2022, 11, 15, 10, 15, 21, 121), 1
            , UsdCurrency
            , null
            , "1cc4bc8c59ae4933a89d0de0dfb19404"
            , null));
        accountSubGroups[9].Add(CreateAccount(Account, "1cf5723d00ac45ca943273f7c9851307", "CeditWork", null,
            new DateTime(2022, 11, 15, 10, 15, 21, 121), 1
            , UahCurrency
            , null
            , "3b0c6076db334d63b24b04afbae93352"
            , null));
        accountSubGroups[9].Add(CreateAccount(Account, "fbaccc8f08654942993ff671ef9789a0", "Tax", null,
            new DateTime(2022, 11, 15, 10, 15, 21, 321), 2
            , UahCurrency
            , null
            , null
            , null));
        accountSubGroups[10].Add(CreateAccount(Account, "484d8b3e9e22459f817f25e38c74b0b4", "Dad", "CashDad",
            new DateTime(2023, 01, 05, 12, 11, 21, 121), 1
            , UahCurrency
            , null
            , Dad
            , null));
        accountSubGroups[10].Add(CreateAccount(Account, "9297ff933a8d40f9b0c870cccf816fcb", "Mom", "CashMom",
            new DateTime(2023, 01, 05, 12, 11, 21, 121), 2
            , UahCurrency
            , null
            , Mom
            , null));
        accountSubGroups[10].Add(CreateAccount(Account, "a03b1414578544dc8418a87cb58cd7d6", "Dad", "CardDad",
            new DateTime(2023, 01, 05, 12, 11, 21, 121), 3
            , UahCurrency
            , null
            , Dad
            , null));
        accountSubGroups[10].Add(CreateAccount(Account, "e2986e12986f470a95b94fc9ca72abcc", "Mom", "CardMom",
            new DateTime(2023, 01, 05, 12, 11, 21, 121), 4
            , UahCurrency
            , null
            , Mom
            , null));
        accountSubGroups[10].Add(CreateAccount(Account, "37fd6d8966f74c89b05565cdbcb38433", "USD", "CashUSD",
            new DateTime(2023, 01, 07, 12, 11, 21, 121), 5
            , UsdCurrency
            , null
            , null
            , null));
        accountSubGroups[10].Add(CreateAccount(Account, "40a5f7fa809a4dd49d8b21e0e6f25a6d", "EUR", "CashEUR",
            new DateTime(2023, 01, 08, 12, 11, 21, 121), 6
            , EurCurrency
            , null
            , null
            , null));
        accountSubGroups[11].Add(CreateAccount(Account, "31dad22bd96641da8f639e96e15f740c", "Charity", null,
            new DateTime(2023, 01, 05, 12, 11, 21, 121), 1
            , UahCurrency
            , "39ed82962389496bb29c1f9b50fd301c"
            , null
            , null));
        accountSubGroups[12].Add(CreateAccount(Account, "60ced6f6647a4823b9836aa4d63b5e15", "SalaryDad", null,
            new DateTime(2022, 12, 05, 11, 11, 21, 121), 1
            , UahCurrency
            , null
            , Dad
            , null));
        accountSubGroups[12].Add(CreateAccount(Account, "d5f87136715a4bd6a44d2a1cc536fce0", "SalaryMom", null,
            new DateTime(2022, 12, 05, 12, 11, 21, 121), 2
            , UahCurrency
            , null
            , Mom
            , null));
    }


    public static XElement CreateAccount(string elementName, string id, string name, string description,
        DateTime timeStamp, int order
        , string currencyId
        , string categoryId
        , string correspondentId
        , string projectId, bool isFavorite = false)
    {
        var account = new XElement(elementName);
        account.Add(new XAttribute(Id, id));
        account.Add(new XAttribute(Name, name));
        if (!string.IsNullOrEmpty(description)) account.Add(new XAttribute(Description, description));
        account.Add(new XAttribute(TimeStamp, timeStamp.ToString(TimeStampFormat)));
        account.Add(new XAttribute(Order, order));
        account.Add(new XAttribute(IsFavorite, isFavorite));
        account.Add(new XAttribute(CurrencyId, currencyId));

        if (!string.IsNullOrEmpty(correspondentId)) account.Add(new XAttribute(CorrespondentId, correspondentId));
        if (!string.IsNullOrEmpty(categoryId)) account.Add(new XAttribute(CategoryId, categoryId));
        if (!string.IsNullOrEmpty(projectId)) account.Add(new XAttribute(ProjectId, projectId));
        return account;
    }

    #endregion

    #region TemplateSection

    public static XElement CreateTemplateGroups(XElement root)
    {
        var teplateSection = new XElement(TemplateSection);
        root.Add(teplateSection);
        teplateSection.Add(CreateElement(TemplateGroup, "ffdddaf66fdc4372abf98ae61dd1bb42", "Dayly", null,
            new DateTime(2022, 12, 02, 12, 00, 30, 111), 1, true));
        teplateSection.Add(CreateElement(TemplateGroup, "177bdb957aa741a69883148eaa553ba6", "Weekly", null,
            new DateTime(2022, 12, 02, 12, 10, 30, 122), 2, true));
        teplateSection.Add(CreateElement(TemplateGroup, "99036f361fdb4dedbfc61729c89fd7fa", "Quarterly", null,
            new DateTime(2022, 12, 02, 12, 20, 30, 231), 3, true));
        teplateSection.Add(CreateElement(TemplateGroup, "527d8dd3aa5f40deae90ec4ec3b92f3c", "Yearly", null,
            new DateTime(2022, 12, 02, 12, 30, 30, 114), 4, true));
        teplateSection.Add(CreateElement(TemplateGroup, "527d8dd3aa5f40deae90ec4ec3b92f3c", "Monthly", null,
            new DateTime(2022, 12, 02, 12, 30, 30, 114), 4, true));
        return teplateSection;
    }

    public static void CreateTeplates(XElement templateSection)
    {
        var templates = templateSection.Elements().ToList();
        templates[0].Add(CreateElement(Template, "090324940b0842e1aebb32da8548e275", "Ones", "goods by way",
            new DateTime(2022, 12, 02, 10, 30, 30, 104), 1, true));
        templates[1].Add(CreateElement(Template, "d16b256c6d72402b93fc472262343b2f", "MarketMom", "goods on market",
            new DateTime(2022, 12, 02, 12, 30, 30, 114), 1, true));
        templates[1].Add(CreateElement(Template, "7e36525c27d54d888c049fddff0bcb00", "MarketDad", "goods on market",
            new DateTime(2022, 12, 02, 12, 30, 30, 114), 2, true));
        templates[1].Add(CreateElement(Template, "6869c1adfcf04a5c99923fb071c39588", "ShopCard", "goods in supermarket",
            new DateTime(2022, 12, 02, 11, 35, 38, 184), 3, true));
        templates[1].Add(CreateElement(Template, "c358e5cc206f4df281c59acc4039b79e", "ShopCash", "goods in supermarket",
            new DateTime(2022, 12, 02, 11, 35, 38, 222), 4, true));
        templates[2].Add(CreateElement(Template, "b206aac4414c4fcca051f31f65b41710", "Tax", null,
            new DateTime(2022, 12, 03, 13, 35, 38, 184), 1));
        templates[3].Add(CreateElement(Template, "04c3535bfc0d41129bab912dadad7c56", "BuyUSD", null,
            new DateTime(2023, 01, 07, 09, 05, 30, 128), 1));
        templates[3].Add(CreateElement(Template, "b4606aa684d447f38c29d71ad9d5091a", "BuyEUR", null,
            new DateTime(2023, 01, 07, 09, 05, 30, 128), 1));
    }

    public static void CreateTemplateEntries(XElement templateSection)
    {
        var teplates = templateSection.Elements().SelectMany(t => t.Elements()).ToList();
        teplates[1].Add(CreateTeplateEntry("01f5a77624694f0e8343bd70d5ef9c73",
            "484d8b3e9e22459f817f25e38c74b0b4")); // DAD by cash UAH
        teplates[1].Add(CreateTeplateEntry("a018aa5a01884f1ab1cb9ddd36806d58",
            "2090834e2bd741b7a310c14f8623f821")); // food for family

        teplates[2].Add(CreateTeplateEntry("a610f420872c4f289b27ebb34065c340",
            "9297ff933a8d40f9b0c870cccf816fcb")); // MOM by cash UAH
        teplates[2].Add(CreateTeplateEntry("ad0e9ba5c2c5404f9efa041d63cb29ac",
            "2090834e2bd741b7a310c14f8623f821")); // food for family

        teplates[3].Add(CreateTeplateEntry("7ae19a01bb5e48f5a4c2dce2ea0fdbe6",
            "e2986e12986f470a95b94fc9ca72abcc")); // mom by CARD UAH
        teplates[3].Add(CreateTeplateEntry("0ea9c7dc3c52410583027da6abb7bd55",
            "2090834e2bd741b7a310c14f8623f821")); // food for family
        teplates[3].Add(CreateTeplateEntry("b844afe93efb46ce8b2b064098f752a2",
            "9d2f433b1aa14ddd9bac11d49348fde2")); // things for family
        teplates[3].Add(CreateTeplateEntry("185cf60cee4d40c982ceb66861645801",
            "31dad22bd96641da8f639e96e15f740c")); // charity for animals

        teplates[4].Add(CreateTeplateEntry("68bdb97aeda44134b6b3c1329367ae04",
            "a03b1414578544dc8418a87cb58cd7d6")); // dad by card UAH
        teplates[4].Add(CreateTeplateEntry("05b4334449a44701b2f0de5a7bb6b0af",
            "2090834e2bd741b7a310c14f8623f821")); // food for family
        teplates[4].Add(CreateTeplateEntry("b0b419acb2e944a6868e491a94d97020",
            "9d2f433b1aa14ddd9bac11d49348fde2")); // things for family
        teplates[4].Add(CreateTeplateEntry("8c3d6a6b9c114c61825a7cc9f0f6b74a",
            "31dad22bd96641da8f639e96e15f740c")); // charity for animals

        teplates[5].Add(CreateTeplateEntry("9d5c243500784440a5ac3591a3d9f02f", "a03b1414578544dc8418a87cb58cd7d6",
            -15000.0000m)); // DAD BY CARD UAH
        teplates[5].Add(CreateTeplateEntry("88cb9836993e4f5eaedeb381f249dd09", "fbaccc8f08654942993ff671ef9789a0",
            15000.0000m)); // tax  UAH

        teplates[6].Add(CreateTeplateEntry("a07e24fc216341ed9defa2a2f52fc3ef",
            "484d8b3e9e22459f817f25e38c74b0b4")); //DAD By Cash UAH 
        teplates[6].Add(CreateTeplateEntry("a4e51b9448d541e29903cce4304c6199",
            "37fd6d8966f74c89b05565cdbcb38433")); //income USD

        teplates[7].Add(CreateTeplateEntry("a347cf3aafb14be3be5f8738ea57249c",
            "484d8b3e9e22459f817f25e38c74b0b4")); //DAD By Cash UAH 
        teplates[7].Add(CreateTeplateEntry("222e0185dc204e81b1341f3748963043",
            "40a5f7fa809a4dd49d8b21e0e6f25a6d")); //income EUR
    }

    public static XElement CreateTeplateEntry(string id, string accountId, decimal sum = 0.0000m)
    {
        var element = new XElement(TemplateEntry);
        element.Add(new XAttribute(Id, id));
        element.Add(new XAttribute(AccountId, accountId));
        element.Add(new XAttribute(Amount, sum.ToString(DecimalFormat, CultureInfo.InvariantCulture)));
        return element;
    }

    #endregion

    #region TransactionSection

    public static XElement CreateTransactions(XElement root)
    {
        var transactionSection = new XElement(TransactionSection);
        root.Add(transactionSection);

        transactionSection.Add(CreateTransaction("ee63649ecda345a4856315b941ed0e52"
            , new DateTime(2022, 12, 01, 01, 30, 30, 111)
            , new DateTime(2023, 01, 01, 12, 33, 33)
            , TransactionState.Planned, "who"));
        transactionSection.Add(CreateTransaction("17de78ca30ad4342b8e96019612b3281"
            , new DateTime(2022, 12, 01, 02, 40, 10, 222)
            , new DateTime(2023, 01, 02, 11, 30, 31)
            , TransactionState.Draft, "what"));
        transactionSection.Add(CreateTransaction("9fb308d6eea6458dafec94e438fa67af"
            , new DateTime(2022, 12, 01, 02, 40, 10, 333)
            , new DateTime(2023, 01, 03, 10, 35, 33)
            , TransactionState.Confirmed, "why"));
        transactionSection.Add(CreateTransaction("22df725e4f484fc79487c47bd3406643"
            , new DateTime(2022, 12, 01, 02, 40, 10, 444)
            , new DateTime(2023, 01, 04, 09, 33, 34)
            , TransactionState.Confirmed, "best"));
        transactionSection.Add(CreateTransaction("22df725e4f484fc79487c47bd3406643"
            , new DateTime(2022, 12, 02, 02, 40, 10, 555)
            , new DateTime(2023, 02, 06, 09, 33, 34)
            , TransactionState.Confirmed, null));
        transactionSection.Add(CreateTransaction("105b856fea8744f5bf745ee81dd830a3"
            , new DateTime(2023, 01, 01, 02, 40, 10, 888)
            , new DateTime(2023, 02, 05, 09, 39, 25)
            , TransactionState.Confirmed, "west"));
        transactionSection.Add(CreateTransaction("808ca85d21e444f0af8548e0913cb9a4"
            , new DateTime(2023, 02, 07, 02, 40, 10, 777)
            , new DateTime(2023, 02, 07, 09, 39, 25)
            , TransactionState.NoValid, null));
        return transactionSection;
    }

    public static XElement CreateTransaction(string id, DateTime timeStamp, DateTime dateTime, TransactionState state,
        string comment)
    {
        var transaction = new XElement(Transaction);
        transaction.Add(new XAttribute(Id, id));
        transaction.Add(new XAttribute(TimeStamp, timeStamp.ToString(TimeStampFormat)));
        transaction.Add(new XAttribute(DateTime, dateTime.ToString(DateTimeFormat)));
        transaction.Add(new XAttribute(State, state));
        if (!string.IsNullOrEmpty(comment)) transaction.Add(new XAttribute(Comment, comment));
        return transaction;
    }

    public static void CreateTransactionEntries(XElement transactionSection)
    {
        var transactions = transactionSection.Elements().ToList();
        transactions[0].Add(CreateTransactionEntry("e54c6b9b7953445c961d78a5d72666f4",
            "484d8b3e9e22459f817f25e38c74b0b4", -250.0200m));
        transactions[0].Add(CreateTransactionEntry("2cd3c60c174e4eefa3cc049d7185d317",
            "2090834e2bd741b7a310c14f8623f821", 250.0200m));
        transactions[1].Add(CreateTransactionEntry("1fb529e5687043e5bf201a93e3fe45bc",
            "9297ff933a8d40f9b0c870cccf816fcb", -21.3500m));
        transactions[1].Add(CreateTransactionEntry("99bf90621d10414ea514326fd46dfb92",
            "2090834e2bd741b7a310c14f8623f821", 21.3500m));
        transactions[2].Add(CreateTransactionEntry("1fa79540a37145b685e1c5bfdb331210",
            "e2986e12986f470a95b94fc9ca72abcc", -2158.3600m));
        transactions[2].Add(CreateTransactionEntry("77daa38a6a214abfac079d010c583e26",
            "9d2f433b1aa14ddd9bac11d49348fde2", 58.3600m));
        transactions[2].Add(CreateTransactionEntry("01dd9d92347c4e2c8c9478ec52773c2b",
            "31dad22bd96641da8f639e96e15f740c", 109.5000m));
        transactions[2].Add(CreateTransactionEntry("121414f8203148ac93e654eb51d0f968",
            "2090834e2bd741b7a310c14f8623f821", 1990.5000m));
        transactions[3].Add(CreateTransactionEntry("007c93390e7c433f8795578f4032a196",
            "a03b1414578544dc8418a87cb58cd7d6", -15947.2500m));
        transactions[3].Add(CreateTransactionEntry("0d2029632b0e44ca9c3734688eba6386",
            "fbaccc8f08654942993ff671ef9789a0", 15947.2500m));
        transactions[4].Add(CreateTransactionEntry("2536e03e0c4443858c3f6c7bedcacc00",
            "484d8b3e9e22459f817f25e38c74b0b4", -18410.0000m));
        transactions[4].Add(CreateTransactionEntry("59a5d6c06d3d40b9996ed90ddf0dfc4a",
            "37fd6d8966f74c89b05565cdbcb38433", 500.0000m, 36.8200m));
        transactions[5].Add(CreateTransactionEntry("3e1edc4a9c4344f5ab3c154e448e6d34",
            "484d8b3e9e22459f817f25e38c74b0b4", -19255.0000m));
        transactions[5].Add(CreateTransactionEntry("b0837f476831473391fb8111e458c781",
            "40a5f7fa809a4dd49d8b21e0e6f25a6d", 500.0000m, 38.5100m));
    }

    public static XElement CreateTransactionEntry(string id, string accountId, decimal amount, decimal rate = 1)
    {
        var element = new XElement(TransactionEntry);
        element.Add(new XAttribute(Id, id));
        element.Add(new XAttribute(AccountId, accountId));
        element.Add(new XAttribute(Amount, amount.ToString(DecimalFormat, CultureInfo.InvariantCulture)));
        element.Add(new XAttribute(Rate, rate.ToString(DecimalFormat, CultureInfo.InvariantCulture)));
        return element;
    }

    #endregion
}