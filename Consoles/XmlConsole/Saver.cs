﻿namespace XmlConsole;

public static class Saver
{
    public static void Proceed(Ledger ledger)
    {
        var serializer = new XmlSerializer();
        var xmlDoc = serializer.Serialize(ledger);

        var xmlStorage = new XmlFileStorage(@"c:\\MyXmlFirst.xml");
        xmlStorage.Save(xmlDoc);
    }
}