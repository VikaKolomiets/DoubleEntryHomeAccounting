﻿using Common.Params.Interfaces;

namespace Common.Params;

public class AccountParam : INamedParam, IFavoriteParam, IChildParam
{
    public Guid CurrencyId { get; set; }
    public Guid? CategoryId { get; set; }
    public Guid? CorrespondentId { get; set; }
    public Guid? ProjectId { get; set; }
    public Guid ParentId { get; set; }
    public bool IsFavorite { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
}