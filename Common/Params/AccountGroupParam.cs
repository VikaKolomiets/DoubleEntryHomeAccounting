﻿using Common.Params.Interfaces;

namespace Common.Params;

public class AccountGroupParam : INamedParam, IFavoriteParam
{
    public bool IsFavorite { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
}