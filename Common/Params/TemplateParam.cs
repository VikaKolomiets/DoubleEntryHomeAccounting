﻿using Common.Params.Interfaces;

namespace Common.Params;

public class TemplateParam : INamedParam, IFavoriteParam, IChildParam
{
    public Guid ParentId { get; set; }
    public bool IsFavorite { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public List<TemplateEntryParam> Entries { get; } = new();
}