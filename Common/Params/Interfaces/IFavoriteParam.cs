﻿namespace Common.Params.Interfaces;

public interface IFavoriteParam
{
    bool IsFavorite { get; set; }
}