﻿namespace Common.Params.Interfaces;

public interface INamedParam
{
    string Name { get; set; }
    string Description { get; set; }
}