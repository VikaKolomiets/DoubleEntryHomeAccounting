﻿namespace Common.Params.Interfaces;

public interface IChildParam
{
    Guid ParentId { get; set; }
}