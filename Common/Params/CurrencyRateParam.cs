﻿namespace Common.Params;

public class CurrencyRateParam
{
    public Guid CurrencyId { get; set; }

    public DateTime Date { get; set; }

    public decimal Rate { get; set; }

    public string Comment { get; set; }
}