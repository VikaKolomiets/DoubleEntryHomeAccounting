﻿namespace Common.Params;

public class CurrencyParam
{
    public string Code { get; set; }

    public string Symbol { get; set; }

    public string Name { get; set; }
}