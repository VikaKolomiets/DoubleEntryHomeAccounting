﻿using Common.Models.Interfaces;

namespace Common.Utils.Ordering;

public static class OrderingUtils
{
    public static void Reorder<T>(List<T> entities) where T : IOrderedEntity
    {
        List<T> orderedItems = entities.OrderBy(i => i.Order).ToList();

        for (int i = 0; i < orderedItems.Count; i++)
        {
            orderedItems[i].Order = i + 1;
        }
    }

    public static void SetOrder<T>(List<T> entities, T orderedEntity, int order) where T : IOrderedEntity
    {
        if (orderedEntity.Order > order || orderedEntity.Order == 0)
        {
            foreach (T item in entities)
            {
                if (item.Order >= order)
                    item.Order++;
            }
            orderedEntity.Order = order;
            Reorder(entities);
        }
        else if (orderedEntity.Order < order)
        {
            foreach (T item in entities)
            {
                if (item.Order <= order && item.Order > orderedEntity.Order)
                    item.Order--;
            }
            orderedEntity.Order = order;
            Reorder(entities);
        }
    }
}