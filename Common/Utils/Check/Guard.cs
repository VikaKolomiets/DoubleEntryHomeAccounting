﻿using Common.DataAccess.Base;
using Common.Models.Interfaces;
using Common.Params.Interfaces;

namespace Common.Utils.Check;

public static class Guard
{
    public static void CheckParamForNull<T>(T entity)
    {
        if (entity == null) 
        {
            throw new ArgumentNullException($"{typeof(T).Name} cannot be a null");
        }
    }

    public static void CheckParamNameForNull<T>(T entity) where T : INamedParam
    {
        if (entity.Name == null)
        {
            throw new ArgumentNullException($"In {typeof(T).Name} the Name cannot be a null");
        }
    }

    public static async Task CheckEntityWithSameName<T>(IParentRepository<T> repository, Guid id, string name)
        where T : IEntity, INamedEntity
    {
        List<T> entities = await repository.GetByName(name);
        CheckEntityWithSameName(entities, id, name);
    }

    public static async Task CheckEntityWithSameName<T>(IChildRepository<T> repository, Guid parentId, Guid id, string name)
        where T : IEntity, INamedEntity
    {
        List<T> entities = await repository.GetByName(parentId, name);
        CheckEntityWithSameName(entities, id, name);
    }

    private static void CheckEntityWithSameName<T>(List<T> entities, Guid id, string name)
        where T : IEntity, INamedEntity
    {
        if (entities
                .Where(e => string.Equals(e.Name, name, StringComparison.InvariantCultureIgnoreCase))
                .FirstOrDefault(t => t.Id != id) != null)
        {
            throw new ArgumentException($"In {typeof(T).Name} with the same name has already existed in the collection");
        }
    }

    public static async Task CheckExistedChildrenInTheGroup<T>(IChildRepository<T> repository, Guid parentId)
        where T : IEntity, INamedEntity
    {
        if (await repository.GetCount(parentId) > 0)
        {
            throw new ArgumentException("Parent cannot be deleted. It contains items");
        }
    }
}