﻿using Common.Models;
using Common.Params;
using Common.Services.Base;

namespace Common.Services;

public interface ITemplateService : IReferenceChildEntityService<Template, TemplateParam>
{
}