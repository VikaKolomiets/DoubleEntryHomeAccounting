﻿using Common.Params;
using Common.Services.Base;

namespace Common.Services;

public interface ICurrencyService : IOrderedEntityService, IFavoriteEntityService
{
    Task<Guid> Add(CurrencyParam param, decimal initialRate);
    Task Update(CurrencyParam param);
    Task Delete(string isoCode);
}