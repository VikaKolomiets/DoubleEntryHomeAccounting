﻿using Common.Models.Interfaces;

namespace Common.Services.Base;

public interface IEntityService<T, in TParam> where T : IEntity
{
    Task<Guid> Add(TParam param);
    Task Update(Guid entityId, TParam param);
    Task Delete(Guid entityId);
}