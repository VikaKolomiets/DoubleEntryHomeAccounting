﻿using Common.Models.Interfaces;

namespace Common.Services.Base;

public interface IReferenceChildEntityService<T, in TParam> : IReferenceEntityService<T, TParam>, IChildEntityService
    where T : IEntity
{
}