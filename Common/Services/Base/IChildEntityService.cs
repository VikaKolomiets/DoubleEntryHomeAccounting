﻿namespace Common.Services.Base;

public interface IChildEntityService
{
    Task MoveToAnotherParent(Guid entityId, Guid parentId);
}