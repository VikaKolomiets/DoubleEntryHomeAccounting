﻿namespace Common.Services.Base;

public interface IOrderedEntityService
{
    Task SetOrder(Guid entityId, int order);
}