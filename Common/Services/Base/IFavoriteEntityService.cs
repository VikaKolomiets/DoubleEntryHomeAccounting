﻿namespace Common.Services.Base;

public interface IFavoriteEntityService
{
    Task SetFavoriteStatus(Guid entityId, bool isFavorite);
}