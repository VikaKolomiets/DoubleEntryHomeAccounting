﻿namespace Common.Services.Base;

public interface ICombinedEntityService
{
    Task CombineTwoEntities(Guid primaryId, Guid secondaryId);
}