﻿using Common.Models.Interfaces;

namespace Common.Services.Base;

public interface IReferenceEntityService<T, in TParam> : IEntityService<T, TParam>, IOrderedEntityService, IFavoriteEntityService
    where T : IEntity
{
}