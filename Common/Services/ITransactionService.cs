﻿using Common.Models;
using Common.Params;
using Common.Services.Base;

namespace Common.Services;

public interface ITransactionService : IEntityService<Transaction, TransactionParam>
{
    Task DeleteTransactionList(List<Guid> transactionIds);
}