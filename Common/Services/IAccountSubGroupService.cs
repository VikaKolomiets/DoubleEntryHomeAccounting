﻿using Common.Models;
using Common.Params;
using Common.Services.Base;

namespace Common.Services;

public interface IAccountSubGroupService : IReferenceChildEntityService<AccountSubGroup, AccountSubGroupParam>
{
}