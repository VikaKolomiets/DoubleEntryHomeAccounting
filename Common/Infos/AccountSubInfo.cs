﻿using Common.Infos.Base;
using Common.Infos.Interfaces;

namespace Common.Infos;

public class AccountSubInfo : ReferenceParentInfo, IReferenceChildInfo
{
    public Guid ParentId { get; set; }
}