﻿namespace Common.Infos.Interfaces;

public interface IInfo
{
    public Guid Id { get; set; }
}