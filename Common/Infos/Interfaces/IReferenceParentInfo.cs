﻿namespace Common.Infos.Interfaces;

public interface IReferenceParentInfo : IReferenceInfo, IParentInfo
{
}