﻿namespace Common.Infos.Interfaces;

public interface IReferenceInfo : IInfo, INamedInfo, IOrderedInfo, IFavoriteInfo
{
}