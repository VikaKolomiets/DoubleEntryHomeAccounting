﻿namespace Common.Infos.Interfaces;

public interface IReferenceChildInfo : IReferenceInfo, IChildInfo
{
}