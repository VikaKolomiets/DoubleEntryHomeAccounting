﻿namespace Common.Infos.Interfaces;

public interface IChildInfo
{
    Guid ParentId { get; set; }
}