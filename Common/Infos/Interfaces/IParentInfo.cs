﻿namespace Common.Infos.Interfaces;

public interface IParentInfo
{
    List<Guid> ChildrenIds { get; }
}