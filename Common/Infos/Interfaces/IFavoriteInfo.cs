﻿namespace Common.Infos.Interfaces;

public interface IFavoriteInfo
{
    bool IsFavorite { get; set; }
}