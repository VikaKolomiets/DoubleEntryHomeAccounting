﻿using Common.Infos.Interfaces;

namespace Common.Infos.Base;

public class ReferenceParentInfo : ReferenceInfo, IReferenceParentInfo
{
    public List<Guid> ChildrenIds { get; } = new();
}