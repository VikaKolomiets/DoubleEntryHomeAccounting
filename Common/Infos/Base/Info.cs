﻿using Common.Infos.Interfaces;

namespace Common.Infos.Base;

public class Info : IInfo
{
    public Guid Id { get; set; }
}