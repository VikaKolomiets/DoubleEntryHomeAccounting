﻿using Common.Infos.Interfaces;

namespace Common.Infos.Base;

public class ReferenceChildInfo : ReferenceInfo, IReferenceChildInfo
{
    public Guid ParentId { get; set; }
}