﻿using Common.Infos.Base;
using Common.Models.Enums;

namespace Common.Infos;

public class TransactionInfo : Info
{
    public DateTime DateTime { get; set; }
    public TransactionState State { get; set; }
    public string Comment { get; set; }
    public List<Guid> EntriesIds { get; } = new();
}