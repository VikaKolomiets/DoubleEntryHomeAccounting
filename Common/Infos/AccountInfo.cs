﻿using Common.Infos.Base;

namespace Common.Infos;

public class AccountInfo : ReferenceChildInfo
{
    public Guid CurrencyId { get; set; }
    public Guid? CorrespondentId { get; set; }
    public Guid? CategoryId { get; set; }
    public Guid? ProjectId { get; set; }
}