﻿using Common.Infos.Base;

namespace Common.Infos;

public class TemplateInfo : ReferenceChildInfo
{
    public List<Guid> EntriesIds { get; } = new();
}