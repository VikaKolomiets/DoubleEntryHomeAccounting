﻿using Common.Infos.Base;

namespace Common.Infos;

public class TransactionEntryInfo : Info
{
    public Guid TransactionId { get; set; }
    public Guid AccountId { get; set; }
    public decimal Amount { get; set; }
    public decimal Rate { get; set; }
}