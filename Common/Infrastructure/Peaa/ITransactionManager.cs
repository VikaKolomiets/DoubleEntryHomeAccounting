﻿namespace Common.Infrastructure.Peaa
{
    public interface ITransactionManager
    {
        Task StartTransaction();
        Task CommitTransaction();
        Task RollbackTransaction();
    }
}
