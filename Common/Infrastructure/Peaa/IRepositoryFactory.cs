﻿namespace Common.Infrastructure.Peaa
{
    public interface IRepositoryFactory
    {
        T GetRepository<T>();
    }
}
