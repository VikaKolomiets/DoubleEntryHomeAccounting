﻿namespace Common.Infrastructure.Peaa
{
    public interface IStoreManager
    {
        Task RejectChanges();
        Task SaveChanges();
    }
}
