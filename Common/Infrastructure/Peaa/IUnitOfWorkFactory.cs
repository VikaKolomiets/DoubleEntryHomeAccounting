﻿namespace Common.Infrastructure.Peaa
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create(bool isReadOnly = false);
    }
}
