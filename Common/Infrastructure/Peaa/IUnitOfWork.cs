﻿namespace Common.Infrastructure.Peaa
{
    public interface IUnitOfWork : IRepositoryFactory, ITransactionManager, IStoreManager, IDisposable
    {
    }
}
