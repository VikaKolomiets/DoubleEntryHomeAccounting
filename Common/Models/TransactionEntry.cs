﻿using Common.Models.Base;

namespace Common.Models;

public class TransactionEntry : Entity
{
    public Transaction Transaction { get; set; }
    public Account Account { get; set; }
    public decimal Amount { get; set; }
    public decimal Rate { get; set; }
}