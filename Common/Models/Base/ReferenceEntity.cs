﻿using Common.Models.Interfaces;

namespace Common.Models.Base;

public abstract class ReferenceEntity : Entity, IReferenceEntity
{
    public string TimeStamp { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int Order { get; set; }
    public bool IsFavorite { get; set; }
}