﻿using Common.Models.Interfaces;

namespace Common.Models.Base;

public abstract class Entity : IEntity
{
    protected Entity()
    {
        Id = Guid.NewGuid();
    }

    public Guid Id { get; set; }

    public override string ToString()
    {
        return $"{GetType().Name} {Id:N}";
    }
}