﻿using Common.Models.Interfaces;

namespace Common.Models.Base;

public abstract class ReferenceParentEntity<TChild> : ReferenceEntity, IReferenceParentEntity<TChild>
{
    public List<TChild> Children { get; } = new();
}