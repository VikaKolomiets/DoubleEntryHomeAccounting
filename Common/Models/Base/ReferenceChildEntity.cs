﻿using Common.Models.Interfaces;

namespace Common.Models.Base;

public abstract class ReferenceChildEntity<TParent> : ReferenceEntity, IReferenceChildEntity<TParent>
{
    public TParent Parent { get; set; }
}