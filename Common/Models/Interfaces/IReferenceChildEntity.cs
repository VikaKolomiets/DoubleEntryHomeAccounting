﻿namespace Common.Models.Interfaces;

public interface IReferenceChildEntity<TParent> : IReferenceEntity, IChildEntity<TParent>
{
}