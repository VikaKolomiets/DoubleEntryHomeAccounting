﻿namespace Common.Models.Interfaces;

public interface ITrackedEntity
{
    string TimeStamp { get; set; }
}