﻿namespace Common.Models.Interfaces;

public interface IReferenceParentEntity<TChild> : IReferenceEntity, IParentEntity<TChild>
{
}