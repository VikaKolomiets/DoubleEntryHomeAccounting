﻿namespace Common.Models.Interfaces;

public interface IChildEntity<TParent>
{
    TParent Parent { get; set; }
}