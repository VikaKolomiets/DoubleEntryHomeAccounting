﻿namespace Common.Models.Interfaces;

public interface IFavoriteEntity
{
    bool IsFavorite { get; set; }
}