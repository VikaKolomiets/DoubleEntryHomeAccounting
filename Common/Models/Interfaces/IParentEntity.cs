﻿namespace Common.Models.Interfaces;

public interface IParentEntity<TChild>
{
    List<TChild> Children { get; }
}