﻿using Common.Models.Base;

namespace Common.Models;

public class TemplateEntry : Entity
{
    public Template Template { get; set; }
    public Account Account { get; set; }
    public decimal Amount { get; set; }
}