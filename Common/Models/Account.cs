﻿using Common.Models.Base;

namespace Common.Models;

public class Account : ReferenceChildEntity<AccountSubGroup>
{
    public Currency Currency { get; set; }
    public Category Category { get; set; }
    public Correspondent Correspondent { get; set; }
    public Project Project { get; set; }
}