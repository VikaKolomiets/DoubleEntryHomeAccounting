﻿using Common.Models.Base;
using Common.Models.Interfaces;

namespace Common.Models;

public class AccountSubGroup : ReferenceParentEntity<Account>, IReferenceChildEntity<AccountGroup>
{
    public AccountGroup Parent { get; set; }
}