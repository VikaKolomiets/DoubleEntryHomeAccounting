﻿namespace Common.Models.Config;

public class SystemConfig
{
    public string MainCurrencyIsoCode { get; set; }
    public DateTime MinDate { get; set; }
    public DateTime MaxDate { get; set; }
}