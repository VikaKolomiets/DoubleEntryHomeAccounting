﻿using Common.Models.Base;
using Common.Models.Enums;
using Common.Models.Interfaces;

namespace Common.Models;

public class Transaction : Entity, ITrackedEntity
{
    public string TimeStamp { get; set; }
    public DateTime DateTime { get; set; }
    public TransactionState State { get; set; }
    public string Comment { get; set; }
    public List<TransactionEntry> Entries { get; set; } = new();
}