﻿using Common.Models.Base;

namespace Common.Models;

public class Template : ReferenceChildEntity<TemplateGroup>
{
    public List<TemplateEntry> Entries { get; } = new();
}