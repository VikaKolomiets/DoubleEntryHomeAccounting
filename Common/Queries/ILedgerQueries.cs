﻿using Common.Infos;

namespace Common.Queries;

public interface ILedgerQueries
{
    LedgerInfo Get();
}