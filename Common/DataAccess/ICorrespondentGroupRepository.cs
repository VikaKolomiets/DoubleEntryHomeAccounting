﻿using Common.DataAccess.Base;
using Common.Models;

namespace Common.DataAccess;

public interface ICorrespondentGroupRepository : IParentEntityRepository<CorrespondentGroup>
{
}