﻿using Common.DataAccess.Base;
using Common.Models;

namespace Common.DataAccess;

public interface ITemplateRepository : IChildEntityRepository<Template>
{
    Task<List<TemplateEntry>> GetEntriesByAccount(Account account);
    Task<int> GetTemplateEntriesCount(Guid accountId);
}