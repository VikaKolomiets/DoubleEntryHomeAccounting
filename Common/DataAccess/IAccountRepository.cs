﻿using Common.DataAccess.Base;
using Common.Models;

namespace Common.DataAccess;

public interface IAccountRepository : IChildEntityRepository<Account>
{
    [Obsolete]
    Task LoadCurrency(Account account);

    Task<List<Account>> GetAccountsByCorrespondent(Correspondent correspondent);
    Task<List<Account>> GetAccountsByCategory(Category category);
    Task<List<Account>> GetAccountsByProject(Project project);
    Task<List<Account>> GetAccountsByCurrency(Currency currency);
}