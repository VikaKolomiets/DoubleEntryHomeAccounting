﻿using Common.DataAccess.Base;
using Common.Models;

namespace Common.DataAccess;

public interface ITransactionRepository : IEntityRepository<Transaction>
{
    Task<List<TransactionEntry>> GetEntriesByAccount(Account account);
    Task<int> GetTransactionEntriesCount(Guid accountId);
}