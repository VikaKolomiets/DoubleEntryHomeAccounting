﻿using Common.DataAccess.Base;
using Common.Models;

namespace Common.DataAccess;

public interface ITemplateGroupRepository : IParentEntityRepository<TemplateGroup>
{
}