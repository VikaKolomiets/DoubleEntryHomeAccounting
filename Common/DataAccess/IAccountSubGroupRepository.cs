﻿using Common.DataAccess.Base;
using Common.Models;

namespace Common.DataAccess;

public interface IAccountSubGroupRepository : IChildEntityRepository<AccountSubGroup>, IParentRepository<AccountSubGroup>
{
    
}