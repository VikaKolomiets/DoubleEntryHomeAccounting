﻿using Common.DataAccess.Base;
using Common.Models;

namespace Common.DataAccess;

public interface ICurrencyRepository : IEntityRepository<Currency>
{
    Task<Currency> GetByIsoCode(string isoCode);
    Task<int> GetMaxOrder();
    Task<int> GetCount();
    Task<List<Currency>> GetList();
}