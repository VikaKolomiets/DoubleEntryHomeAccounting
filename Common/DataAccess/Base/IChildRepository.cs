﻿using Common.Models.Interfaces;

namespace Common.DataAccess.Base;

public interface IChildRepository<T>
    where T : IEntity
{
    [Obsolete]
    Task LoadParent(T entity);
    Task<List<T>> GetByName(Guid parentId, string name);
    Task<int> GetMaxOrder(Guid parentId);
    Task<int> GetCount(Guid parentId);
    Task<List<T>> GetList(Guid parentId);
}