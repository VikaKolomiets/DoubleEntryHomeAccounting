﻿using Common.Models.Interfaces;

namespace Common.DataAccess.Base;

public interface IParentRepository<T>
    where T : IEntity
{
    [Obsolete]
    Task LoadChildren(T entity);
    Task <List<T>> GetByName(string name);
    Task<int> GetMaxOrder();
    Task<int> GetCount();
    Task<List<T>> GetList();
}