﻿using Common.Models.Interfaces;

namespace Common.DataAccess.Base;

public interface IParentEntityRepository<T> : IEntityRepository<T>, IParentRepository<T>
    where T : IEntity, INamedEntity
{
}