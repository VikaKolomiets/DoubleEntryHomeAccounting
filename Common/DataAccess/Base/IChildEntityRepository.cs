﻿using Common.Models.Interfaces;

namespace Common.DataAccess.Base;

public interface IChildEntityRepository<T> : IEntityRepository<T>, IChildRepository<T>
    where T : IEntity, INamedEntity
{
}