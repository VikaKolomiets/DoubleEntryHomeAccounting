﻿using Common.Models.Interfaces;

namespace Common.DataAccess.Base;

public interface IEntityRepository<T>
    where T : IEntity
{
    Task<T> Get(Guid id);
    Task<List<T>> FindBy(Func<T, bool> predicate, int top = 0);

    Task Add(T entity);
    Task AddList(List<T> list);

    Task Update(T entity);
    Task UpdateList(List<T> list);

    Task Delete(T entity);
    Task DeleteList(List<T> list);
}