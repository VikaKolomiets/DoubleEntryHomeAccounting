﻿using Common.DataAccess.Base;
using Common.Models;

namespace Common.DataAccess;

public interface IProjectRepository : IChildEntityRepository<Project>
{
}