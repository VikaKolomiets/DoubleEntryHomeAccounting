﻿using Business.Services.Base;
using Common.DataAccess;
using Common.Infrastructure.Peaa;
using Common.Models;
using Common.Params;
using Common.Services;

namespace Business.Services;

public class CorrespondentService 
    : ReferenceChildEntityService<Correspondent, CorrespondentGroup, ElementEntityParam>, ICorrespondentService
{
    public CorrespondentService(IUnitOfWorkFactory unitOfWorkFactory)
        : base(unitOfWorkFactory)
    {
    }

    protected override Func<IAccountRepository, Correspondent, Task<List<Account>>> GetAccountsByEntity =>
        async (accountRepository, correspondent) => await accountRepository.GetAccountsByCorrespondent(correspondent);


    protected override Action<Correspondent, Account> AccountEntitySetter =>
        (correspondent, account) => account.Correspondent = correspondent;
}