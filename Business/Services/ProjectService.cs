﻿using Business.Services.Base;
using Common.DataAccess;
using Common.Infrastructure.Peaa;
using Common.Models;
using Common.Params;
using Common.Services;

namespace Business.Services;

public class ProjectService 
    : ReferenceChildEntityService<Project, ProjectGroup, ElementEntityParam>, IProjectService
{
    public ProjectService(IUnitOfWorkFactory unitOfWorkFactory)
        : base(unitOfWorkFactory)
    {
    }

    protected override Func<IAccountRepository, Project, Task<List<Account>>> GetAccountsByEntity =>
        async (accountRepository, project) => await accountRepository.GetAccountsByProject(project);

    protected override Action<Project, Account> AccountEntitySetter => 
        (project, account) => account.Project = project;
}