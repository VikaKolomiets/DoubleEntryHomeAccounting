﻿using Business.Services.Base;
using Common.DataAccess;
using Common.Infrastructure.Peaa;
using Common.Models;
using Common.Params;
using Common.Services;

namespace Business.Services;

public class CategoryService 
    : ReferenceChildEntityService<Category, CategoryGroup, ElementEntityParam>, ICategoryService
{
    public CategoryService(IUnitOfWorkFactory unitOfWorkFactory)
        : base(unitOfWorkFactory)
    {
    }

    protected override Func<IAccountRepository, Category, Task<List<Account>>> GetAccountsByEntity =>
        async (accountRepository, category) => await accountRepository.GetAccountsByCategory(category);

    protected override Action<Category, Account> AccountEntitySetter =>
        (category, account) => account.Category = category;
}