﻿using Common.DataAccess;
using Common.DataAccess.Base;
using Common.Infrastructure.Peaa;
using Common.Models;
using Common.Models.Interfaces;
using Common.Params.Interfaces;
using Common.Services.Base;
using Common.Utils.Check;
using Common.Utils.Ordering;

namespace Business.Services.Base;

public abstract class ReferenceChildEntityService<TChild, TParent, TParam> : IReferenceChildEntityService<TChild, TParam>, ICombinedEntityService
    where TChild : IReferenceChildEntity<TParent>, new()
    where TParent : IReferenceParentEntity<TChild>
    where TParam : INamedParam, IFavoriteParam, IChildParam
{
    private readonly IUnitOfWorkFactory _unitOfWorkFactory;

    public ReferenceChildEntityService(IUnitOfWorkFactory unitOfWorkFactory)
    {
        _unitOfWorkFactory = unitOfWorkFactory;
    }

    public async Task<Guid> Add(TParam param)
    {
        using IUnitOfWork unitOfWork = _unitOfWorkFactory.Create();

        Guard.CheckParamForNull(param);
        Guard.CheckParamNameForNull(param);

        IChildEntityRepository<TChild> entityRepository = unitOfWork.GetRepository<IChildEntityRepository<TChild>>();
        IParentEntityRepository<TParent> parentEntityRepository = unitOfWork.GetRepository<IParentEntityRepository<TParent>>();

        TParent parent = await Getter.GetEntityById(parentEntityRepository.Get, param.ParentId);
        await Guard.CheckEntityWithSameName(entityRepository, parent.Id, Guid.Empty, param.Name);

        TChild addedEntity = new TChild()
        {
            Name = param.Name,
            Description = param.Description,
            IsFavorite = param.IsFavorite,
            Order = await entityRepository.GetMaxOrder(parent.Id) + 1
        };

        addedEntity.Parent = parent;
        parent.Children.Add(addedEntity);

        await entityRepository.Add(addedEntity);

        await unitOfWork.SaveChanges();

        return addedEntity.Id;
    }

    public async Task Update(Guid entityId, TParam param)
    {
        using IUnitOfWork unitOfWork = _unitOfWorkFactory.Create();

        Guard.CheckParamForNull(param);
        Guard.CheckParamNameForNull(param);

        IChildEntityRepository<TChild> entityRepository = unitOfWork.GetRepository<IChildEntityRepository<TChild>>();
        TChild updatedEntity = await Getter.GetEntityById(entityRepository.Get, entityId);
        await entityRepository.LoadParent(updatedEntity);
        TParent parent = updatedEntity.Parent;
        await Guard.CheckEntityWithSameName(entityRepository, parent.Id, entityId, param.Name);

        updatedEntity.Name = param.Name;
        updatedEntity.Description = param.Description;
        updatedEntity.IsFavorite = param.IsFavorite;

        await unitOfWork.SaveChanges();

        await entityRepository.Update(updatedEntity);
    }

    public async Task Delete(Guid entityId)
    {
        using IUnitOfWork unitOfWork = _unitOfWorkFactory.Create();

        IChildEntityRepository<TChild> entityRepository = unitOfWork.GetRepository<IChildEntityRepository<TChild>>();
        IParentEntityRepository<TParent> parentEntityRepository = unitOfWork.GetRepository<IParentEntityRepository<TParent>>();
        IAccountRepository accountRepository = unitOfWork.GetRepository<IAccountRepository>();


        TChild deletedEntity = await Getter.GetEntityById(entityRepository.Get, entityId);
        await entityRepository.LoadParent(deletedEntity);
        TParent parent = deletedEntity.Parent;

        await parentEntityRepository.LoadChildren(parent);

        List<Account> accounts = await this.GetAccountsByEntity(accountRepository,deletedEntity);
        foreach (Account account in accounts)
        {
            AccountEntitySetter(default, account);
            await accountRepository.Update(account);
        }

        parent.Children.Remove(deletedEntity);
        await entityRepository.Delete(deletedEntity);

        OrderingUtils.Reorder(parent.Children);
        await entityRepository.UpdateList(parent.Children);

        await unitOfWork.SaveChanges();
    }

    public async Task SetOrder(Guid entityId, int order)
    {
        using IUnitOfWork unitOfWork = _unitOfWorkFactory.Create();

        IChildEntityRepository<TChild> entityRepository = unitOfWork.GetRepository<IChildEntityRepository<TChild>>();
        IParentEntityRepository<TParent> parentEntityRepository = unitOfWork.GetRepository<IParentEntityRepository<TParent>>();

        TChild entity = await Getter.GetEntityById(entityRepository.Get, entityId);
        if (entity.Order != order)
        {
            await entityRepository.LoadParent(entity);
            TParent parent = entity.Parent;
            await parentEntityRepository.LoadChildren(parent);
            OrderingUtils.SetOrder(parent.Children, entity, order);
            await entityRepository.UpdateList(parent.Children);
        }

        await unitOfWork.SaveChanges();
    }

    public async Task SetFavoriteStatus(Guid entityId, bool isFavorite)
    {
        using IUnitOfWork unitOfWork = _unitOfWorkFactory.Create();

        IChildEntityRepository<TChild> entityRepository = unitOfWork.GetRepository<IChildEntityRepository<TChild>>();
        TChild etrity = await Getter.GetEntityById(entityRepository.Get, entityId);
        if (etrity.IsFavorite != isFavorite)
        {
            etrity.IsFavorite = isFavorite;
            await entityRepository.Update(etrity);
        }

        await unitOfWork.SaveChanges();
    }

    public async Task MoveToAnotherParent(Guid entityId, Guid parentId)
    {
        using IUnitOfWork unitOfWork = _unitOfWorkFactory.Create();

        IChildEntityRepository<TChild> entityRepository = unitOfWork.GetRepository<IChildEntityRepository<TChild>>();
        IParentEntityRepository<TParent> parentEntityRepository = unitOfWork.GetRepository<IParentEntityRepository<TParent>>();

        TChild entity = await Getter.GetEntityById(entityRepository.Get, entityId);
        await entityRepository.LoadParent(entity);
        TParent fromParent = entity.Parent;
        TParent toParent = await Getter.GetEntityById(parentEntityRepository.Get, parentId);

        if (fromParent.Id != toParent.Id)
        {
            await Guard.CheckEntityWithSameName(entityRepository, toParent.Id, entity.Id, entity.Name);
            await parentEntityRepository.LoadChildren(fromParent);

            entity.Order = await entityRepository.GetMaxOrder(toParent.Id) + 1;
            entity.Parent = toParent;
            toParent.Children.Add(entity);
            await entityRepository.Update(entity);

            fromParent.Children.Remove(entity);
            OrderingUtils.Reorder(fromParent.Children);
            await entityRepository.UpdateList(fromParent.Children);
        }

        await unitOfWork.SaveChanges();
    }

    public async Task CombineTwoEntities(Guid primaryId, Guid secondaryId)
    {
        using IUnitOfWork unitOfWork = _unitOfWorkFactory.Create();

        IChildEntityRepository<TChild> entityRepository = unitOfWork.GetRepository<IChildEntityRepository<TChild>>();
        IParentEntityRepository<TParent> parentEntityRepository = unitOfWork.GetRepository<IParentEntityRepository<TParent>>();
        IAccountRepository accountRepository = unitOfWork.GetRepository<IAccountRepository>();

        TChild primaryEntity = await Getter.GetEntityById(entityRepository.Get, primaryId);
        TChild secondaryEntity = await Getter.GetEntityById(entityRepository.Get, secondaryId);

        if (primaryEntity.Id != secondaryEntity.Id)
        {
            List<Account> accounts = await GetAccountsByEntity(accountRepository, secondaryEntity);
            foreach (Account account in accounts)
            {
                AccountEntitySetter(primaryEntity, account);
                await accountRepository.Update(account);
            }

            await entityRepository.LoadParent(secondaryEntity);
            TParent parent = secondaryEntity.Parent;
            await parentEntityRepository.LoadChildren(parent);

            parent.Children.Remove(secondaryEntity);
            await entityRepository.Delete(secondaryEntity);

            OrderingUtils.Reorder(parent.Children);
            await entityRepository.UpdateList(parent.Children);
        }

        await unitOfWork.SaveChanges();
    }

    protected abstract Func<IAccountRepository, TChild, Task<List<Account>>> GetAccountsByEntity { get; }
    protected abstract Action<TChild, Account> AccountEntitySetter { get; }
}