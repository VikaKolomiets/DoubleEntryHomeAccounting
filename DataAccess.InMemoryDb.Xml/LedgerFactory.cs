﻿using DataAccess.InMemoryDb.Xml.Persistence;

namespace DataAccess.InMemoryDb.Xml;

public class LedgerFactory : ILedgerFactory
{
    private readonly IXmlSerializer xmlSerializer;
    private readonly IXmlStorage xmlStorage;
    private readonly IXmlValidator xmlValidator;
    private Ledger ledger;

    public LedgerFactory(IXmlStorage xmlStorage, IXmlValidator xmlValidator, IXmlSerializer serializer)
    {
        this.xmlStorage = xmlStorage;
        this.xmlValidator = xmlValidator;
        xmlSerializer = serializer;
    }

    public Ledger Get()
    {
        if (ledger == null)
        {
            var doc = xmlStorage.Load();

            xmlValidator.Validate(doc);

            ledger = xmlSerializer.Deserialize(doc);
        }

        return ledger;
    }

    public void Save(Ledger ledger)
    {
        if (ledger == null) throw new ArgumentNullException(nameof(ledger));

        this.ledger = ledger;
        var doc = xmlSerializer.Serialize(ledger);
        xmlStorage.Save(doc);
    }
}