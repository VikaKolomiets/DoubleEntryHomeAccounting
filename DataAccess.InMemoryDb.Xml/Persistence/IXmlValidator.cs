﻿using System.Xml.Linq;

namespace DataAccess.InMemoryDb.Xml.Persistence;

public interface IXmlValidator
{
    void Validate(XDocument doc);
}