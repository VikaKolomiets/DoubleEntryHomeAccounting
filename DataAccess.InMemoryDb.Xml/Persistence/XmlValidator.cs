﻿using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace DataAccess.InMemoryDb.Xml.Persistence;

public class XmlValidator : IXmlValidator
{
    public void Validate(XDocument doc)
    {
        var xsd = new XmlSchemaSet();
        TextReader reader = new StringReader(XmLResource.MainSchema);
        var xmlReader = XmlReader.Create(reader);
        xsd.Add("", xmlReader);

        var errors = new List<string>();
        doc.Validate(xsd, (o, args) => errors.Add(args.Message));
        if (errors.Count > 0)
        {
            var message = string.Join(Environment.NewLine, errors);
            throw new ArgumentException(message);
        }
    }
}