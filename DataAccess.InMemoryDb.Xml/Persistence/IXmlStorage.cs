﻿using System.Xml.Linq;

namespace DataAccess.InMemoryDb.Xml.Persistence;

public interface IXmlStorage
{
    XDocument Load();
    void Save(XDocument doc);
}