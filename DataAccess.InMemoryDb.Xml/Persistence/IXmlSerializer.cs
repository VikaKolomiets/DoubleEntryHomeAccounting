﻿using System.Xml.Linq;

namespace DataAccess.InMemoryDb.Xml.Persistence;

public interface IXmlSerializer
{
    XDocument Serialize(Ledger ledger);
    Ledger Deserialize(XDocument document);
}