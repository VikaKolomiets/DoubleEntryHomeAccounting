﻿using DataAccess.InMemoryDb.Json.Persistence;

namespace DataAccess.InMemoryDb.Json;

public class JsonLedgerFactory : ILedgerFactory
{
    private readonly IJsonSerializer _serializer;
    private readonly IJsonStorage _storage;
    private readonly IJsonValidator _validator;
    private Ledger _ledger;

    public JsonLedgerFactory(IJsonSerializer serializer, IJsonStorage storage, IJsonValidator validator)
    {
        _serializer = serializer;
        _storage = storage;
        _validator = validator;
    }

    public Ledger Get()
    {
        if (_ledger == null)
        {
            var doc = _storage.Load();

            _validator.Validate(doc);

            _ledger = _serializer.Deserialize(doc);
        }

        return _ledger;
    }

    public void Save(Ledger ledger)
    {
        if (_ledger == null) throw new NullReferenceException("Ledger cannot be null");
        var doc = _serializer.Serialize(_ledger);

        _storage.Save(doc);
    }
}