﻿using Newtonsoft.Json.Linq;

namespace DataAccess.InMemoryDb.Json.Persistence;

public interface IJsonStorage
{
    JObject Load();
    Task Save(JObject doc);
}