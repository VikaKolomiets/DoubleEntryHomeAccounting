﻿using Newtonsoft.Json.Linq;

namespace DataAccess.InMemoryDb.Json.Persistence;

public interface IJsonValidator
{
    void Validate(JObject doc);
}