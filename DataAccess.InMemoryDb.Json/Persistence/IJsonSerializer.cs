﻿using Newtonsoft.Json.Linq;

namespace DataAccess.InMemoryDb.Json.Persistence;

public interface IJsonSerializer
{
    JObject Serialize(Ledger ledger);
    Ledger Deserialize(JObject document);
}